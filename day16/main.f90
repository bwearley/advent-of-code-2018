program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, j !, x, y

    !-- Input file unit
    integer :: input_unit

    !-- Position indicator in character line
    integer :: pos

    !-- Number of lines in input file
    integer :: num_instr

    !-- I/O Status
    integer :: iostat

    !-- Operation Indices
    integer,parameter :: ix_addr = 00
    integer,parameter :: ix_addi = 01
    integer,parameter :: ix_mulr = 02
    integer,parameter :: ix_muli = 03
    integer,parameter :: ix_banr = 04
    integer,parameter :: ix_bani = 05
    integer,parameter :: ix_borr = 06
    integer,parameter :: ix_bori = 07
    integer,parameter :: ix_setr = 08
    integer,parameter :: ix_seti = 09
    integer,parameter :: ix_gtir = 10
    integer,parameter :: ix_gtri = 11
    integer,parameter :: ix_gtrr = 12
    integer,parameter :: ix_eqir = 13
    integer,parameter :: ix_eqri = 14
    integer,parameter :: ix_eqrr = 15

    !-- Registers
    integer,parameter :: NUM_REGISTERS = 4
    integer :: reg(0:NUM_REGISTERS-1)

    !-- Opcodes
    integer,parameter :: NUM_OPCODES = 16
    type :: Opcode
        character(len=4) :: instr
        integer :: opcode = -1
        integer :: opcode_ix
        integer :: possibilities(0:NUM_OPCODES-1) = 1
        integer :: num_possibilities = 0
    end type
    type(Opcode) :: opcodes(0:NUM_OPCODES-1)

    ! Number of samples with 3 or more possibilities
    integer :: part1 = 0

    !-- Samples
    integer :: num_samples = 0

    integer,parameter :: MAX_SAMPLES = 1000

    type :: Sample
        integer :: reg_0(0:NUM_REGISTERS-1)
        integer :: reg_f(0:NUM_REGISTERS-1)
        integer :: opcode
        integer :: a
        integer :: b
        integer :: c
        logical :: possibilities(0:NUM_OPCODES) = .true.
        integer :: num_possibilities = 0
    end type
    type(Sample) :: samples(MAX_SAMPLES)

    !-- Instructions
    type :: Instruction
        integer :: op
        integer :: a
        integer :: b
        integer :: c
    end type
    type(Instruction), allocatable :: instructions(:)

    !-- Input file reading properties
    integer,parameter            :: max_line_len = 50
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file
    character(len=:),allocatable :: input_file_2

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file   = get_value_for_arg('input_file')
    input_file_2 = get_value_for_arg('input_file_2')

    !-- Configure Opcodes
    opcodes(00) % instr = 'addr'; opcodes(00) % opcode_ix = ix_addr
    opcodes(01) % instr = 'addi'; opcodes(01) % opcode_ix = ix_addi
    opcodes(02) % instr = 'mulr'; opcodes(02) % opcode_ix = ix_mulr
    opcodes(03) % instr = 'muli'; opcodes(03) % opcode_ix = ix_muli
    opcodes(04) % instr = 'banr'; opcodes(04) % opcode_ix = ix_banr
    opcodes(05) % instr = 'bani'; opcodes(04) % opcode_ix = ix_bani
    opcodes(06) % instr = 'borr'; opcodes(06) % opcode_ix = ix_borr
    opcodes(07) % instr = 'bori'; opcodes(07) % opcode_ix = ix_bori
    opcodes(08) % instr = 'setr'; opcodes(08) % opcode_ix = ix_setr
    opcodes(09) % instr = 'seti'; opcodes(09) % opcode_ix = ix_seti
    opcodes(10) % instr = 'gtir'; opcodes(10) % opcode_ix = ix_gtir
    opcodes(11) % instr = 'gtri'; opcodes(11) % opcode_ix = ix_gtri
    opcodes(12) % instr = 'gtrr'; opcodes(12) % opcode_ix = ix_gtrr
    opcodes(13) % instr = 'eqir'; opcodes(13) % opcode_ix = ix_eqir
    opcodes(14) % instr = 'eqri'; opcodes(14) % opcode_ix = ix_eqri
    opcodes(15) % instr = 'eqrr'; opcodes(15) % opcode_ix = ix_eqrr

    !-- Start timer
    call syslog % start_timer

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        iostat  = iostat,     &
        form    = 'formatted' &
    )
    i = 1
    READ_SAMPLE_INSTRUCTIONS: do

        ! Read line
        read (input_unit,'(a)',iostat=iostat) line

        if (iostat /= 0) exit READ_SAMPLE_INSTRUCTIONS

        ! Read initial state
        if (line(1:7) == 'Before:') then

            ! Strip Before:  [
            pos = index(line, '[')
            line(1:pos) = ' '

            ! Strip trailing ]
            pos = index(line, ']')
            line(pos:) = ' '

            read (line,*)              &
                samples(i) % reg_0(0), &
                samples(i) % reg_0(1), &
                samples(i) % reg_0(2), &
                samples(i) % reg_0(3)

        ! Read final state
        else if (line(1:6) == 'After:') then

            ! Strip After:  [
            pos = index(line, '[')
            line(1:pos) = ' '

            ! Strip trailing ]
            pos = index(line, ']')
            line(pos:) = ' '

            read (line,*)              &
                samples(i) % reg_f(0), &
                samples(i) % reg_f(1), &
                samples(i) % reg_f(2), &
                samples(i) % reg_f(3)

        ! Blank line: move to next sample
        else if (line == ' ') then
            i = i + 1
            if (i > MAX_SAMPLES) then
                call syslog%log(__FILE__,'ERROR: Exceeded maximum number of samples.')
                call bomb
            end if

        ! Read opcode, a, b, c values
        else
            read(line,*) samples(i) % opcode, samples(i) % a, samples(i) % b, samples(i) % c
        end if 

    end do READ_SAMPLE_INSTRUCTIONS
    close (input_unit)
    num_samples = i

    !-- Write
    call syslog%log(__FILE__,'Read '//string(num_samples)//' CPU samples.')
    if (.false.) then
        do i = 1, num_samples
            write (syslog%unit,'(a,i0)')  'Instruction ', i
            write (syslog%unit,'(a,4i3)') 'Reg_0: ', samples(i)%reg_0
            write (syslog%unit,'(a,4i3)') 'Reg_f: ', samples(i)%reg_f
            write (syslog%unit,'(a,4i3)') 'Instr: ', samples(i)%opcode, samples(i)%a, samples(i)%b, samples(i)%c 
        end do
    end if

    do i = 1, num_samples

        !addr
        reg(:) = samples(i) % reg_0(:)
        call addr(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_addr) = .false.
        end if

        !addi
        reg(:) = samples(i) % reg_0(:)
        call addi(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_addi) = .false.
        end if

        !mulr
        reg(:) = samples(i) % reg_0(:)
        call mulr(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_mulr) = .false.
        end if

        !muli
        reg(:) = samples(i) % reg_0(:)
        call muli(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_muli) = .false.
        end if

        !banr
        reg(:) = samples(i) % reg_0(:)
        call banr(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_banr) = .false.
        end if

        !bani
        reg(:) = samples(i) % reg_0(:)
        call bani(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_bani) = .false.
        end if

        !borr
        reg(:) = samples(i) % reg_0(:)
        call borr(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_borr) = .false.
        end if

        !bori
        reg(:) = samples(i) % reg_0(:)
        call bori(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_bori) = .false.
        end if

        !setr
        reg(:) = samples(i) % reg_0(:)
        call setr(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_setr) = .false.
        end if

        !seti
        reg(:) = samples(i) % reg_0(:)
        call seti(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_seti) = .false.
        end if

        !gtir
        reg(:) = samples(i) % reg_0(:)
        call gtir(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_gtir) = .false.
        end if

        !gtri
        reg(:) = samples(i) % reg_0(:)
        call gtri(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_gtri) = .false.
        end if

        !gtrr
        reg(:) = samples(i) % reg_0(:)
        call gtrr(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_gtrr) = .false.
        end if

        !eqir
        reg(:) = samples(i) % reg_0(:)
        call eqir(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_eqir) = .false.
        end if

        !eqri
        reg(:) = samples(i) % reg_0(:)
        call eqri(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_eqri) = .false.
        end if

        !eqrr
        reg(:) = samples(i) % reg_0(:)
        call eqrr(samples(i)%a, samples(i)%b, samples(i)%c)
        if (all(reg(:) == samples(i) % reg_f(:))) then
            samples(i) % num_possibilities = samples(i) % num_possibilities + 1
        else
            samples(i) % possibilities(ix_eqrr) = .false.
        end if

    end do

    !-- Part 1: Identify how many samples had 3 or more opcode possibilities
    do i = 1, num_samples
        if (samples(i) % num_possibilities >= 3) part1 = part1 + 1
    end do
    write (syslog%unit,'(a,i0)') 'Part 1: ', part1 !493
    write (          *,'(a,i0)') 'Part 1: ', part1 !493

    !-- Part 2: Identify opcodes
    do j = 0, NUM_OPCODES-1
        do i = 1, num_samples
            if (.not. samples(i) % possibilities(j)) then
                opcodes(j) % possibilities(samples(i) % opcode) = 0
            end if
        end do
    end do

    !-- Write chart of opcode possibilities
    write (syslog%unit,'(5x)',advance='no')
    do j = 0, NUM_OPCODES-1
        write (syslog%unit,'(i3)',advance='no') j
    end do
    write (syslog%unit,*) ! advance
    do j = 0, NUM_OPCODES-1
        write (syslog%unit,'(1x,a,16l3,i3)') opcodes(j) % instr, opcodes(j) % possibilities, sum(opcodes(j) % possibilities)
    end do

    !-- Count lines in input file 2
    num_instr = lines_in_file(input_file_2)
    allocate(instructions(num_instr))

    !-- Open file and read into memory
    open (                      & 
        newunit = input_unit,   & 
        file    = input_file_2, &
        action  = 'read',       &
        status  = 'old',        &
        iostat  = iostat,       &
        form    = 'formatted'   &
    )
    READ_INSTRUCTIONS: do i = 1, num_instr

        ! Read line
        read (input_unit,'(a)',iostat=iostat) line

        ! Read instructions
        read(line,*) instructions(i) % op, instructions(i) % a, instructions(i) % b, instructions(i) % c

    end do READ_INSTRUCTIONS
    close (input_unit)

    !-- Process instructions
    reg(:) = 0
    do i = 1, num_instr
        call do_instr(instructions(i)%op,instructions(i)%a,instructions(i)%b,instructions(i)%c)
    end do

    !-- Write final status
    write (syslog%unit,'(a,i0)') 'Part 2: ', reg(0) ! 445
    write (          *,'(a,i0)') 'Part 2: ', reg(0) ! 445

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

contains

    subroutine do_instr(op, a, b, c)
        integer,intent(in) :: op
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c

        select case (op)
            case(0);  call banr(a, b, c);
            case(1);  call muli(a, b, c);
            case(2);  call bori(a, b, c);
            case(3);  call setr(a, b, c);
            case(4);  call addi(a, b, c);
            case(5);  call eqrr(a, b, c);
            case(6);  call gtri(a, b, c);
            case(7);  call gtir(a, b, c);
            case(8);  call borr(a, b, c);
            case(9);  call eqri(a, b, c);
            case(10); call bani(a, b, c);
            case(11); call addr(a, b, c);
            case(12); call eqir(a, b, c);
            case(13); call mulr(a, b, c);
            case(14); call seti(a, b, c);
            case(15); call gtrr(a, b, c);
        case default
        end select
    end subroutine

    !> addr (add register) stores into register C the
    ! result of adding register A and register B
    subroutine addr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a) + reg(b)
    end subroutine

    !> addi (add immediate) stores into register C the
    ! result of adding register A and value B
    subroutine addi(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a) + b
    end subroutine

    !>mulr (multiply register) stores into register C the
    ! result of multiplying register A and register B
    subroutine mulr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a) * reg(b)
    end subroutine

    !> muli (multiply immediate) stores into register C the
    ! result of multiplying register A and value B
    subroutine muli(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a) * b
    end subroutine

    !> banr (bitwise AND register) stores into register C the
    ! result of the bitwise AND of register A and register B
    subroutine banr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
       reg(c) = iand(reg(a), reg(b))
    end subroutine

    !> bani (bitwise AND immediate) stores into register C the
    ! result of the bitwise AND of register A and value B
    subroutine bani(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = iand(reg(a), b)
    end subroutine

    !> borr (bitwise OR register) stores into register C the
    ! result of the bitwise OR of register A and register B
    subroutine borr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = ior(reg(a), reg(b))
    end subroutine

    !> bori (bitwise OR immediate) stores into register C the
    ! result of the bitwise OR of register A and value B
    subroutine bori(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = ior(reg(a), b)
    end subroutine

    !> setr (set register) copies the contents of register A
    ! into register C. (Input B is ignored.)
    subroutine setr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a)
    end subroutine

    !> seti (set immediate) stores value A into register C.
    ! (Input B is ignored.)
    subroutine seti(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = a
    end subroutine

    !> gtir (greater-than immediate/register) sets register C
    ! to 1 if value A is greater than register B. Otherwise,
    ! register C is set to 0
    subroutine gtir(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (a > reg(b)) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> gtri (greater-than register/immediate) sets register C
    ! to 1 if register A is greater than value B. Otherwise,
    ! register C is set to 0
    subroutine gtri(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (reg(a) > b) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> gtrr (greater-than register/register) sets register C
    ! to 1 if register A is greater than register B. Otherwise,
    ! register C is set to 0
    subroutine gtrr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (reg(a) > reg(b)) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> eqir (equal immediate/register) sets register C to 1 if
    ! value A is equal to register B. Otherwise, register C is
    ! set to 0
    subroutine eqir(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (a == reg(b)) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> eqri (equal register/immediate) sets register C to 1 if
    ! register A is equal to value B. Otherwise, register C is
    ! set to 0
    subroutine eqri(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (reg(a) == b) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> eqrr (equal register/register) sets register C to 1 if
    ! register A is equal to register B. Otherwise, register C
    ! is set to 0
    subroutine eqrr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (reg(a) == reg(b)) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

end program