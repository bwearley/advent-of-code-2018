program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, num_iters
    integer :: x, y

    integer :: x_min, x_max
    integer :: y_min, y_max
    integer :: x_size, y_size

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Point positions and velocities
    integer,allocatable :: x_pos(:)
    integer,allocatable :: y_pos(:)
    integer,allocatable :: x_vel(:)
    integer,allocatable :: y_vel(:)

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(x_pos(num_lines))
    allocate(y_pos(num_lines))
    allocate(x_vel(num_lines))
    allocate(y_vel(num_lines))

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    write (syslog%unit,'(a)') "      #      X      Y     Xv     Yv"
    do i = 1, num_lines
        
        !-- Read
        read (input_unit,'(a)') line

        ! Remove: "position=<"
        line(1:10) = ' '

        ! Remove: "> velocity=<"
        line(scan(line,'>'):scan(line,'<')) = ' '

        ! Remove: ">"
        line(scan(line,'>'):scan(line,'>')) = ' '
  
        ! Read data
        read(line,*) x_pos(i),y_pos(i),x_vel(i),y_vel(i)

        !-- Write to log
        write (syslog%unit,'(5i7)') i, x_pos(i),y_pos(i),x_vel(i),y_vel(i)

    end do
    close (input_unit)

    !-- Start timer
    call syslog % start_timer

    !-- Integrate point velocities
    num_iters = 0
    CALC_POS: do
        num_iters = num_iters + 1

        ! Calculate new positions
        x_pos(:) = x_pos(:) + x_vel(:)
        y_pos(:) = y_pos(:) + y_vel(:)

        ! Determine canvas height
        x_min = minval(x_pos)
        x_max = maxval(x_pos)
        y_min = minval(y_pos)
        y_max = maxval(y_pos)
        x_size = x_max - x_min
        y_size = y_max - y_min
        
        ! For sample.txt: y_size < 9
        if (y_size < 10) exit CALC_POS

    end do CALC_POS

    !-- Write to log
    call syslog%log(__FILE__,'Performed '//string(num_iters)//' iterations.')
    write (syslog%unit,'(4(a,i0),a)') ' Bounding points:'
    write (syslog%unit,'(a,i0)') 'x_min = ', x_min
    write (syslog%unit,'(a,i0)') 'x_max = ', x_max
    write (syslog%unit,'(a,i0)') 'y_min = ', y_min
    write (syslog%unit,'(a,i0)') 'y_max = ', y_max
    write (syslog%unit,'(2(a,i0),a)') ' Size of canvas is: (',x_size,',',y_size,')'

    !-- Draw the message
    write (syslog%unit,'(a)') 'The message is: ' ! GGLZLHCE
    DRAW_Y: do y = y_min, y_max

        DRAW_X: do x = x_min, x_max

            CHECK_PTS: do i = 1, num_lines

                if (y == y_pos(i) .and. x == x_pos(i)) then
                    write (syslog%unit,'(a)',advance='no') '#'
                    cycle DRAW_X
                end if

                if (i == num_lines) then
                    write (syslog%unit,'(a)',advance='no') ' '
                end if

            end do CHECK_PTS

        end do DRAW_X
        write (syslog%unit,*) ! advance

    end do DRAW_Y
    write (syslog%unit,*) ! advance

    !-- Part 2 (10144)
    call syslog%log(__FILE__,'This would have taken '//string(num_iters)//' seconds.')

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program