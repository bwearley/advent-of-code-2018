module map_mod
    use syslog_mod
    use string_tools_mod
    use get_digit_of_number_mod
    use Queue_mod
    implicit none

    integer,parameter :: TIME_TO_MOVE_ONE_SQUARE = 1

    integer,parameter :: TIME_TO_SWITCH_TOOLS = 7

    ! Map Dimensions
    integer :: x_dim = 0
    integer :: y_dim = 0

    integer :: x_dim_min = 0
    integer :: y_dim_min = 0

    integer :: depth = 0
    integer :: target(2)

    !-- Map Square Type Definitions
    enum,bind(c)
        enumerator :: None
        enumerator :: Rocky
        enumerator :: Narrow
        enumerator :: Wet
        enumerator :: RockyTarget
    end enum

    !-- Tool Definitions
    enum,bind(C)
        enumerator :: ClimbingGear
        enumerator :: Torch
        enumerator :: Neither
    end enum

    !-- MapSquare struct
    type :: MapSquare
        integer :: x = -1
        integer :: y = -1
        integer :: geo_cache = -1
        integer :: ero_cache = -1
        logical :: visited = .false.
        integer :: curr_tool = Neither
        integer :: dist = +HUGE(x_dim)
    contains
        procedure :: a
        procedure :: geologic
        procedure :: erosion
        procedure :: sqtype
        procedure :: risk
        procedure :: possible_tools
    end type
    type(MapSquare), allocatable :: map(:,:)
    type(MapSquare), allocatable :: dijmap(:,:,:)

contains

    ! Tools can only be used in certain regions:
    ! + In rocky regions, you can use the climbing gear or the torch.
    !   You cannot use neither (you'll likely slip and fall).
    ! + In wet regions, you can use the climbing gear or neither tool.
    !   You cannot use the torch (if it gets wet, you won't have a light source).
    ! + In narrow regions, you can use the torch or neither tool.
    !   You cannot use the climbing gear (it's too bulky to fit).
    integer function minimum_distance_point_to_point_3(x0,y0,t0,x1,y1,t1) result(dist)
        integer :: x0,x1
        integer :: y0,y1
        integer :: t0,t1
        integer :: newx, newy
        integer :: time
        integer :: newtool
        ! Queue
        TYPE(QITEM) :: queue(0:x_dim*y_dim*3)
        type(QITEM) :: node
        integer :: first, last

        ! Init queue indices
        first = 0
        last = 0
        
        ! Mark starting point
        call add_to_queue(Qitem(x=x0,y=y0,tool=t0,dist=0),queue,last)

        ! BFS
        do while (first /= last)
            
            node = queue(first)
            call delete_from_queue(first,last)
            
            ! Destination found
            !if (node%x == x1 .and. node%y == y1 .and. node%tool == t1) then
            !    dist = node % dist
            !    !return
            !end if

            ! Moving y down
            newx = node % x
            newy = node % y + 1
            newtool = node % tool
            time = TIME_TO_MOVE_ONE_SQUARE
            if (newy <= y_dim) then
                if (tool_is_valid_at_point(newtool,newx,newy)) then
                    if (node%dist+time < dijmap(newx,newy,newtool) % dist) then !(.not. dijmap(newx,newy,newtool) % visited) then
                        dijmap(newx,newy,newtool) % dist = node%dist+time
                        call add_to_queue(Qitem(x=newx,y=newy,tool=newtool,dist=node%dist+time),queue,last)
                    end if
                end if
            end if
            
            ! Moving y up
            newx = node % x
            newy = node % y - 1
            newtool = node % tool
            time = TIME_TO_MOVE_ONE_SQUARE
            if (newy >= y_dim_min) then
                if (tool_is_valid_at_point(newtool,newx,newy)) then
                    if (node%dist+time < dijmap(newx,newy,newtool) % dist) then !(.not. dijmap(newx,newy,newtool) % visited) then
                        dijmap(newx,newy,newtool) % dist = node%dist+time
                        call add_to_queue(Qitem(x=newx,y=newy,tool=newtool,dist=node%dist+time),queue,last)
                    end if
                end if
            end if

            ! Moving x left
            newx = node % x - 1
            newy = node % y
            newtool = node % tool
            time = TIME_TO_MOVE_ONE_SQUARE
            if (newx >= x_dim_min) then
                if (tool_is_valid_at_point(newtool,newx,newy)) then
                    if (node%dist+time < dijmap(newx,newy,newtool) % dist) then
                        dijmap(newx,newy,newtool) % dist = node%dist+time
                        call add_to_queue(Qitem(x=newx,y=newy,tool=newtool,dist=node%dist+time),queue,last)
                    end if
                end if
            end if

            ! Moving x right
            newx = node % x + 1
            newy = node % y
            newtool = node % tool
            time = TIME_TO_MOVE_ONE_SQUARE
            if (newx <= x_dim) then
                if (tool_is_valid_at_point(newtool,newx,newy)) then
                    if (node%dist+time < dijmap(newx,newy,newtool) % dist) then
                        dijmap(newx,newy,newtool) % dist = node%dist+time
                        call add_to_queue(Qitem(x=newx,y=newy,tool=newtool,dist=node%dist+time),queue,last)
                    end if
                end if
            end if

            ! Staying put & switching tools
            newx = node % x
            newy = node % y
            newtool = switch_tool_at_point(newx,newy,node % tool)
            time = TIME_TO_SWITCH_TOOLS
            if (node%dist+time < dijmap(newx,newy,newtool) % dist) then
                dijmap(newx,newy,newtool) % dist = node%dist+time
                call add_to_queue(Qitem(x=newx,y=newy,tool=newtool,dist=node%dist+time),queue,last)
            end if

        end do
        
        dist = dijmap(x1,y1,t1) % dist
        return

    end function

    integer function switch_tool_at_point(x0,y0,t0) result(tool)
        integer,intent(in) :: x0
        integer,intent(in) :: y0
        integer,intent(in) :: t0
        integer :: poss_tools(2)

        poss_tools(:) = map(x0,y0) % possible_tools()    

        if (poss_tools(1) == t0) then
            tool = poss_tools(2)
        else if (poss_tools(2) == t0) then
            tool = poss_tools(1)
        else
            write (syslog%unit,'(a)') 'switch_tool_at_point failed at point'
            write (syslog%unit,'(a,i3,a,i3)') 'Point: ', x0, ',', y0
            stop
        end if

    end function

    function possible_tools(self) result(tools)
        class(MapSquare) :: self
        integer :: tools(2)

        ! Rocky
        if (self % sqtype() == Rocky) then
            tools(1) = ClimbingGear
            tools(2) = Torch
            return
        end if

        ! Wet
        if (self % sqtype() == Wet) then
            tools(1) = ClimbingGear
            tools(2) = Neither
            return
        end if

        ! Narrow
        if (self % sqtype() == Narrow) then
            tools(1) = Torch
            tools(2) = Neither
            return
        end if

        ! RockyTarget
        if (self % sqtype() == RockyTarget) then
            tools(1) = Torch
            tools(2) = ClimbingGear ! <- Still: must switch to torch
            return
        end if

    end function

    logical function tool_is_valid_at_point(tool,x0,y0) result(valid)
        integer,intent(in) :: tool
        integer,intent(in) :: x0
        integer,intent(in) :: y0


        select case (map(x0,y0) % sqtype())

        case (Rocky,RockyTarget)
            if (tool == ClimbingGear .or. tool == Torch) then
                valid = .true.
                return
            else
                valid = .false.
                return
            end if

        case (Wet)
            if (tool == ClimbingGear .or. tool == Neither) then
                valid = .true.
                return
            else
                valid = .false.
                return
            end if

        case (Narrow)
            if (tool == Torch .or. tool == Neither) then
                valid = .true.
                return
            else
                valid = .false.
                return
            end if

        case default

            write (syslog%unit,'(a)') 'Error: tool_is_valid filed at square'
            write (syslog%unit,'(a,2i4)') 'Sq:', x0, y0
            stop

        end select

    end function tool_is_valid_at_point

    !> Character value of a coordinate
    function a(self)
        class(MapSquare) :: self
        character(len=1) :: a
        select case (self % sqtype())
        case (Rocky)
            a = '.'
        case (Wet)
            a = '='
        case (Narrow)
            a = '|'
        case (RockyTarget)
            a = 'T'
        case default
        end select
    end function

    !> Geologic index of this coordinate
    ! The region at 0,0 (the mouth of the cave) has a geologic index of 0.
    ! The region at the coordinates of the target has a geologic index of 0.
    ! If the region's Y coordinate is 0, the geologic index is its X coordinate times 16807.
    ! If the region's X coordinate is 0, the geologic index is its Y coordinate times 48271.
    ! Otherwise, the region's geologic index is the result of multiplying the erosion levels 
    ! of the regions at X-1,Y and X,Y-1.
    recursive integer function geologic(self)
        class(MapSquare) :: self
        
        ! Cache
        if (self % geo_cache /= -1) then
            geologic = self % geo_cache
            return
        end if

        if (self % x == 0 .and. self % y == 0) then
            geologic = 0
        else if (self % x == target(1) .and. self % y == target(2)) then
            geologic = 0
        else if (self % y == 0) then
            geologic = self % x * 16807
        else if (self % x == 0) then
            geologic = self % y * 48271
        else
            geologic = &
                map(self % x-1, self % y    ) % erosion() &
              * map(self % x  , self % y - 1) % erosion()
        end if

        ! Cache
        self % geo_cache = geologic

    end function

    !> Erosion level of this coordinate
    ! A region's erosion level is its geologic index plus the cave system's depth,
    ! all modulo 20183. Then:
    ! If the erosion level modulo 3 is 0, the region's type is rocky.
    ! If the erosion level modulo 3 is 1, the region's type is wet.
    ! If the erosion level modulo 3 is 2, the region's type is narrow.
    recursive integer function erosion(self)
        class(MapSquare) :: self

        ! Cache
        if (self % ero_cache /= -1) then
            erosion = self % ero_cache
            return
        end if

        erosion = mod((self % geologic() + depth),20183)

        self % ero_cache = erosion
    end function

    !> Gets the type of this coordinate
    ! If the erosion level modulo 3 is 0, the region's type is rocky.
    ! If the erosion level modulo 3 is 1, the region's type is wet.
    ! If the erosion level modulo 3 is 2, the region's type is narrow.
    recursive integer function sqtype(self)
        class(MapSquare) :: self

        !if (self % sqtype() /= None) return

        if (self % x == target(1) .and. self % y == target(2)) then
            sqtype = RockyTarget
            return
        end if

        select case (mod(self % erosion(), 3))
        case (0)
            sqtype = Rocky
        case (1)
            sqtype = Wet
        case (2)
            sqtype = Narrow
        case default
            write (syslog%unit,'(a,2i3,a,i0,a,i4)')         &
                'Error: Could not get type of coordinate ', &
                self % x, self % y, ' with erosion level ', &
                self % erosion(), '; erosion % 3 = ',       &
                mod(self % erosion(), 3)
            stop
        end select

    end function

    !> Risk for this coordinate
    ! 0 for rocky regions
    ! 1 for wet regions
    ! 2 for narrow regions
    recursive integer function risk(self)
        class(MapSquare) :: self

        select case (self % sqtype())
        case (Rocky,RockyTarget)
            risk = 0
        case (Wet)
            risk = 1
        case (Narrow)
            risk = 2
        case default
            write (syslog%unit,'(a,3i3)')   &
                'Risk function failed for', &
                self % x, self % y
            stop
        end select
    end function

    !> Write out the current state of map
    subroutine write_state(unit)
        implicit none
        integer,intent(in) :: unit
        integer :: x, y

        !-- Write header
        ! ! Hundreds Place
        ! write (syslog%unit,'(a)', advance='no') '    '
        ! do x = x_dim_min, x_dim !_max
        !     write (syslog%unit,'(i1)',advance='no') get_digit_of_number(3,x)
        ! end do
        write (unit,*) ! advance
        ! Tens Place
        write (unit,'(a)', advance='no') '    '
        do x = x_dim_min, x_dim !_max
            write (unit,'(i1)',advance='no') get_digit_of_number(2,x)
        end do
        write (unit,*) ! advance
        ! Ones Place
        write (unit,'(a)', advance='no') '    '
        do x = x_dim_min, x_dim !_max
            write (unit,'(i1)',advance='no') get_digit_of_number(1,x)
        end do
        write (unit,*) ! advance
        write (unit,*) ! advance

        !-- Grid
        do y = y_dim_min, y_dim !_max !min(y_dim_max,100)
            write (unit,'(i0.3,a)',advance='no') y, ' '
            do x = x_dim_min, x_dim !_max
                write (unit,'(a)',advance='no') map(x,y) % a()
            end do
            write (unit,*) ! advance
        end do
    end subroutine



end module
