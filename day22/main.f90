program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use map_mod
    implicit none

    !-- Counters
    integer :: i
    integer :: x, y

    integer :: pos

    !-- Input file unit
    integer :: input_unit

    !-- Puzzle values
    integer :: part1 = 0
    integer :: part2 = 0

    integer,allocatable :: part1rect(:,:)

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Start timer
    call syslog % start_timer

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    !-- Read input file
    do i = 1, 2

        ! Read line
        read (input_unit,'(a)') line

        ! Remove label
        pos = index(line,':')
        line = line(pos+1:)

        ! 1st line: read depth
        if (i == 1) read(line,*) depth

        ! 2nd line: read target coordinates
        if (i == 2) read(line,*) target(1),target(2)

    end do
    close (input_unit)
    write (syslog%unit,'(a,i0)') 'Depth: ',depth
    write (syslog%unit,'(2(a,i0))') 'Target: ',target(1),', ',target(2)

    ! Allocate map
    x_dim = 300
    y_dim = depth + 10
    allocate(map(x_dim_min:x_dim,y_dim_min:y_dim))
    allocate(dijmap(x_dim_min:x_dim,y_dim_min:y_dim,0:2)) !<- third dimension is number of tools
    allocate(part1rect(0:target(1),0:target(2)))

    !-- Generate map
    ! Assign coordinates
    do y = y_dim_min, y_dim
        do x = x_dim_min, x_dim
            map(x,y) % x = x
            map(x,y) % y = y
        end do
    end do
    flush(syslog%unit)

    call write_state(syslog%unit)

    ! Initialize dijmap
    do i = 0, 2
        dijmap(:,:,i) = map(:,:)
    end do

    !-- Part 1
    do y = 0, target(2)
        do x = 0, target(1)
            part1rect(x,y) = map(x,y) % risk()
        end do
    end do
    part1 = sum(part1rect)
    write (          *,*) 'Part 1: ',part1 ! 7743
    write (syslog%unit,*) 'Part 1: ',part1 ! 7743

    !-- Part 2
    part2 = minimum_distance_point_to_point_3(0,0,Torch,target(1),target(2),Torch)
    write (          *,*) 'Part 2: ',part2 ! 1029
    write (syslog%unit,*) 'Part 2: ',part2 ! 1029

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program