program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, j

    !-- Index of character
    integer :: ix

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Parameters
    integer,parameter :: SERIAL_LEN       = 26  ! length of a serial number
    integer,parameter :: NUM_CHARS        = 26  ! number of ASCII characters to track
    !integer,parameter :: ASCII_CHAR_SHIFT = 96  ! shift in ASCII characters to get array index
    ! A = 65, Z = 90, a = 97, z = 122
    ! a = 97 -> i = 1

    !-- Main variables

    ! Data from input file (serials)
    character(len=SERIAL_LEN),allocatable :: serials(:)      

    ! Serials -> iachar
    integer,allocatable :: iserials(:,:)

    ! Temporary
    integer :: itmp(SERIAL_LEN)     

    ! Frequency of letters
    integer,allocatable :: freqs(:,:)
    integer,allocatable :: count_2(:)
    integer,allocatable :: count_3(:)  

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(serials            (num_lines))
    allocate(iserials(SERIAL_LEN,num_lines))
    allocate(freqs(NUM_CHARS,    num_lines))
    allocate(count_2            (num_lines))
    allocate(count_3            (num_lines))

    !-- Initializations
    freqs(:,:)    = 0
    iserials(:,:) = 0
    itmp(:)       = 0
    count_2(:)    = 0
    count_3(:)    = 0

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    read (input_unit,*) (serials(i), i = 1, num_lines)
    close (input_unit)
    write (syslog%unit,*) 'Found input values: '
    write (syslog%unit,'(a)') serials

    !-- Start timer
    call syslog % start_timer

    !-- Calculate frequency of each letter
    do i = 1, num_lines
        
        do j = 1, SERIAL_LEN
 
            ix = letter_index(serials(i)(j:j)) !iachar(serials(i)(j:j))-ASCII_CHAR_SHIFT

            freqs(ix,i) = freqs(ix,i) + 1

            iserials(j,i) = ix

        end do

        !-- Get counts
        if (any(freqs(:,i) == 2)) count_2(i) = 1
        if (any(freqs(:,i) == 3)) count_3(i) = 1
    end do

    !-- Find serials that differ by only 1
    PRIMARY_LOOP: do i = 1, num_lines

        SECONDARY_LOOP: do j = 1, num_lines

            if (i == j) cycle

            itmp(:) = min(abs(iserials(:,i) - iserials(:,j)),1)

            if (sum(itmp) == 1) then
                write (syslog%unit,*) 'Found:'
                write (syslog%unit,*) 'Suspected serials: ', i, j
                write (syslog%unit,'(26i3)') iserials(:,i)
                write (syslog%unit,'(26i3)') iserials(:,j)
                write (syslog%unit,'(a)') serials(i)
                write (syslog%unit,'(a)') serials(j)
                exit PRIMARY_LOOP
            end if

        end do SECONDARY_LOOP

    end do PRIMARY_LOOP

    !-- Write frequency data to log
    write (syslog%unit,*) 'Serial Counts:'
    write (syslog%unit,'(a)',advance='no') '---------------------------'
    ! Write header
    do i = 1, NUM_CHARS
        write (syslog%unit,'(a3)',advance='no') lowercase_letter_at_index(i) !achar(i+ASCII_CHAR_SHIFT)
    end do
    write (syslog%unit,'(a3)') '  2  3'
    do i = 1, num_lines
        write (syslog%unit,'(a,26i3,26i3)',advance='no') serials(i),freqs(:,i), count_2(i), count_3(i)
    end do

    !-- Write character indices to log
    write (syslog%unit,*) 'Character indices:'
    write (syslog%unit,'(a)',advance='no') '---------------------------'
    ! Write header
    do i = 1, NUM_CHARS
        write (syslog%unit,'(i3)',advance='no') i
    end do
    write (syslog%unit,*) ! advance
    do i = 1, num_lines
        write (syslog%unit,'(a,26i3,26i3)',advance='no') serials(i), iserials(:,i)
    end do

    !-- Write diagnostic data to log
    write (syslog%unit,*) 'Counts:'
    write (syslog%unit,*) 'Boxes with 2 repeats: ', sum(count_2)
    write (syslog%unit,*) 'Boxes with 3 repeats: ', sum(count_3)

    !-- Write checksum answer
    write (syslog%unit,*) 'Checksum: ', sum(count_2) * sum(count_3) !7221
    write (          *,*) 'Checksum: ', sum(count_2) * sum(count_3)

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program