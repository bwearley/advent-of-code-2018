package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func main() {
	file, err := os.Open("../day2.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	const ASCII_CHAR_SHIFT = 97

	// Read serial numbers
	var serials []string
	var iserials [][]int
	numLines := 0
	for scanner.Scan() {
		line := scanner.Text()
		serials = append(serials, line)
		numLines += 1
		var iserial []int
		for _, char := range line {
			iserial = append(iserial, int(char)-ASCII_CHAR_SHIFT)
		}
		iserials = append(iserials, iserial)
	}

	type FreqStats struct {
		freq [26]int
	}

	// Calculate letter frequencies
	count2 := 0
	count3 := 0
	var freqs []FreqStats
	for _, ser := range iserials {
		var freq [26]int
		for _, lett := range ser {
			freq[lett] += 1
		}

		// Count those with 2 or more
		for _, f := range freq {
			if f == 2 {
				count2 += 1
				break
			}
		}
		// Count those with 3 or more
		for _, f := range freq {
			if f == 3 {
				count3 += 1
				break
			}
		}
		freqs = append(freqs, FreqStats{freq})
	}

	part1 := count2 * count3
	fmt.Println("Boxes with 2 repeats: ", count2)
	fmt.Println("Boxes with 3 repeats: ", count3)
	fmt.Println("Part 1: ", part1)

	// Determine serials that differ by 1 letter
	for i, ser := range iserials {
		for j, ser2 := range iserials {

			if i == j {
				continue
			}

			var diffs [26]int
			for i := 0; i < len(ser); i++ {
				diffs[i] = abs(ser[i] - ser2[i])
				diffs[i] = min(diffs[i], 1)
			}
			chk := 0
			for _, d := range diffs {
				chk += d
			}
			if chk == 1 {
				fmt.Println("Matching serials: ", i, j)
				fmt.Println("Serial 1: ", serials[i])
				fmt.Println("Serial 2: ", serials[j])
				part2 := ""
				for chridx, d := range diffs {
					if d == 0 {
						part2 += string(serials[i][chridx])
					}
				}
				fmt.Println("Part 2: ", part2) //mkcdflathzwsvjxrevymbdpoq
				return
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
