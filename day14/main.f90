program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, r, loops
    integer :: num_recipes = 0

    ! Holds 2 digits of number
    integer :: digits(2)

    !-- Puzzle inputs
    integer,parameter :: PUZZLE_INPUT = 190221
    integer,parameter :: PUZZLE_INPUT_DIGITS(6) = [ 1, 9, 0, 2, 2, 1]

    !-- Turn on/off pyramid printouts
    logical,parameter :: DEBUG_PRINTOUTS = .false.

    !-- MAIN VALUES
    integer,parameter :: MAX_RECIPES = 50000000

    !-- Recipe struct
    type :: Recipe
        integer :: score = 0
        integer :: elf = 0
        type(Recipe), pointer :: cw  => null()
        type(Recipe), pointer :: ccw => null()
    end type

    type(Recipe), target  :: recipes(MAX_RECIPES)
    type(Recipe), pointer :: elf1rec => recipes(1)
    type(Recipe), pointer :: elf2rec => recipes(2)

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Start timer
    call syslog % start_timer

    !-- Initialize
    recipes(1) % score = 3
    recipes(1) % elf = 1
    recipes(1) % cw  => recipes(2)
    recipes(1) % ccw => recipes(2)

    recipes(2) % score = 7
    recipes(2) % elf = 2
    recipes(2) % cw  => recipes(1)
    recipes(2) % ccw => recipes(1)
    num_recipes = 2

    if (DEBUG_PRINTOUTS) call write_recipe_status(2)
 
    loops = 0
    r = 3
    MAIN_LOOP: do

        loops = loops + 1

        !-- Generate scores of new recipes
        digits(:) = digits_of_recipe_sum(elf1rec,elf2rec)

        !-- Add new recipes
        if (digits(1) == 0) then

            ! Create 1 new recipe
            recipes(r)   % score =  digits(2)
            recipes(r)   % ccw   => recipes(r-1)
            recipes(r)   % cw    => recipes(1)
            recipes(r-1) % cw    => recipes(r)
            r = r + 1

            num_recipes = num_recipes + 1

        else
            
            ! Create 2 new recipes
            recipes(r)   % score =  digits(1)
            recipes(r)   % ccw   => recipes(r-1)
            recipes(r)   % cw    => recipes(r+1)
            recipes(r-1) % cw    => recipes(r)
            r = r + 1
            
            recipes(r)   % score =  digits(2)
            recipes(r)   % ccw   => recipes(r-1)
            recipes(r)   % cw    => recipes(1)
            recipes(r-1) % cw    => recipes(r)
            r = r + 1

            num_recipes = num_recipes + 2

        end if

        !-- Elves pick new recipes
        elf1rec % elf = 0
        do i = 1, 1 + elf1rec % score
            elf1rec => elf1rec % cw
        end do
        elf1rec % elf = 1
        
        elf2rec % elf = 0
        do i = 1, 1 + elf2rec % score
            elf2rec => elf2rec % cw
        end do
        elf2rec % elf = 2

        if (DEBUG_PRINTOUTS) call write_recipe_status(r-1)

        if (r == MAX_RECIPES-2) exit MAIN_LOOP

    end do MAIN_LOOP

    !call write_recipe_status(r-1)
    write (syslog%unit,'(a,i0,a)') 'Made ',num_recipes, ' recipes.'

    !-- Part 1
    write (syslog%unit,'(a)',advance='no') 'Part 1: '
    write (          *,'(a)',advance='no') 'Part 1: '
    do i = PUZZLE_INPUT+1, PUZZLE_INPUT + 10
        write (syslog%unit,'(i0)',advance='no') recipes(i) % score !1191216109
        write (          *,'(i0)',advance='no') recipes(i) % score !1191216109
    end do
    write (syslog%unit,*) ! advance
    write (          *,*) ! advance

    !-- Part 2
    CHECK: do i = 1, num_recipes

        if (recipes(i+0) % score == PUZZLE_INPUT_DIGITS(1) .and. &
            recipes(i+1) % score == PUZZLE_INPUT_DIGITS(2) .and. &
            recipes(i+2) % score == PUZZLE_INPUT_DIGITS(3) .and. &
            recipes(i+3) % score == PUZZLE_INPUT_DIGITS(4) .and. &
            recipes(i+4) % score == PUZZLE_INPUT_DIGITS(5) .and. &
            recipes(i+5) % score == PUZZLE_INPUT_DIGITS(6)) then
        
            write (syslog%unit,'(a,i0)') 'Part 2: ',i-1 !20268576
            write (          *,'(a,i0)') 'Part 2: ',i-1 !20268576
            exit CHECK

        end if

        if (i == num_recipes-6) then
            write (syslog%unit,'(a,i0,a)') 'Failed to find the puzzle input.'
        end if

    end do CHECK

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

contains

    subroutine write_recipe_status(rmax)
        implicit none
        integer,intent(in) :: rmax
        integer :: i
        character(len=1) :: open_paren, close_paren
        character(len=1),parameter :: ELF1_PAREN_OPEN = '('
        character(len=1),parameter :: ELF1_PAREN_CLOSE = ')'
        character(len=1),parameter :: ELF2_PAREN_OPEN = '['
        character(len=1),parameter :: ELF2_PAREN_CLOSE = ']'

        do i = 1, rmax

            select case (recipes(i) % elf)
            case (1)
                open_paren  = ELF1_PAREN_OPEN
                close_paren = ELF1_PAREN_CLOSE
            case (2)
                open_paren  = ELF2_PAREN_OPEN
                close_paren = ELF2_PAREN_CLOSE
            case default
                open_paren  = ' '
                close_paren = ' '
            end select

            write(syslog%unit,'(a,i1,a)',advance='no') &
                open_paren, recipes(i)%score, close_paren

        end do

        write(syslog%unit,*) ! advance

    end subroutine

    function digits_of_recipe_sum(recipe1,recipe2) result(digits)
        type(Recipe),intent(in)  :: recipe1
        type(Recipe),intent(in)  :: recipe2
        integer,dimension(2) :: digits
        integer              :: total

        total = recipe1 % score + recipe2 % score

        digits(1) = get_digit_of_number(2,total)
        digits(2) = get_digit_of_number(1,total)

    end function

    pure integer function get_digit_of_number(digit_place,num) result(digit)
        integer,intent(in) :: digit_place
        integer,intent(in) :: num
        digit = mod(int(num / 10**(digit_place-1)),10)
    end function

end program