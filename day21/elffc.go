package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	outputFortranName = "elfc.f90"
)

// optype
const (
	banr = 0
	muli = 1
	bori = 2
	setr = 3
	addi = 4
	eqrr = 5
	gtri = 6
	gtir = 7
	borr = 8
	eqri = 9
	bani = 10
	addr = 11
	eqir = 12
	mulr = 13
	seti = 14
	gtrr = 15
)

type Instruction struct {
	opcode string
	op     int
	a      int
	b      int
	c      int
}

func writeFortranOut(num int, ip_reg int, reg0 int, instr []Instruction) {

	f, err := os.Create(outputFortranName)
	check(err)
	defer f.Close()

	f.WriteString("program main\n")
	f.WriteString("implicit none\n")

	f.WriteString("! Registers\n")
	f.WriteString("integer :: ip = 0\n")
	f.WriteString("integer :: num_ops = 0\n")
	f.WriteString("integer,parameter :: NUM_REGISTERS = 6\n")
	f.WriteString("integer :: reg(0:NUM_REGISTERS-1) = 0\n")
	f.WriteString(fmt.Sprintf("integer :: reg0_0 = %d\n", reg0))

	f.WriteString("! Instruction Pointer Bound to Register 0\n")

	f.WriteString(fmt.Sprintf("integer,parameter :: NUM_INSTRUCTIONS = %d\n", num))
	f.WriteString(fmt.Sprintf("integer :: ip_reg = %d\n", ip_reg))

	f.WriteString(fmt.Sprintf("reg(0) = %d ! initial value of register 0\n", reg0))

	f.WriteString("\n")

	f.WriteString("11 continue ! reset\n")
	f.WriteString("if (num_ops > 0) then\n")
	f.WriteString("\tnum_ops = 0\n")
	f.WriteString("\treg0_0 = reg0_0 + 1\n")
	f.WriteString("\treg(0) = reg0_0\n")
	f.WriteString("\tip = 0\n")
	f.WriteString("end if\n")

	//f.WriteString("write (*,'(*(g0))') 'Initial reg(0): ',reg(0)\n")

	f.WriteString("if (mod(reg0_0,1000) == 0) write (*,'(*(g0))') 'Initial reg(0): ',reg(0)\n")

	f.Sync()

	for i, thisInstr := range instr {

		gotoIndex := i + 100

		f.WriteString(fmt.Sprintf("\n %d  reg(ip_reg) = ip\n", gotoIndex))

		switch thisInstr.op {
		case addr:
			f.WriteString("!addr\n")
			f.WriteString(fmt.Sprintf("reg(%d) = reg(%d) + reg(%d)\n", thisInstr.c, thisInstr.a, thisInstr.b))
			//reg(c) = reg(a) + reg(b)
		case addi:
			f.WriteString("!addi\n")
			f.WriteString(fmt.Sprintf("reg(%d) = reg(%d) + %d\n", thisInstr.c, thisInstr.a, thisInstr.b))
			//reg(c) = reg(a) + b
		case mulr:
			f.WriteString("!mulr\n")
			f.WriteString(fmt.Sprintf("reg(%d) = reg(%d) * reg(%d)\n", thisInstr.c, thisInstr.a, thisInstr.b))
			//reg(c) = reg(a) * reg(b)
		case muli:
			f.WriteString("!muli\n")
			f.WriteString(fmt.Sprintf("reg(%d) = reg(%d) * %d\n", thisInstr.c, thisInstr.a, thisInstr.b))
			//reg(c) = reg(a) * b
		case banr:
			f.WriteString("!banr\n")
			f.WriteString(fmt.Sprintf("reg(%d) = iand(reg(%d), reg(%d))\n", thisInstr.c, thisInstr.a, thisInstr.b))
			//reg(c) = iand(reg(a), reg(b))
		case bani:
			f.WriteString("!bani\n")
			f.WriteString(fmt.Sprintf("reg(%d) = iand(reg(%d), %d)\n", thisInstr.c, thisInstr.a, thisInstr.b))
			//reg(c) = iand(reg(a), b)
		case borr:
			f.WriteString("!borr\n")
			f.WriteString(fmt.Sprintf("reg(%d) = ior(reg(%d), reg(%d))\n", thisInstr.c, thisInstr.a, thisInstr.b))
			//reg(c) = ior(reg(a), reg(b))
		case bori:
			f.WriteString("!bori\n")
			f.WriteString(fmt.Sprintf("reg(%d) = ior(reg(%d), %d)\n", thisInstr.c, thisInstr.a, thisInstr.b))
			//reg(c) = ior(reg(a), b)
		case setr:
			f.WriteString("!setr\n")
			f.WriteString(fmt.Sprintf("reg(%d) = reg(%d)\n", thisInstr.c, thisInstr.a))
			//reg(c) = reg(a)
		case seti:
			f.WriteString("!seti\n")
			f.WriteString(fmt.Sprintf("reg(%d) = %d\n", thisInstr.c, thisInstr.a))
			//reg(c) = a
		case gtir:
			f.WriteString("!gtir\n")
			f.WriteString(fmt.Sprintf("if (%d > reg(%d)) then\n", thisInstr.a, thisInstr.b))
			f.WriteString(fmt.Sprintf("reg(%d) = 1\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("else\n"))
			f.WriteString(fmt.Sprintf("reg(%d) = 0\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("end if\n"))
			//if (a > reg(b)) then
			//	reg(c) = 1
			//else
			//	reg(c) = 0
			//end if
		case gtri:
			f.WriteString("!gtri\n")
			f.WriteString(fmt.Sprintf("if (reg(%d) > %d) then\n", thisInstr.a, thisInstr.b))
			f.WriteString(fmt.Sprintf("reg(%d) = 1\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("else\n"))
			f.WriteString(fmt.Sprintf("reg(%d) = 0\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("end if\n"))
			//if (reg(a) > b) then
			//	reg(c) = 1
			//else
			//	reg(c) = 0
			//end if
		case gtrr:
			f.WriteString("!gtrr\n")
			f.WriteString(fmt.Sprintf("if (reg(%d) > reg(%d)) then\n", thisInstr.a, thisInstr.b))
			f.WriteString(fmt.Sprintf("reg(%d) = 1\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("else\n"))
			f.WriteString(fmt.Sprintf("reg(%d) = 0\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("end if\n"))
			//if (reg(a) > reg(b)) then
			//	reg(c) = 1
			//else
			//	reg(c) = 0
			//end if
		case eqir:
			f.WriteString("!eqir\n")
			f.WriteString(fmt.Sprintf("if (%d == reg(%d)) then\n", thisInstr.a, thisInstr.b))
			f.WriteString(fmt.Sprintf("reg(%d) = 1\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("else\n"))
			f.WriteString(fmt.Sprintf("reg(%d) = 0\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("end if\n"))
			//if (a == reg(b)) then
			//	reg(c) = 1
			//else
			//	reg(c) = 0
			//end if
		case eqri:
			f.WriteString("!eqri\n")
			f.WriteString(fmt.Sprintf("if (reg(%d) == %d) then\n", thisInstr.a, thisInstr.b))
			f.WriteString(fmt.Sprintf("reg(%d) = 1\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("else\n"))
			f.WriteString(fmt.Sprintf("reg(%d) = 0\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("end if\n"))
			//if (reg(a) == b) then
			//	reg(c) = 1
			//else
			//	reg(c) = 0
			//end if
		case eqrr:
			f.WriteString("!eqrr\n")
			f.WriteString(fmt.Sprintf("if (reg(%d) == reg(%d)) then\n", thisInstr.a, thisInstr.b))
			f.WriteString(fmt.Sprintf("reg(%d) = 1\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("else\n"))
			f.WriteString(fmt.Sprintf("reg(%d) = 0\n", thisInstr.c))
			f.WriteString(fmt.Sprintf("end if\n"))
			//if (reg(a) == reg(b)) then
			//	reg(c) = 1
			//else
			//	reg(c) = 0
			//end if
		default:
			fmt.Println("Unknown opcode", thisInstr.op)
			os.Exit(1)
		}

		f.WriteString("num_ops = num_ops + 1\n")
		f.WriteString("ip = reg(ip_reg)\n")
		f.WriteString("ip = ip + 1\n")

		f.WriteString("if (ip > NUM_INSTRUCTIONS-1) then\n")
		f.WriteString(fmt.Sprintf("\twrite (*,'(*(g0))') 'Register %d: ',reg(ip_reg)\n", ip_reg))
		f.WriteString("\twrite (*,'(*(g0))') 'Init reg(0): ',reg0_0\n")
		f.WriteString("\twrite (*,'(*(g0))') 'reg(0): ',reg(0)\n")
		//f.WriteString("\twrite (*,'(*(g0))') 'reg(1): ',reg(1)\n")
		//f.WriteString("\twrite (*,'(*(g0))') 'reg(2): ',reg(2)\n")
		//f.WriteString("\twrite (*,'(*(g0))') 'reg(3): ',reg(3)\n")
		//f.WriteString("\twrite (*,'(*(g0))') 'reg(4): ',reg(4)\n")
		f.WriteString("\twrite (*,'(*(g0))') 'reg(5): ',reg(5)\n")
		f.WriteString("\twrite (*,'(*(g0))') 'Num Ops: ',num_ops\n")
		f.WriteString("if (num_ops > 1000) goto 11\n") //16457176
		//f.WriteString("\tstop\n")
		f.WriteString("end if\n")

		f.WriteString("if (num_ops > 5000) goto 11\n") //16457176

		//f.WriteString("goto (100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115), ip + 1 \n")
		f.WriteString("goto (")
		for i := 0; i < len(instr); i++ {
			f.WriteString(fmt.Sprintf("%d", i+100))
			if i < len(instr)-1 {
				f.WriteString(",")
			}
		}
		f.WriteString("), ip + 1 \n")
	}

	f.WriteString("end program \n")

}

func main() {

	// Read elf assembly file
	filename := "day21.txt"
	if len(os.Args) > 1 {
		filename = os.Args[1]
	} else {
		fmt.Println("Error: Must specify input file.")
		os.Exit(1)
	}
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	var instr []Instruction
	numInstructions := -1
	ip := -1

	reg0 := 0
	if len(os.Args) >= 2 {
		reg0, _ = strconv.Atoi(os.Args[2])
		fmt.Println("Overriding initial reg0 value with ", reg0)
	}

	for scanner.Scan() {

		line := scanner.Text()
		instrWords := strings.Fields(line)

		numInstructions += 1

		// Read instruction pointer first: #ip 1
		if numInstructions == 0 {
			ip_in, _ := strconv.Atoi(instrWords[1])
			ip = ip_in

			// Read instruction
		} else {

			opcode := instrWords[0]
			a, _ := strconv.Atoi(instrWords[1])
			b, _ := strconv.Atoi(instrWords[2])
			c, _ := strconv.Atoi(instrWords[3])

			optype := -1
			switch opcode {
			case "addr":
				optype = addr
			case "addi":
				optype = addi
			case "mulr":
				optype = mulr
			case "muli":
				optype = muli
			case "banr":
				optype = banr
			case "bani":
				optype = bani
			case "borr":
				optype = borr
			case "bori":
				optype = bori
			case "setr":
				optype = setr
			case "seti":
				optype = seti
			case "gtir":
				optype = gtir
			case "gtri":
				optype = gtri
			case "gtrr":
				optype = gtrr
			case "eqir":
				optype = eqir
			case "eqri":
				optype = eqri
			case "eqrr":
				optype = eqrr
			default:
				fmt.Println("Unknown opcode", instrWords[0])
				os.Exit(1)
			}

			// Define instruction
			instr = append(instr,
				Instruction{
					opcode: opcode,
					a:      a,
					b:      b,
					c:      c,
					op:     optype,
				},
			)
		}
	}

	fmt.Println("Read ", numInstructions, "instructions")
	fmt.Println("ip=", ip)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	writeFortranOut(numInstructions, ip, reg0, instr)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
