program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use elfcode_mod
    implicit none

    !-- Counters
    integer :: i

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Puzzle answers
    integer :: part1 = 0
    integer :: part2 = 0

    !-- Saved answers
    integer,parameter :: MAX_SEEN_ITERATIONS = 10000000
    integer :: seen(MAX_SEEN_ITERATIONS) = 0
    integer :: iter = 0

    ! Number of instructions in input file
    integer :: num_instr

    integer :: reg_id = -1

    logical :: debug_printouts = .false.

    !-- Input file reading properties
    integer,parameter            :: max_line_len = 50
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)
    num_instr = num_lines - 1

    !-- Allocate accordingly
    allocate(instructions(0:num_instr-1))

    !-- Start timer
    call syslog % start_timer

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    
    !-- Read instruction pointer, e.g., #ip 0
    read (input_unit,'(a)') line
    if (line(1:1) == '#') then

        ! Strip #ip
        line(1:3) = ' '

        ! Get register #
        read (line,*) reg_id
        
        ! Pointer assignment
        ip % reg_ptr => reg(reg_id)

        write (syslog%unit,*) 'IP => ', reg_id
    else
        write (syslog%unit,*) 'Failed reading instruction pointer'
        stop
    end if

    !-- Read instructions, e.g., seti 5 0 1
    READ_INSTRUCTIONS: do i = 0, num_instr-1

        ! Read line
        read (input_unit,'(a)') line

        instructions(i) % opc = line(1:4)

        line(1:4) = ' '

        select case (instructions(i) % opc)
            case ('banr'); instructions(i) % op = 00 
            case ('muli'); instructions(i) % op = 01 
            case ('bori'); instructions(i) % op = 02 
            case ('setr'); instructions(i) % op = 03 
            case ('addi'); instructions(i) % op = 04 
            case ('eqrr'); instructions(i) % op = 05 
            case ('gtri'); instructions(i) % op = 06 
            case ('gtir'); instructions(i) % op = 07 
            case ('borr'); instructions(i) % op = 08 
            case ('eqri'); instructions(i) % op = 09 
            case ('bani'); instructions(i) % op = 10
            case ('addr'); instructions(i) % op = 11
            case ('eqir'); instructions(i) % op = 12
            case ('mulr'); instructions(i) % op = 13
            case ('seti'); instructions(i) % op = 14
            case ('gtrr'); instructions(i) % op = 15
        end select

        line(1:4) = ' '

        read(line,*) instructions(i) % a, instructions(i) % b, instructions(i) % c

    end do READ_INSTRUCTIONS
    close (input_unit)

    if (debug_printouts) then
        do i = 0, num_instr-1
            write (syslog%unit,*)                                       &
            i, instructions(i) % opc, instructions(i) % op,             &
            instructions(i) % a, instructions(i) % b, instructions(i) % c
        end do
    end if

    EXE: do

        if (debug_printouts) then
            write (syslog%unit,'(a,i2,a,6i8,a,a,3i3)',advance='no') &
                'ip=', ip % value, '[',         &
                reg(0),                 &
                reg(1),                 &
                reg(2),                 &
                reg(3),                 &
                reg(4),                 &
                reg(5), '] ',           &
                instructions(ip % value) % opc, &
                instructions(ip % value) % a,   &
                instructions(ip % value) % b,   &
                instructions(ip % value) % c
            flush (syslog%unit)
        end if

        !-- Optimization for my input
        if (ip % value == 28) then

            ! Instruction 28 `eqrr 5 0 2' looks for reg(5) == reg(0)
            ! Therefore, because reg(0) is not touched until this time,
            ! reg(0) must be initialized to value of reg(5) at the time
            ! of this instruction
            if (part1 == 0) then
                part1 = reg(5)
                write (syslog%unit,'(a,i0)') 'Part 1: ', part1 ! 16457176
                write (          *,'(a,i0)') 'Part 1: ', part1 ! 16457176
            end if
            
            do i = 1, iter
                if (reg(5) == seen(i)) then
                    part2 = seen(iter)
                    write (syslog%unit,'(a,i0)') 'Part 2: ', part2 ! Part 2: 13625951
                    write (          *,'(a,i0)') 'Part 2: ', part2
                    exit EXE
                end if
            end do
            iter = iter + 1
            if (iter > MAX_SEEN_ITERATIONS) then
                write (*,'(a)') 'Error: Exceeded MAX_SEEN_ITERATIONS'
            end if
            seen(iter) = reg(5)

        end if

        !-- Normal process
        call instructions(ip % value) % exec

        if (debug_printouts) then
            write (syslog%unit,'(a,6i8,a,i2)') &
                '[',        &
                reg(0),     &
                reg(1),     &
                reg(2),     &
                reg(3),     &
                reg(4),     &
                reg(5), '] ip=', ip % value
            flush (syslog%unit)
        end if
 
        if (ip % value > num_instr-1) exit EXE

    end do EXE

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program