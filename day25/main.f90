program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, j, cix

    !-- New point inputs
    integer :: new_x, new_y, new_z, new_t

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Size of stars(:) array
    integer :: size_stars

    !-- Number of constellations
    integer :: num_constellations = 0

    !-- Number of remaining ungrouped stars
    integer :: num_remaining = 0

    logical :: added_to_const = .false.

    !-- Puzzle Variables
    type :: Star
        integer :: x
        integer :: y
        integer :: z
        integer :: t
        logical :: grouped = .false.
    end type
    type(Star),allocatable :: stars(:)

    type :: Constellation
        type(Star),allocatable :: pts(:)
    end type
    type(Constellation),allocatable :: constellations(:)

    ! Input file reading properties
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    !allocate(stars(num_lines))

    ! Note: This code is intentionally slow and intentionally uses
    ! dynamic array reallocation in hot loops to try to fairly
    ! compare with the performance of the Go version.

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    write (syslog%unit,*) 'Raw Points:'
    write (syslog%unit,*) '  ID   X   Y'
    do i = 1, num_lines
        
        !-- Read
        read (input_unit,*) new_x, new_y, new_z, new_t

        !-- Write to log
        write (syslog%unit,'(4i5)') new_x, new_y, new_z, new_t

        if (allocated(stars)) then
            stars = [ stars, Star(x=new_x,y=new_y,z=new_z,t=new_t) ]
        else
            stars = [ Star(x=new_x,y=new_y,z=new_z,t=new_t) ]
        end if

    end do
    close (input_unit)

    !-- Start timer
    call syslog % start_timer

    size_stars = size(stars,1)

    !-- Initialize first constellation with first point
    num_remaining = size(stars,1)
    stars(1) % grouped = .true.
    constellations = [ Constellation(pts=[stars(1)]) ]
    num_constellations = num_constellations + 1
    added_to_const = .false.

    do

        num_remaining = 0
        do i = 1, size_stars
            if (.not. stars(i) % grouped) num_remaining = num_remaining + 1
        end do
        if (num_remaining == 0) exit

        !write (*,'(*(g0))') "Remaining points: ", num_remaining

        added_to_const = .false.
        do cix = 1, size(constellations,1)
            do i = 1, size(constellations(cix) % pts,1)
                do j = 1, size_stars
                    if (stars(j) % grouped) cycle

                    if (manhattan_distance(stars(j), constellations(cix) % pts(i)) <= 3) then
                        stars(j) % grouped = .true.
                        constellations(cix) % pts = [constellations(cix) % pts, stars(j)]
                        added_to_const = .true.
                    end if
        	    end do
            end do
        end do
        ! Make new constellation
        if (.not. added_to_const) then
            do j = 1, size_stars
                if (.not. stars(j) % grouped) then
                    constellations = [constellations, Constellation(pts=[stars(j)]) ]
                    stars(j) % grouped = .true.
                    num_constellations = num_constellations + 1
                    exit
                end if
            end do
        end if
    end do
    
    write (*,'(*(g0))') "Part 1: ", num_constellations ! 352
    
    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

contains

    integer function manhattan_distance(pt0, pt1) result (dist)
        type(Star),intent(in) :: pt0
        type(Star),intent(in) :: pt1
        dist = abs(pt0%x-pt1%x) + abs(pt0%y-pt1%y) + abs(pt0%z-pt1%z) + abs(pt0%t-pt1%t)
    end function

end program