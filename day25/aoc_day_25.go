package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func manhattanDistance(pt0, pt1 Point) int {
	return abs(pt1.x-pt0.x) + abs(pt1.y-pt0.y) + abs(pt1.z-pt0.z) + abs(pt1.t-pt0.t)
}

// Constellation contains 4-dimensional points in spacetime that are clustered together
type Constellation struct {
	pts []Point
}

// Point contains a 4-dimensional point in spacetime
type Point struct {
	x, y, z, t int
	grouped    bool
}

func main() {

	// Open file
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reader := csv.NewReader(file)

	// Read points from file
	var points []Point
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		var pt Point
		pt.x, err = strconv.Atoi(record[0])
		pt.y, err = strconv.Atoi(record[1])
		pt.z, err = strconv.Atoi(record[2])
		pt.t, err = strconv.Atoi(record[3])
		pt.grouped = false
		//fmt.Println(record)
		if err != nil {
			log.Fatal(err)
		}
		points = append(points, pt)
	}

	// Determine constellations
	var constellations []Constellation

	// Initialize first constellation with first point
	p := Constellation{}
	points[0].grouped = true
	p.pts = append(p.pts, points[0])
	constellations = append(constellations, p)
	numConstellations := 1
	addedToConst := false

	for {

		remainingPoints := 0
		for _, p := range points {
			if !p.grouped {
				remainingPoints += 1
			}
		}
		if remainingPoints == 0 {
			break
		}
		//fmt.Println("Remaining points: ", remainingPoints)

		addedToConst = false
		for cix, cons := range constellations {
			for _, constPt := range cons.pts {
				for j, otherPt := range points {
					if otherPt.grouped {
						continue
					}
					if manhattanDistance(constPt, otherPt) <= 3 {
						points[j].grouped = true
						constellations[cix].pts = append(constellations[cix].pts, points[j])
						addedToConst = true
					}
				}
			}
		}
		// Make new constellation
		if !addedToConst {
			for j, otherPt := range points {
				if !otherPt.grouped {
					p := Constellation{}
					p.pts = append(p.pts, points[j])
					points[j].grouped = true
					constellations = append(constellations, p)
					numConstellations += 1
					break
				}
			}
		}
	}
	fmt.Println("Answer: ", numConstellations) // 352
}
