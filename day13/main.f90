program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use cart_mod
    implicit none

    !-- Counters
    integer :: i, j, x, y

    !-- Time step
    integer :: tick = 0

    !-- Input file unit
    integer :: input_unit

    !-- (x,y) position
    integer :: pos(2)

    !-- Number of lines in input file
    integer :: num_lines

    !-- Number of killed off carts
    integer :: dead_carts = 0

    !-- Termination flags
    logical :: part1_complete = .false.
    logical :: part2_complete = .false.

    ! Input file reading properties
    integer,parameter            :: max_line_len = 150
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)
    grid_y = num_lines - 1

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    
    !-- Start timer
    call syslog % start_timer

    !-- Initialize
    ROTATE_CW  = reshape(ROTATE_CW_0 , shape(ROTATE_CW))
    ROTATE_CCW = reshape(ROTATE_CCW_0, shape(ROTATE_CCW))

    !-- Read track setup
    x = 0
    y = 0
    do i = 1, num_lines

        ! Read line
        read (input_unit,'(a)') line

        ! Grid x-dimension is obtained from the length
        ! of first row, then we can allocate the grid
        if (i == 1) then
            grid_x = len(line) - 1
            allocate(track(0:grid_x,0:grid_y))
        end if

        ! Build grid
        x = 0
        do j = 1, len(line)

            track(x,y) % x = x
            track(x,y) % y = y
            track(x,y) % a = line(j:j)

            ! Define track
            select case (line(j:j))
                case('-');     track(x,y) % track_type = TrackTypeStraightHorz
                case('|');     track(x,y) % track_type = TrackTypeStraightVert
                case('/');     track(x,y) % track_type = TrackTypeCurve1
                case('\');     track(x,y) % track_type = TrackTypeCurve2
                case('+');     track(x,y) % track_type = TrackTypeIntersection
                case('v','^'); track(x,y) % track_type = TrackTypeStraightVert
                case('<','>'); track(x,y) % track_type = TrackTypeStraightHorz
                case default;  track(x,y) % track_type = TrackTypeNone
            end select

            ! Define cart (if applicable)
            select case (line(j:j))
                case('v','^','<','>')
                    num_carts = num_carts + 1
                    carts(num_carts) % id = num_carts
                    carts(num_carts) % position = [ x, y ]
                    track(x,y) % cart => carts(num_carts)
                case default
            end select

            ! Direct cart (if applicable)
            select case (line(j:j))
                case('v')
                    carts(num_carts) % direction = DIRECTION_DOWN
                    call carts(num_carts) % write_info
                case('^')
                    carts(num_carts) % direction = DIRECTION_UP
                    call carts(num_carts) % write_info
                case('<')
                    carts(num_carts) % direction = DIRECTION_LEFT
                    call carts(num_carts) % write_info
                case('>')
                    carts(num_carts) % direction = DIRECTION_RIGHT
                    call carts(num_carts) % write_info
                case default
            end select

            if (num_carts > MAX_NUM_CARTS) then
                write (syslog%unit,*) 'ERROR: Exceeded maximum number of carts.'
                call bomb
            end if
            x = x + 1
        end do
        y = y + 1
    end do
    close (input_unit)

    !-- Write to log
    call syslog%log(__FILE__,'Found '//string(num_carts)// ' carts.')
    call write_state

    !-- Simulation
    tick = 0
    TICK_LOOP: do

        tick = tick + 1

        Y_LOOP: do y = 0, grid_y
            X_LOOP: do x = 0, grid_x

                ! No cart here
                if (.not. associated(track(x,y) % cart)) cycle X_LOOP

                ! Cart here but dead
                !if (track(x,y) % cart % dead) cycle X_LOOP

                ! Already processed this cart this tick
                if (track(x,y) % cart % touched_last_tick == tick) cycle X_LOOP
                
                ! Log
                pos = track(x,y) % cart % xy_pos()
               !write (syslog%unit,'(4(a,i0))')              &
               !    'Updating cart ',track(x,y) % cart % id, &
               !    ' at position (', pos(1),',',pos(2),')'

                ! Update cart
                track(x,y) % cart % touched_last_tick = tick

                ! Update cart position
                if      (track(x,y) % track_type == TrackTypeIntersection) then
                    
                    call track(x,y) % cart % turn
                    pos = track(x,y) % cart % xy_pos()
                   !write (syslog%unit,'(a,i0,a,i0,a)') 'New position: (',pos(1),',',pos(2),')'
                
                else if (track(x,y) % track_type == TrackTypeStraightVert .or. &
                         track(x,y) % track_type == TrackTypeStraightHorz) then
                    
                    track(x,y) % cart % position = track(x,y) % cart % position + track(x,y) % cart % direction
                    pos = track(x,y) % cart % xy_pos()
                   !write (syslog%unit,'(a,i0,a,i0,a)') 'New position: (',pos(1),',',pos(2),')'
                    
                else if (track(x,y) % track_type == TrackTypeCurve1) then

                    if      (all(track(x,y) % cart % direction == DIRECTION_UP)) then
                        track(x,y) % cart % direction = DIRECTION_RIGHT
                    else if (all(track(x,y) % cart % direction == DIRECTION_DOWN)) then
                        track(x,y) % cart % direction = DIRECTION_LEFT
                    else if (all(track(x,y) % cart % direction == DIRECTION_LEFT)) then
                        track(x,y) % cart % direction = DIRECTION_DOWN
                    else if (all(track(x,y) % cart % direction == DIRECTION_RIGHT)) then
                        track(x,y) % cart % direction = DIRECTION_UP
                    else
                        write (syslog%unit,'(a,i0)') 'Unknown failure'
                        call bomb
                    end if
                    track(x,y) % cart % position = track(x,y) % cart % position + track(x,y) % cart % direction
                    pos = track(x,y) % cart % xy_pos()
                   !write (syslog%unit,'(a,i0,a,i0,a)') 'New position: (',pos(1),',',pos(2),')'
                
                else if (track(x,y) % track_type == TrackTypeCurve2) then

                    if      (all(track(x,y) % cart % direction == DIRECTION_UP)) then
                        track(x,y) % cart % direction = DIRECTION_LEFT
                    else if (all(track(x,y) % cart % direction == DIRECTION_DOWN)) then
                        track(x,y) % cart % direction = DIRECTION_RIGHT
                    else if (all(track(x,y) % cart % direction == DIRECTION_LEFT)) then
                        track(x,y) % cart % direction = DIRECTION_UP
                    else if (all(track(x,y) % cart % direction == DIRECTION_RIGHT)) then
                        track(x,y) % cart % direction = DIRECTION_DOWN
                    else
                        write (syslog%unit,'(a,i0)') 'Unknown failure'
                        call bomb
                    end if
                    track(x,y) % cart % position = track(x,y) % cart % position + track(x,y) % cart % direction
                    pos = track(x,y) % cart % xy_pos()
                   !write (syslog%unit,'(a,i0,a,i0,a)') 'New position: (',pos(1),',',pos(2),')'
                
                else
                    write (syslog%unit,'(4(a,i0))') &
                    'Update failure for cart ',track(x,y) % cart % id, &
                    ' at position (', x,',',y,')'
                    call bomb
                end if

                ! Point new location at cart if space is empty
                if (.not. associated(track(pos(1),pos(2)) % cart)) then
                    
                    track(pos(1),pos(2)) % cart => track(x,y) % cart

                    ! Update future track's data
                    if      (all(track(x,y) % cart % direction == DIRECTION_DOWN)) then
                        track(pos(1),pos(2)) % a = 'v'
                    else if (all(track(x,y) % cart % direction == DIRECTION_UP)) then
                        track(pos(1),pos(2)) % a = '^'
                    else if (all(track(x,y) % cart % direction == DIRECTION_LEFT)) then
                        track(pos(1),pos(2)) % a = '<'
                    else if (all(track(x,y) % cart % direction == DIRECTION_RIGHT)) then
                        track(pos(1),pos(2)) % a = '>'
                    else
                        write (syslog%unit,*) &
                            'ERROR: Unknown failure updating track'
                        write (syslog%unit,*) &
                            'Failure point: ('//string(x)//','//string(y)//')'
                        call bomb
                    end if
                
                else

                    ! Kill 2 carts
                    track(x,y) % cart % dead = .true.
                    track(pos(1),pos(2)) % cart % dead = .true.
                    track(pos(1),pos(2)) % a = 'X'
                    write (syslog%unit,'(3(a,i0))') 'Crash at (',pos(1),',',pos(2),') at tick ',tick
                    if (.not. part1_complete) then
                        write (syslog%unit,'(a,i0,a,i0)') 'Part 1: ',pos(1),',',pos(2) !16,45
                        write (          *,'(a,i0,a,i0)') 'Part 1: ',pos(1),',',pos(2) !16,45
                        part1_complete = .true.
                    end if

                    ! Clear out both carts
                    track(x,y) % cart => null()
                    track(pos(1),pos(2)) % cart => null() 

                    dead_carts = dead_carts + 2

                    ! Check if this was the last crash
                    if (dead_carts == num_carts - 1) then
                        write (syslog%unit,'(a,i0)') 'Final crash occurred at tick ',tick
                        part2_complete = .true.
                    end if

                end if

                ! Update this track's data
                select case (track(x,y) % track_type)
                    case (TrackTypeStraightHorz); track(x,y) % a = '-'
                    case (TrackTypeStraightVert); track(x,y) % a = '|'
                    case (TrackTypeCurve1);       track(x,y) % a = '/'
                    case (TrackTypeCurve2);       track(x,y) % a = '\'
                    case (TrackTypeIntersection); track(x,y) % a = '+'
                    case default
                        write (syslog%unit,*) &
                            'ERROR: Unknown failure updating track'
                        write (syslog%unit,*) &
                            'Failure point: ('//string(x),','//string(y)//')'
                        call bomb
                end select

                ! Stop pointing at cart
                track(x,y) % cart => null()

            end do X_LOOP
        end do Y_LOOP

        !call write_state
        !if (tick == 14) exit TICK_LOOP
        if (part2_complete) exit TICK_LOOP

    end do TICK_LOOP

    !-- Find last remaining cart
    Y_SRCH: do y = 0, grid_y
            X_SRCH: do x = 0, grid_x
                if (associated(track(x,y) % cart)) then
                   !track(x,y) % a = '*'
                    call write_state
                    write (syslog%unit,'(a,i0,a,i0)') 'Part 2: ',x,',',y !21,91
                    write (          *,'(a,i0,a,i0)') 'Part 2: ',x,',',y !21,91
                end if
            end do X_SRCH
    end do Y_SRCH

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

contains

    pure integer function get_digit_of_number(digit_place,num) result(digit)
        integer,intent(in) :: digit_place
        integer,intent(in) :: num
        digit = mod(int(num / 10**(digit_place-1)),10)
    end function

    !> Write out the current state of track
    subroutine write_state
        implicit none
        integer :: x, y

        write (syslog%unit,'(a,i0)') 'Time: ', tick

        !-- Write header
        ! Hundreds
        write (syslog%unit,'(a)', advance='no') '   '
        do x = 0, grid_x
            write (syslog%unit,'(i1)',advance='no') get_digit_of_number(3,x)
        end do
        write (syslog%unit,*) ! advance
        ! Tens
        write (syslog%unit,'(a)', advance='no') '   '
        do x = 0, grid_x
            write (syslog%unit,'(i1)',advance='no') get_digit_of_number(2,x)
        end do
        write (syslog%unit,*) ! advance
        ! Ones
        write (syslog%unit,'(a)', advance='no') '   '
        do x = 0, grid_x
            write (syslog%unit,'(i1)',advance='no') get_digit_of_number(1,x)
        end do
        write (syslog%unit,*) ! advance

        !-- Grid
        do y = 0, grid_y
            write (syslog%unit,'(i0.3)',advance='no') y
            do x = 0, grid_x
                write (syslog%unit,'(a)',advance='no') track(x,y) % a
            end do
            write (syslog%unit,*) ! advance
        end do
    end subroutine
end program