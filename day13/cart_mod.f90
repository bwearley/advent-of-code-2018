module cart_mod
    use syslog_mod
    use string_tools_mod
    implicit none

    !-- Direction Matrices
    integer,dimension(2),parameter :: DIRECTION_DOWN  = [  0, +1 ]
    integer,dimension(2),parameter :: DIRECTION_UP    = [  0, -1 ]
    integer,dimension(2),parameter :: DIRECTION_LEFT  = [ -1,  0 ]
    integer,dimension(2),parameter :: DIRECTION_RIGHT = [ +1,  0 ]

    !-- Rotation Matrices
    integer :: ROTATE_CW_0(4) = [0, 1, -1, 0]
    integer :: ROTATE_CCW_0(4) = [0, -1, 1, 0]
    integer :: ROTATE_CW(2,2)
    integer :: ROTATE_CCW(2,2)

    !-- Number of carts on track
    integer :: num_carts = 0
    integer,parameter :: MAX_NUM_CARTS = 20

    !-- Grid dimensions
    integer :: grid_x = 0
    integer :: grid_y = 0

    !-- Turn definitions
    enum,bind(C)
        enumerator :: TurnLeft
        enumerator :: TurnStraight
        enumerator :: TurnRight
    end enum

    !-- Track type definitions
    enum,bind(C)
        enumerator :: TrackTypeNone
        enumerator :: TrackTypeStraightHorz
        enumerator :: TrackTypeStraightVert
        enumerator :: TrackTypeCurve1
        enumerator :: TrackTypeCurve2
        enumerator :: TrackTypeIntersection
    end enum

    !-- Track Segment struct
    type :: TrackSegment
        integer :: x = -1
        integer :: y = -1
        integer :: track_type = TrackTypeNone
        character(len=1) :: a
        type(Cart),pointer :: cart => null()
    end type

    type(TrackSegment), allocatable :: track(:,:)

    type :: Cart
        integer :: id = -1
        !complex :: position = complex(0,0)
        integer :: position(2) = 0
        integer :: next_turn = TurnLeft
        !complex :: direction = complex(0,0)
        integer :: direction(2) = 0
        logical :: dead = .false.
        integer :: touched_last_tick = 0
    contains
        procedure :: write_info
        procedure :: turn
        procedure :: xy_pos
    end type

    type(Cart), target :: carts(MAX_NUM_CARTS)

contains

    subroutine turn(self)
        implicit none
        class(Cart) :: self

        select case (self % next_turn)

            ! Turn left: counter-clockwise
            case (TurnLeft)
                call flush(syslog%unit)
                self % direction = matmul(rotate_ccw,self % direction)
                self % next_turn = TurnStraight

            ! Go straight
            case (TurnStraight)
                self % direction = self % direction
                self % next_turn = TurnRight

            ! Turn right: clockwise
            case (TurnRight)
                self % direction = matmul(rotate_cw,self % direction)
                self % next_turn = TurnLeft

            case default
                call syslog%log(__FILE__, &
                    'ERROR: Bad turn direction for cart ID: '//string(self % id))

        end select
        
        ! Perform position change
        self % position = self % position + self % direction

    end subroutine

    subroutine write_info(self)
        implicit none
        class(Cart) :: self
        integer :: pos(2)

        pos = self % xy_pos()

        write (syslog%unit,'(5(a,i0))') &
            'Cart ', self % id, &
            ' at (',pos(1), &
            ',',pos(2), &
            ') with direction ', &
            self % direction(1), &
            ',', &
            self % direction(2)
    
    end subroutine

    function xy_pos(self) result(xy)
        implicit none
        class(Cart) :: self
        integer     :: xy(2)
        xy(1) = self % position(1)
        xy(2) = self % position(2)
    end function

end module
