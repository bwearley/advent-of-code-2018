module map_mod
    use syslog_mod
    use string_tools_mod
    use get_digit_of_number_mod
    implicit none

    ! Map Dimensions
    integer :: x_dim_min = 0
    integer :: y_dim_min = 0
    integer :: x_dim_max = 0
    integer :: y_dim_max = 0

    !-- Map Square Type Definitions
    enum,bind(c)
        enumerator :: Room
        enumerator :: DoorHorz
        enumerator :: DoorVert
        enumerator :: Wall
        enumerator :: Unknown
    end enum

    !-- MapSquare struct
    type :: MapSquare
        integer :: x = -1
        integer :: y = -1
        integer :: type = Unknown
        integer :: dist = 0
        type(MapSquare), pointer :: N => null()
        type(MapSquare), pointer :: S => null()
        type(MapSquare), pointer :: E => null()
        type(MapSquare), pointer :: W => null()
    contains
        procedure :: a
    end type
    type(MapSquare), allocatable, target :: map(:,:)
    type(MapSquare), pointer :: curr => null()

contains

    !> Character value of a coordinate
    function a(self)
        class(MapSquare) :: self
        character(len=1) :: a
        select case (self % type)
        case (Room)
            a = '.'
        case (DoorVert)
            a = '|'
        case (DoorHorz)
            a = '-'
        case (Wall)
            a = '#'
        case (Unknown)
            a = '?'
        case default
        end select
        if (self%x == 0 .and. self%y == 0) a = 'X'
    end function

    !> Write out the current state of map
    subroutine write_state(unit)
        implicit none
        integer,intent(in),optional :: unit
        integer :: u
        integer :: x, y

        if (present(unit)) then
            u = unit
        else
            u = syslog%unit
        end if

        !-- Write header
        ! Thousands Place
        write (u,'(a)', advance='no') '     '
        do x = x_dim_min, x_dim_max
            write (u,'(i1)',advance='no') abs(get_digit_of_number(4,x))
        end do
        write (u,*) ! advance
        ! Hundreds Place
        write (u,'(a)', advance='no') '     '
        do x = x_dim_min, x_dim_max
            write (u,'(i1)',advance='no') abs(get_digit_of_number(3,x))
        end do
        write (u,*) ! advance
        ! Tens Place
        write (u,'(a)', advance='no') '     '
        do x = x_dim_min, x_dim_max
            write (u,'(i1)',advance='no') abs(get_digit_of_number(2,x))
        end do
        write (u,*) ! advance
        ! Ones Place
        write (u,'(a)', advance='no') '     '
        do x = x_dim_min, x_dim_max
            write (u,'(i1)',advance='no') abs(get_digit_of_number(1,x))
        end do
        write (u,*) ! advance
        write (u,*) ! advance

        !-- Grid
        do y = y_dim_min, y_dim_max
            write (u,'(i0.4,a)',advance='no') abs(y), ' '
            do x = x_dim_min, x_dim_max
                write (u,'(a)',advance='no') map(x,y) % a()
            end do
            write (u,*) ! advance
        end do
    end subroutine

end module
