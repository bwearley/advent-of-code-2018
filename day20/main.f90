program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use Queue_mod
    use map_mod
    implicit none

    !-- Counters
    integer :: i
    integer :: x, y

    !-- Input file unit
    integer :: input_unit

    !-- Puzzle values
    integer :: part1 = 0
    integer :: part2 = 0
    integer,allocatable :: dist_filter(:,:)

    integer :: prev_dist = 0

    type(QITEM) :: starts_q(0:100000)
    type(QITEM) :: ends_q(0:100000)
    type(QITEM) :: node
    integer :: starts_first = 0, starts_last = 0
    integer :: ends_first = 0, ends_last = 0

    ! Input file reading properties
    integer,parameter            :: max_line_len = 15000
    character(len=max_line_len)  :: input_line
    character(len=:),allocatable :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')
    
    !-- Start timer
    call syslog % start_timer

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    read (input_unit,'(a)') input_line
    close (input_unit)
    line = input_line(2:len(trim(input_line))-1)

    if (.true.) then
        write (syslog%unit,'(a)') ' Read line:'
        write (syslog%unit,'(a)') line
    end if

    !-- Allocate a map: don't know dimensions yet
    x_dim_min = -200
    x_dim_max = +200
    y_dim_min = -200
    y_dim_max = +200
    allocate(map        (x_dim_min:x_dim_max,y_dim_min:y_dim_max))
    allocate(dist_filter(x_dim_min:x_dim_max,y_dim_min:y_dim_max))

    !-- Initialize Links
    do y = y_dim_min, y_dim_max
        do x = x_dim_min, x_dim_max
            map(x,y) % x = x
            map(x,y) % y = y
            if (y /= y_dim_min) map(x,y) % N => map(x  ,y-1)
            if (y /= y_dim_max) map(x,y) % S => map(x  ,y+1)
            if (x /= x_dim_min) map(x,y) % W => map(x-1,y  )
            if (x /= x_dim_max) map(x,y) % E => map(x+1,y  )
        end do
    end do

    !-- Main Processing
    curr => map(0,0)
    do i = 1, len(line)

        select case (line(i:i))

            case ('N')
                prev_dist = curr % dist
                curr => curr % N
                curr % type = DoorHorz
                curr => curr % N
                curr % type = Room
                if (curr % dist /= 0) then
                    curr % dist = min(curr % dist, prev_dist + 1)
                else
                    curr % dist = prev_dist + 1
                end if

            case ('S')
                prev_dist = curr % dist
                curr => curr % S
                curr % type = DoorHorz
                curr => curr % S
                curr % type = Room
                if (curr % dist /= 0) then
                    curr % dist = min(curr % dist, prev_dist + 1)
                else
                    curr % dist = prev_dist + 1
                end if

            case ('W')
                prev_dist = curr % dist
                curr => curr % W
                curr % type = DoorVert
                curr => curr % W
                curr % type = Room
                if (curr % dist /= 0) then
                    curr % dist = min(curr % dist, prev_dist + 1)
                else
                    curr % dist = prev_dist + 1
                end if

            case ('E')
                prev_dist = curr % dist
                curr => curr % E
                curr % type = DoorVert
                curr => curr % E
                curr % type = Room
                if (curr % dist /= 0) then
                    curr % dist = min(curr % dist, prev_dist + 1)
                else
                    curr % dist = prev_dist + 1
                end if

            case ('(')

                call add_to_queue(Qitem(x=curr%x,y=curr%y),starts_q,starts_last)

            case (')')

                call pop_from_queue(starts_first,starts_last)

            case ('|')

                node = starts_q(starts_last-1)
                curr => map(node%x,node%y)

            case default

        end select

    end do

    !-- Recalculate grid minima/maxima to shrink output to fit
    GRID_RECALCULATION: block
        integer :: x_min_new = +HUGE(i)
        integer :: y_min_new = +HUGE(i)
        integer :: x_max_new = -HUGE(i)
        integer :: y_max_new = -HUGE(i)
        do y = y_dim_min, y_dim_max
            do x = x_dim_min, x_dim_max
                if (map(x,y) % type /= Unknown) then
                    if (x > x_max_new) x_max_new = x
                    if (y > y_max_new) y_max_new = y
                    if (x < x_min_new) x_min_new = x
                    if (y < y_min_new) y_min_new = y
                end if
            end do
        end do
        x_dim_min = x_min_new - 1
        y_dim_min = y_min_new - 1
        x_dim_max = x_max_new + 1
        y_dim_max = y_max_new + 1 
    end block GRID_RECALCULATION

    !-- Fill in remaining walls
    do y = y_dim_min, y_dim_max
        do x = x_dim_min, x_dim_max
            if (map(x,y) % type == Unknown) map(x,y) % type = Wall
        end do
    end do

    write (syslog%unit,'(a)') ' Map: '
    call write_state(syslog%unit)

    !-- Calculate part 1
    part1 = maxval(map(:,:) % dist) !

    !-- Calculate part 2
    where (map(:,:) % dist >= 1000)
        dist_filter = 1
    elsewhere
        dist_filter = 0
    end where
    part2 = sum(dist_filter)

    !-- Answers
    write (syslog%unit,'(a,i0)') 'Part 1: ', part1 ! 3721
    write (          *,'(a,i0)') 'Part 1: ', part1
    write (syslog%unit,'(a,i0)') 'Part 2: ', part2 ! 8613
    write (          *,'(a,i0)') 'Part 2: ', part2

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program