program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use scandata_mod
    use map_mod
    implicit none

    !-- Counters
    integer :: i
    integer :: x, y

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Handles return value of recursive function. Necessary because apparently
    ! a recursive subroutine can't call a recursive function? Needed to call
    ! recursive function from another recursive function or it would not compile
    logical :: flows

    !-- Puzzle values
    integer :: y_dim_min_save = 0 ! need to cut off answer before y=0
    integer :: part1 = 0
    integer :: part2 = 0

    !-- Bug fix variables
    logical :: has_left_wall = .false.
    logical :: has_right_wall = .false.

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(scans(num_lines))

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    do i = 1, num_lines
        
        ! Read
        read (input_unit,'(a)') line

        ! Create ScanData object
        scans(i) = ScanData(line)

    end do
    close (input_unit)

    !-- Write diagnostics
    if (.false.) then
        write (syslog%unit,'(a)') "    #     X     Y   Xmin  Xmax  Ymin  Ymax  Type  String"
        do i = 1, num_lines
            write (syslog%unit,'(8i6,a)')         &
                i, scans(i) % x, scans(i) % y,    &
                scans(i) % xmin, scans(i) % xmax, &
                scans(i) % ymin, scans(i) % ymax, &
                scans(i) % linetype, scans(i) % s
        end do
    end if

    !-- Start timer
    call syslog % start_timer

    !-- Identify map extents
    do i = 1, num_lines
        if      (scans(i) % linetype == HorizontalLine) then
            if (scans(i) % y    < y_dim_min) y_dim_min = scans(i) % y
            if (scans(i) % y    > y_dim_max) y_dim_min = scans(i) % y
            if (scans(i) % xmin < x_dim_min) x_dim_min = scans(i) % xmin
            if (scans(i) % xmax > x_dim_max) x_dim_max = scans(i) % xmax
        
        else if (scans(i) % linetype == VerticalLine) then
            if (scans(i) % x    < x_dim_min) x_dim_min = scans(i) % x
            if (scans(i) % x    > x_dim_max) x_dim_max = scans(i) % x
            if (scans(i) % ymin < y_dim_min) y_dim_min = scans(i) % ymin
            if (scans(i) % ymax > y_dim_max) y_dim_max = scans(i) % ymax
        else
            call syslog%log(__FILE__,'ERROR: Failed setting up grid for scan '//string(i))
            stop
        end if
    end do
    write (syslog%unit,'(4(a,i0),a)') &
        'Grid size: (',x_dim_min,'-',x_dim_max,',',y_dim_min,'-',y_dim_max,')'
    y_dim_min_save = y_dim_min ! save for answer
    x_dim_min = x_dim_min - 2 ! pad with 2 instead of 1 to ensure the edge case is handled
    x_dim_max = x_dim_max + 2
    y_dim_min = 0
    write (syslog%unit,'(4(a,i0),a)') &
        'Expanding grid to size: (',x_dim_min,'-',x_dim_max,',',y_dim_min,'-',y_dim_max,')'

    !-- Allocate map
    allocate(map(x_dim_min:x_dim_max,y_dim_min:y_dim_max))

    !-- Initialize map
    map(XSOURCE,YSOURCE) % map_type = MapSquareTypeSource
    do i = 1, num_lines
        if (scans(i) % linetype == HorizontalLine) then
            do x = scans(i) % xmin, scans(i) % xmax
                map(x,scans(i) % y) % map_type = MapSquareTypeClay
            end do
        else if (scans(i) % linetype == VerticalLine) then
            do y = scans(i) % ymin, scans(i) % ymax
                map(scans(i) % x,y) % map_type = MapSquareTypeClay
            end do
        else
            call syslog%log(__FILE__,'ERROR: Failed configuring map during scan '//string(i))
            stop
        end if
    end do

    !-- Write initial state
    call write_state

    flows = fill_down(XSOURCE,YSOURCE)

    !-- Bug: recursive filling of the tanks causes the top level to be considered
    ! standing water even if it is not bounded on both sides. This loop scrolls
    ! up the map from the bottom, and checks each standing water cell for left and
    ! right walls. If either is not found, converts the water to flowing water.
    Y_BUGFIX: do y = y_dim_max, y_dim_min_save, -1
        X_BUGFIX: do x = x_dim_min, x_dim_max
            
            if (x == x_dim_max .or. x == x_dim_min .or. y == y_dim_max) cycle X_BUGFIX

            if (map(x,y) % map_type == MapSquareTypeStandingWater) then
                    has_left_wall = .false.
                    has_right_wall = .false.
                    
                    ! Check for left boundary
                    LEFT_BNDRY_CHK: do i = x, x_dim_min, -1
                        if (map(i,y) % map_type == MapSquareTypeSand) exit LEFT_BNDRY_CHK
                        if (map(i,y) % map_type == MapSquareTypeClay) then
                            has_left_wall = .true.
                            exit LEFT_BNDRY_CHK
                        end if
                    end do LEFT_BNDRY_CHK
                    
                    ! Check for right boundary
                    RIGHT_BNDRY_CHK: do i = x, x_dim_max, +1
                        if (map(i,y) % map_type == MapSquareTypeSand) exit RIGHT_BNDRY_CHK
                        if (map(i,y) % map_type == MapSquareTypeClay) then
                            has_right_wall = .true.
                            exit RIGHT_BNDRY_CHK
                        end if
                    end do RIGHT_BNDRY_CHK
                    
                    ! Replace standing water with flowing water
                    if (.not. has_left_wall .or. .not. has_right_wall) then
                        map(x,y) % map_type = MapSquareTypeFlowingWater
                    end if

            end if

        end do X_BUGFIX
    end do Y_BUGFIX

    call write_state

    !-- Calculate part 1 & part 2 answers
    Y_LP_P1: do y = y_dim_min_save, y_dim_max
        X_LP_P1: do x = x_dim_min, x_dim_max
            if (map(x,y) % map_type == MapSquareTypeFlowingWater) part1 = part1 + 1
            if (map(x,y) % map_type == MapSquareTypeStandingWater) then
                part1 = part1 + 1
                part2 = part2 + 1
            end if
        end do X_LP_P1
    end do Y_LP_P1

    !-- Write out answers
    write (syslog%unit,'(a,i0)') 'Part 1: ',part1 ! 44743
    write (          *,'(a,i0)') 'Part 1: ',part1
    write (syslog%unit,'(a,i0)') 'Part 2: ',part2 ! 34172
    write (          *,'(a,i0)') 'Part 2: ',part2

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

contains

    ! Fill downward recursively
    recursive logical function fill_down(x,y) result(fd)
        integer,intent(in) :: x
        integer,intent(in) :: y
        logical :: left
        logical :: right
        integer :: this_y

        if (y + 1 > y_dim_max) then
            fd = .true.
            return
        end if

       !call write_state

        ! Can fill down
        if (map(x,y+1) % map_type /= MapSquareTypeClay) then
            map(x,y+1) % map_type = MapSquareTypeFlowingWater
            fd = fill_down(x,y+1)
            fd = .true.
            return
        
        ! Can't fill down: fill left & right
        else
            left = .true.
            right = .true.
            this_y = y
            do while (left .and. right)
                map(x,this_y) % map_type = MapSquareTypeStandingWater
                left  = fill_left (x,this_y)
                right = fill_right(x,this_y)
                this_y = this_y - 1 ! back up 1 level to fill left and right again
            end do
        end if
    end function

    ! Fill leftward recursively
    recursive logical function fill_left(x,y) result(fl)
        integer,intent(in) :: x
        integer,intent(in) :: y
        logical :: void
        
        if (x-1 < x_dim_min .or. x+1 > x_dim_max .or. y > y_dim_max) then
            fl = .true.
            return
        end if

       !call write_state

        ! Done filling left, return to fill down
        if (map(x-1,y) % map_type == MapSquareTypeClay) then
            fl = .true.
            return
        else
            if (map(x,y+1) % map_type /= MapSquareTypeSand) then
                map(x-1,y) % map_type = MapSquareTypeStandingWater
                fl = fill_left(x-1,y)
            else
                if (map(x+1,y+1) % map_type == MapSquareTypeClay) then
                    map(x,y) % map_type = MapSquareTypeStandingWater
                    void = fill_down(x, y)
                    fl = .false.
                    return
                else
                    map(x,y) % map_type = MapSquareTypeSand
                    fl = .false.
                    return
                end if
            end if
        end if

    end function

    ! Fill rightward recursively
    recursive logical function fill_right(x,y) result(fr)
        integer,intent(in) :: x
        integer,intent(in) :: y
        logical :: void

        if (x-1 < x_dim_min .or. x+1 > x_dim_max .or. y > y_dim_max) then
            fr = .true.
            return
        end if

       !call write_state

        ! Done filling right, return to fill down
        if (map(x+1,y) % map_type == MapSquareTypeClay) then
            fr = .true.
            return
        else
            if (map(x,y+1) % map_type /= MapSquareTypeSand) then
                map(x+1,y) % map_type = MapSquareTypeStandingWater
                fr = fill_right(x+1,y)
            else
                if (map(x-1,y+1) % map_type == MapSquareTypeClay) then
                    map(x,y) % map_type = MapSquareTypeStandingWater
                    void = fill_down(x, y)
                    fr = .false.
                    return
                else
                    map(x,y) % map_type = MapSquareTypeSand
                    fr = .false.
                    return
                end if
            end if
        end if

    end function

end program