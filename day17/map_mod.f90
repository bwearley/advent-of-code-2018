module map_mod
    use syslog_mod
    use string_tools_mod
    use get_digit_of_number_mod
    implicit none

    !-- Integer Kind
    integer,private,parameter :: ik = 0

    !-- Overall map dimensions
    integer :: x_dim_min = +HUGE(ik)
    integer :: x_dim_max = -HUGE(ik)
    integer :: y_dim_min = +HUGE(ik)
    integer :: y_dim_max = -HUGE(ik)

    !-- Coordinates of water source
    integer,parameter :: XSOURCE = 500
    integer,parameter :: YSOURCE =   0

    !-- Map Square Type Definitions
    enum,bind(c)
        enumerator :: MapSquareTypeSource
        enumerator :: MapSquareTypeFlowingWater
        enumerator :: MapSquareTypeStandingWater
        enumerator :: MapSquareTypeClay
        enumerator :: MapSquareTypeSand
    end enum

    !-- MapSquare struct
    type :: MapSquare
        integer :: x = -1
        integer :: y = -1
       !character(len=1) :: a
        integer :: map_type = MapSquareTypeSand
    contains
        procedure :: a
    end type
    type(MapSquare), allocatable :: map(:,:)

contains

    !> Character value of a coordinate
    function a(self)
        class(MapSquare) :: self
        character(len=1) :: a
        select case (self % map_type)
        case (MapSquareTypeClay)
            a = '#'
        case (MapSquareTypeFlowingWater)
            a = '|'
        case (MapSquareTypeSand)
            a = '.'
        case (MapSquareTypeStandingWater)
            a = '~'
        case (MapSquareTypeSource)
            a = '+'
        case default
        end select
    end function

    !> Write out the current state of map
    subroutine write_state
        implicit none
        integer :: x, y

        !-- Write header
        ! Hundreds Place
        write (syslog%unit,'(a)', advance='no') '    '
        do x = x_dim_min, x_dim_max
            write (syslog%unit,'(i1)',advance='no') get_digit_of_number(3,x)
        end do
        write (syslog%unit,*) ! advance
        ! Tens Place
        write (syslog%unit,'(a)', advance='no') '    '
        do x = x_dim_min, x_dim_max
            write (syslog%unit,'(i1)',advance='no') get_digit_of_number(2,x)
        end do
        write (syslog%unit,*) ! advance
        ! Ones Place
        write (syslog%unit,'(a)', advance='no') '    '
        do x = x_dim_min, x_dim_max
            write (syslog%unit,'(i1)',advance='no') get_digit_of_number(1,x)
        end do
        write (syslog%unit,*) ! advance

        !-- Grid
        do y = y_dim_min, y_dim_max !min(y_dim_max,100)
            write (syslog%unit,'(i0.4)',advance='no') y
            do x = x_dim_min, x_dim_max
                write (syslog%unit,'(a)',advance='no') map(x,y) % a()
            end do
            write (syslog%unit,*) ! advance
        end do
    end subroutine

end module
