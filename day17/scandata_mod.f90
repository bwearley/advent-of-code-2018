module scandata_mod
    use syslog_mod
    use string_tools_mod
    implicit none

    enum,bind(C)
        enumerator :: LineTypeNone
        enumerator :: VerticalLine
        enumerator :: HorizontalLine
    end enum

    type :: ScanData
        character(len=25) :: s
        integer :: x    = 0
        integer :: y    = 0
        integer :: xmin = 0
        integer :: xmax = 0
        integer :: ymin = 0
        integer :: ymax = 0
        integer :: linetype = LineTypeNone
    end type
    interface ScanData
        module procedure init_scandata_from_string
    end interface

    type(ScanData), allocatable :: scans(:)

contains

    type(ScanData) function init_scandata_from_string(str) result(sc)
        character(len=*) :: str
        integer :: pos

        if (str(1:1) == 'x') then

            sc % linetype = VerticalLine
            
            ! Strip x=
            pos = index(str,'x')
            str(pos:pos+1) = ' '

            ! Strip y=
            pos = index(str,'y')
            str(pos:pos+1) = ' '

            ! Strip ..
            pos = index(str,'..')
            str(pos:pos+1) = ','

            ! Read
            read (str,*) sc % x, sc % ymin, sc % ymax

            sc % s = str

        else if (str(1:1) == 'y') then

            sc % linetype = HorizontalLine

            ! Strip y=
            pos = index(str,'y')
            str(pos:pos+1) = ' '

            ! Strip x=
            pos = index(str,'x')
            str(pos:pos+1) = ' '

            ! Strip ..
            pos = index(str,'..')
            str(pos:pos+1) = ','

            ! Read
            read (str,*) sc % y, sc % xmin, sc % xmax

            sc % s = str

        else
            call syslog%log(__FILE__,'ERROR: Failed to read line: '//str)
            stop
        end if

    end function

end module
