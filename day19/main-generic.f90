program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use elfcode_mod
    implicit none

    !-- Counters
    integer :: i

    !-- Input file unit
    integer :: input_unit

    !-- Position indicator in character line
    integer :: pos

    !-- Number of lines in input file
    integer :: num_lines

    ! Number of instructions in input file
    integer :: num_instr

    integer :: reg_id = -1

    logical :: debug_printouts = .false.

    !-- Input file reading properties
    integer,parameter            :: max_line_len = 50
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    character(len=3) :: reg0value_str
    integer :: reg0value_int
    type(Argument) :: reg0arg

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)
    num_instr = num_lines - 1

    !-- Override initial 0 value
    reg0arg = get_argument_named('reg0value')
    if (reg0arg % is_configured()) then
        reg0value_str = get_value_for_arg('reg0value')
        read (reg0value_str,*) reg0value_int
        reg(0) = reg0value_int
    end if

    !-- Allocate accordingly
    allocate(instructions(0:num_instr-1))

    !-- Start timer
    call syslog % start_timer

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    
    !-- Read instruction pointer, e.g., #ip 0
    read (input_unit,'(a)') line
    if (line(1:1) == '#') then

        ! Strip #ip
        line(1:3) = ' '

        ! Get register #
        read (line,*) reg_id
        
        ! Pointer assignment
        ip % reg_ptr => reg(reg_id)

        write (syslog%unit,*) 'IP => ', reg_id
    else
        write (syslog%unit,*) 'Failed reading instruction pointer'
        stop
    end if

    !-- Read instructions, e.g., seti 5 0 1
    READ_INSTRUCTIONS: do i = 0, num_instr-1

        ! Read line
        read (input_unit,'(a)') line

        instructions(i) % opc = line(1:4)

        line(1:4) = ' '

        select case (instructions(i) % opc)
            case ('banr'); instructions(i) % op = 00 
            case ('muli'); instructions(i) % op = 01 
            case ('bori'); instructions(i) % op = 02 
            case ('setr'); instructions(i) % op = 03 
            case ('addi'); instructions(i) % op = 04 
            case ('eqrr'); instructions(i) % op = 05 
            case ('gtri'); instructions(i) % op = 06 
            case ('gtir'); instructions(i) % op = 07 
            case ('borr'); instructions(i) % op = 08 
            case ('eqri'); instructions(i) % op = 09 
            case ('bani'); instructions(i) % op = 10
            case ('addr'); instructions(i) % op = 11
            case ('eqir'); instructions(i) % op = 12
            case ('mulr'); instructions(i) % op = 13
            case ('seti'); instructions(i) % op = 14
            case ('gtrr'); instructions(i) % op = 15
        end select

        line(1:4) = ' '

        read(line,*) instructions(i) % a, instructions(i) % b, instructions(i) % c

    end do READ_INSTRUCTIONS
    close (input_unit)

    do i = 0, num_instr-1
        write (syslog%unit,*)                                       &
        i, instructions(i) % opc, instructions(i) % op,             &
        instructions(i) % a, instructions(i) % b, instructions(i) % c
    end do

    EXE: do

        if (debug_printouts) then
            write (syslog%unit,'(a,i2,a,6i8,a,a,3i3)',advance='no') &
                'ip=', ip % value, '[',         &
                reg(0),                 &
                reg(1),                 &
                reg(2),                 &
                reg(3),                 &
                reg(4),                 &
                reg(5), ']',            &
                instructions(ip % value) % opc, &
                instructions(ip % value) % a,   &
                instructions(ip % value) % b,   &
                instructions(ip % value) % c
            flush (syslog%unit)
        end if

        call instructions(ip % value) % exec

        if (debug_printouts) then
            write (syslog%unit,'(a,6i8,a)') &
                '[',        &
                reg(0),     &
                reg(1),     &
                reg(2),     &
                reg(3),     &
                reg(4),     &
                reg(5), ']'
            flush (syslog%unit)
        end if
 
        if (ip % value > num_instr-1) exit EXE

    end do EXE

    !-- Part 1: Identify register 0 final value
    ! Part 1: 1922
    write (syslog%unit,'(a,i0)') 'Answer: ', reg(0)
    write (          *,'(a,i0)') 'Answer: ', reg(0)


    ! !-- Process instructions
    ! reg(:) = 0
    ! do i = 1, num_instr
    !     call do_instr(instructions(i)%op,instructions(i)%a,instructions(i)%b,instructions(i)%c)
    ! end do


    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program