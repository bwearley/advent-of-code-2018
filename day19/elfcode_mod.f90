module elfcode_mod
    use syslog_mod

    !-- Registers
    integer,parameter :: NUM_REGISTERS = 6
    integer,target :: reg(0:NUM_REGISTERS-1) = 0

    !-- Instructions
    type :: Instruction_Pointer
        integer :: value = 0
        integer, pointer :: reg_ptr => null()
    contains
        procedure :: updt_0
        procedure :: updt_1
    end type
    type(Instruction_Pointer) :: ip

    !-- Instructions
    type :: Instruction
        integer :: op
        integer :: a
        integer :: b
        integer :: c
        character(len=4) :: opc
    contains
        procedure :: exec
    end type
    type(Instruction), allocatable :: instructions(:)

contains

    subroutine exec(self)
        class(Instruction) :: self
        call ip % updt_0
        call do_instr(self % op, self % a, self % b, self % c)
        call ip % updt_1
    end subroutine

    subroutine updt_0(self)
        class(Instruction_Pointer) :: self
        self % reg_ptr = self % value
    end subroutine

    subroutine updt_1(self)
        class(Instruction_Pointer) :: self
        self % value = self % reg_ptr
        self % value = self % value + 1
    end subroutine

    subroutine do_instr(op, a, b, c)
        integer,intent(in) :: op
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c

        select case (op)
            case(0);  call banr(a, b, c);
            case(1);  call muli(a, b, c);
            case(2);  call bori(a, b, c);
            case(3);  call setr(a, b, c);
            case(4);  call addi(a, b, c);
            case(5);  call eqrr(a, b, c);
            case(6);  call gtri(a, b, c);
            case(7);  call gtir(a, b, c);
            case(8);  call borr(a, b, c);
            case(9);  call eqri(a, b, c);
            case(10); call bani(a, b, c);
            case(11); call addr(a, b, c);
            case(12); call eqir(a, b, c);
            case(13); call mulr(a, b, c);
            case(14); call seti(a, b, c);
            case(15); call gtrr(a, b, c);
        case default
        end select
    end subroutine

    !> addr (add register) stores into register C the
    ! result of adding register A and register B
    subroutine addr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a) + reg(b)
    end subroutine

    !> addi (add immediate) stores into register C the
    ! result of adding register A and value B
    subroutine addi(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a) + b
    end subroutine

    !>mulr (multiply register) stores into register C the
    ! result of multiplying register A and register B
    subroutine mulr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a) * reg(b)
    end subroutine

    !> muli (multiply immediate) stores into register C the
    ! result of multiplying register A and value B
    subroutine muli(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a) * b
    end subroutine

    !> banr (bitwise AND register) stores into register C the
    ! result of the bitwise AND of register A and register B
    subroutine banr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
       reg(c) = iand(reg(a), reg(b))
    end subroutine

    !> bani (bitwise AND immediate) stores into register C the
    ! result of the bitwise AND of register A and value B
    subroutine bani(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = iand(reg(a), b)
    end subroutine

    !> borr (bitwise OR register) stores into register C the
    ! result of the bitwise OR of register A and register B
    subroutine borr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = ior(reg(a), reg(b))
    end subroutine

    !> bori (bitwise OR immediate) stores into register C the
    ! result of the bitwise OR of register A and value B
    subroutine bori(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = ior(reg(a), b)
    end subroutine

    !> setr (set register) copies the contents of register A
    ! into register C. (Input B is ignored.)
    subroutine setr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = reg(a)
    end subroutine

    !> seti (set immediate) stores value A into register C.
    ! (Input B is ignored.)
    subroutine seti(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        reg(c) = a
    end subroutine

    !> gtir (greater-than immediate/register) sets register C
    ! to 1 if value A is greater than register B. Otherwise,
    ! register C is set to 0
    subroutine gtir(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (a > reg(b)) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> gtri (greater-than register/immediate) sets register C
    ! to 1 if register A is greater than value B. Otherwise,
    ! register C is set to 0
    subroutine gtri(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (reg(a) > b) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> gtrr (greater-than register/register) sets register C
    ! to 1 if register A is greater than register B. Otherwise,
    ! register C is set to 0
    subroutine gtrr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (reg(a) > reg(b)) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> eqir (equal immediate/register) sets register C to 1 if
    ! value A is equal to register B. Otherwise, register C is
    ! set to 0
    subroutine eqir(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (a == reg(b)) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> eqri (equal register/immediate) sets register C to 1 if
    ! register A is equal to value B. Otherwise, register C is
    ! set to 0
    subroutine eqri(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (reg(a) == b) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

    !> eqrr (equal register/register) sets register C to 1 if
    ! register A is equal to register B. Otherwise, register C
    ! is set to 0
    subroutine eqrr(a, b, c)
        integer,intent(in) :: a
        integer,intent(in) :: b
        integer,intent(in) :: c
        if (reg(a) == reg(b)) then
            reg(c) = 1
        else
            reg(c) = 0
        end if
    end subroutine

end module