program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use node_mod
    use parse_mod
    implicit none

    !-- Counters
    integer :: i

    integer :: pos

    !-- Input file unit
    integer :: input_unit

    !-- Input file reading properties
    integer,parameter            :: max_line_len = 50000
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: line_copy
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    read (input_unit,'(a)') line
    close (input_unit)
    line_copy = line

    !-- Count number of values
    num_values = 1
    COUNT_VALUES: do
        pos = index(trim(line), ' ')
        if (pos == 0) exit COUNT_VALUES
        num_values = num_values + 1
        line = line(pos+1:)
    end do COUNT_VALUES
    write(syslog%unit,'(a,i0,a)') 'Found ',num_values,' values.'

    !-- Allocate data appropriately
    allocate(values(num_values))

    !-- Maximum number of nodes would be half the total number of values
    allocate(nodes(num_values/2))

    !-- Load line into values
    read (line_copy,*) (values(i), i = 1, num_values)

    !-- Parse data into node tree
    write (syslog%unit,'(a)') 'NodeID #Chld #Meta Value  Metadata'
    call parse_data

    !-- Calculate answers
    sum_metadata = 0
    i = 1
    do
        if (nodes(i) % nodeid == 0) exit

        write (syslog%unit,'(4i6,100i5)') i,     &
            nodes(i) % num_children, &
            nodes(i) % num_metadata, &
            nodes(i) % value,        & 
            nodes(i) % metadata

        sum_metadata = sum_metadata + sum(nodes(i) % metadata)
        i = i + 1
    end do

    !-- Log answer
    write (syslog%unit,*) 'Part 1: ',sum_metadata ! 41926
    write (          *,*) 'Part 1: ',sum_metadata,' (sample: 138, ans: 41926)' ! 41926

    !-- Log answer
    write (syslog%unit,*) 'Part 2: ',nodes(1) % value ! 24262
    write (          *,*) 'Part 2: ',nodes(1) % value,' (sample: 66, ans: 24262)' ! 24262

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program