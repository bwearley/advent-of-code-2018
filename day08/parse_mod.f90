module parse_mod
USE string_tools_mod
implicit none
contains

    recursive subroutine parse_data
        use node_mod
        implicit none
        integer :: num_children
        integer :: num_metadata
        integer :: j, k
        integer :: last_value = 0
        integer :: vix_save, nid_save
        integer :: childrenvals(values(vix))

        ! Increment node ID
        new_node_id = new_node_id + 1

        ! Grab number of children, number of metadata (first 2 entries)
        num_children = values(vix)
        num_metadata = values(vix+1)

        ! Save this node ID
        nid_save = new_node_id

        ! Populate new node with data
        nodes(nid_save) = Node(        &
            id=nid_save,               &  
            vix=vix,                   &
            num_children=num_children, &
            num_metadata=num_metadata)

        ! Move index to next 2
        vix = vix + 2
        vix_save = vix

        ! Parse child nodes
        if (num_children > 0) then
            do j = 1, num_children
                call parse_data
                childrenvals(j) = last_value
            end do
        end if

        ! Save metadata
        nodes(nid_save) % metadata = values(vix:vix-1+num_metadata)
        
        ! Save location of metadata (if applicable)
        if (num_metadata > 0) then
            nodes(nid_save) % vix_meta_start = vix
            nodes(nid_save) % vix_meta_end   = vix-1+num_metadata
        end if

        ! Save value of current node
        if (num_children == 0) then
            nodes(nid_save) % value = sum(nodes(nid_save) % metadata)
        else
            do j = 1, num_metadata
                k = nodes(nid_save) % metadata(j)
                if (k <= num_children .and. k /= 0) then
                    nodes(nid_save) % value = &
                          nodes(nid_save) % value + childrenvals(k)
                end if
            end do
        end if

        ! Save last recorded value to pass back up
        last_value = nodes(nid_save) % value

        vix = vix + num_metadata
    end subroutine
end module