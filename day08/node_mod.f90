module node_mod
    use syslog_mod
    implicit none

    !-- Number of values in input file
    integer :: num_values

    !-- Sum of metadata (part 1)
    integer :: sum_metadata = 0

    !-- Value index (index of current position in values(:) array)
    integer :: vix = 1

    !-- Index to insert next node
    integer :: new_node_id = 0

    !-- Raw integer values read
    integer,allocatable :: values(:)

    !-- Node struct
    type :: node
        integer :: nodeid         = 0      ! index of this node in nodes(:)
        integer :: vix_start      = 0      ! vix for start of node data in values(:)
        integer :: vix_meta_start = 0      ! vix for start of metadata in values(:)
        integer :: vix_meta_end   = 0      ! vix for end of metadata in values(:)
        integer :: value          = 0      ! calculated value for this node
        integer :: num_children   = 0      ! number of child nodes
        integer :: num_metadata   = 0      ! number of meta data
       !integer,allocatable :: children(:) ! index to children in nodes (doesn't work)
        integer,allocatable :: metadata(:) ! holds all metadata
    end type
    interface node
        module procedure init_node
    end interface

    !-- Array of nodes
    type(node), allocatable :: nodes(:)

contains

    type(node) function init_node(id,vix,num_children,num_metadata) result(nd)
        implicit none
        integer,intent(in) :: id
        integer,intent(in) :: vix
        integer,intent(in) :: num_children
        integer,intent(in) :: num_metadata

        nd % vix_start = vix
        nd % nodeid = id
        nd % num_children = num_children
        nd % num_metadata = num_metadata

       !allocate(nd % children(num_children))
        allocate(nd % metadata(num_metadata))
        
    end function
end module