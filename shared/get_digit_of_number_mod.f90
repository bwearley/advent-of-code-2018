module get_digit_of_number_mod
contains
  pure integer function get_digit_of_number(digit_place,num) result(digit)
       integer,intent(in) :: digit_place
       integer,intent(in) :: num
       digit = mod(int(num / 10**(digit_place-1)),10)
   end function
end module