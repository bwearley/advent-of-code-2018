module floating_point
    use, intrinsic :: iso_fortran_env, only: REAL32, REAL64, REAL128
    implicit none
    private

    !-- Floating Point Definitions
    integer,parameter :: sp = REAL32
    integer,parameter :: dp = REAL64
    integer,parameter :: qp = REAL128

    !-- Select Floating Point
    integer,parameter,public :: fp = dp
end module
