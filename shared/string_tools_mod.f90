module string_tools_mod
    use floating_point
    implicit none
    private
    public :: string, str2int, letter_index, uppercase_letter_at_index, lowercase_letter_at_index
    
    !-- Private Parameters
    integer,parameter :: tmp_str_len = 32

    !-- ASCII Parameters
    integer,parameter :: ASCII_CHAR_SHIFT_UPPERCASE = 64
    integer,parameter :: ASCII_CHAR_SHIFT_LOWERCASE = 96
    ! A = 65, Z = 90, a = 97, z = 122
    ! a = 97 -> i = 1
    ! A = 65 -> i = 1

    interface string
        module procedure string_int_4
        module procedure string_int_8
        module procedure string_real
    end interface

contains

    !> Converts an integer into a string of fmt I0 (minimum length integer)
    pure function string_int_4(var_int) result(str)
        character(len=tmp_str_len) :: tmp
        character(len=:),allocatable :: str
        integer(4),intent(in) :: var_int
        write (tmp,fmt='(i0)') var_int
        str = trim(tmp)
    end function

    !> Converts an integer into a string of fmt I0 (minimum length integer)
    pure function string_int_8(var_int) result(str)
        character(len=tmp_str_len) :: tmp
        character(len=:),allocatable :: str
        integer(8),intent(in) :: var_int
        write (tmp,fmt='(i0)') var_int
        str = trim(tmp)
    end function

    !> Converts a real variable into a string of format fmt
    pure function string_real(var_real,fmt) result(str)
        character(len=tmp_str_len)   :: tmp
        character(len=:),allocatable :: str
        real(fp),intent(in)          :: var_real
        character(len=*),intent(in)  :: fmt
        write (tmp,fmt=fmt) var_real
        str = trim(tmp)
    end function

    !> Converts an input string to an integer
    integer pure function str2int(str)
        character(len=*),intent(in) :: str
        read (str,*) str2int
    end function

    !> Returns the index of alphabet letters
    integer pure elemental function letter_index(a) result(ix)
        character(len=1),intent(in) :: a
        if (iachar(a) > ASCII_CHAR_SHIFT_LOWERCASE) then
            ix = iachar(a) - ASCII_CHAR_SHIFT_LOWERCASE
        else
            ix = iachar(a) - ASCII_CHAR_SHIFT_UPPERCASE
        end if
    end function

    !> Returns the uppercase letter at alphabetical index
    character(len=1) pure elemental function uppercase_letter_at_index(ix) result(a)
        integer,intent(in) :: ix
        a = achar(ix + ASCII_CHAR_SHIFT_UPPERCASE)
    end function

    !> Returns the lowercase letter at alphabetical index
    character(len=1) pure elemental function lowercase_letter_at_index(ix) result(a)
        integer,intent(in) :: ix
        a = achar(ix + ASCII_CHAR_SHIFT_LOWERCASE)
    end function

end module