module file_tools_mod
use syslog_mod

contains

    integer function lines_in_file(filename) result(lines)
        implicit none
        character(len=*),intent(in) :: filename
        integer :: iostat
        integer :: unit
        
        !-- Initialize
        lines = 0
        call syslog % log(__FILE__,'Begin counting lines in file '//filename)
        
        !-- Open file
        open(              &
            newunit=unit,  &
            file=filename, &
            action='read', &
            status='old',  &
            iostat=iostat  &
        )

        !-- Count lines
        do
            read(unit,*,iostat=iostat)
            if (iostat /= 0) exit
            lines = lines + 1
        end do
        close(unit)

        write(syslog % unit,'(a,i0,a)') '<'//__FILE__//'> Found ',lines,' lines in file '//filename
    end function
end module
