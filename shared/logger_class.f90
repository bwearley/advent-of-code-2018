module logger_class
    implicit none
    private
    public :: logger
    integer,parameter :: max_filename_len = 30
    
    type logger
        character(len=max_filename_len) :: filename
        integer :: unit
        real :: timer_start, timer_end
    contains
        procedure :: log
        procedure :: write_line
        procedure :: start_timer
        procedure :: end_timer
    end type

    interface logger
        module procedure init_logger
    end interface

contains

    function init_logger(filename) result(new_logger)
        implicit none
        character(len=*) :: filename
        type(logger) :: new_logger

        new_logger%filename = filename

        open(newunit=new_logger%unit,file=filename)

    end function
    
    subroutine write_line(self,line)
        implicit none
        class(logger) :: self
        character(len=*) :: line

        write(self%unit,*) line
    end subroutine

    subroutine log(self,source,message)
        implicit none
        class(logger) :: self
        character(len=*) :: source
        character(len=*) :: message

        write(self%unit,'(A)') '<'//source//'> '//message
    end subroutine

    !
    ! Timer Subroutines
    !
    subroutine start_timer(self,source)
        implicit none
        class(logger) :: self
        character(len=*),optional :: source
 
        call cpu_time(self%timer_start)

        if (present(source)) then
            call self%log(source,'Timer started.')
        else
            call self%write_line('Timer started.')
        end if
    end subroutine

    subroutine end_timer(self,source)
        implicit none
        class(logger) :: self
        character(len=*),optional :: source
        character(len=10) :: time_duration

        call cpu_time(self%timer_end)

        write(time_duration,'(F10.6)') self%timer_end - self%timer_start

        if (present(source)) then
            call self%log(source,'Timer ended.')
            call self%log(source,'Total time: '//time_duration)
        else
            call self%write_line('Timer ended.')
            call self%write_line('Total time: '//time_duration)
        end if

    end subroutine

end module
