module syslog_mod
    use logger_class
    implicit none
    private
    public :: syslog, init_syslog
    character(len=*),parameter :: syslog_filename = 'output.out'
    type(logger) :: syslog

contains

    subroutine init_syslog
        implicit none
        integer :: time(8)
        character(len=100) :: date_string

        !-- Initialize & Get Date
        syslog = logger(syslog_filename)
        call date_and_time(values=time)
        write(date_string,'(I0.4,A,5(I0.2,A),I0.3)') time(1),'/',time(2),'/',time(3),' ',time(5),':',time(6),':',time(7),':',time(8)

        !-- Header
        call syslog%write_line('/*')
        call syslog%write_line('/* SYSTEM LOG')
        call syslog%write_line('/*')
        call syslog%write_line('')

        !-- Write Date & Time
        call syslog%log('INIT_SYSLOG','Initialized at '//date_string)

        !-- Write Initial Statements
        call syslog%log('INIT_SYSLOG','Finished initializing logger.')

    end subroutine

end module
