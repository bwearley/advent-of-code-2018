program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use nanobot_mod
    implicit none

    !-- Counters
    integer :: i
    integer :: x, y, z
    integer :: a, b, c

    !-- Cube extents
    integer :: x_min = +HUGE(i)
    integer :: x_max = -HUGE(i)
    integer :: y_min = +HUGE(i)
    integer :: y_max = -HUGE(i)
    integer :: z_min = +HUGE(i)
    integer :: z_max = -HUGE(i)

    ! type :: Coord3d
    !     integer :: x = 0
    !     integer :: y = 0
    !     integer :: z = 0
    ! end type

    type :: SearchBox
        integer :: covered_bots = 0
        integer :: origin_dist = 0
        integer :: side_length = 0
        ! Coordinates of corner (where coordinates are "smallest")
        integer :: x = 0
        integer :: y = 0
        integer :: z = 0
    end type

    integer :: searchboxes_count = 0
    type(SearchBox),allocatable,target :: searchboxes(:)
    type(SearchBox),pointer :: box => null()

    !-- Heap
    integer :: heap_count = 0
    integer,allocatable :: heap(:)

    integer :: new_side = 0

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Part 1 values
    integer :: max_power = 0
    integer :: max_power_bot = 0

    integer :: length = 0

    !-- Puzzle values
    integer :: part1 = 0
    integer :: part2 = 0

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)
    write (syslog%unit,'(a,i0,a)') 'Found ',num_lines, ' nanobots'

    !-- Allocate data appropriately
    allocate(nanobots(num_lines))
    allocate(heap(num_lines*num_lines))
    allocate(searchboxes(num_lines*num_lines))

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    do i = 1, num_lines
        
        ! Read
        read (input_unit,'(a)') line

        ! Create Nanobot object
        nanobots(i) = Nanobot(line)

    end do
    close (input_unit)

    !-- Write diagnostics
    if (.false.) then
        write (syslog%unit,'(a)') "         #         X         Y         Z         R"
        do i = 1, num_lines
            write (syslog%unit,'(5i10)') i,        &
                nanobots(i) % x, &
                nanobots(i) % y, &
                nanobots(i) % z, &
                nanobots(i) % r
        end do
    end if

    !-- Start timer
    call syslog % start_timer

    !-- Identify maximum power bot
    do i = 1, num_lines
        if (nanobots(i) % r > max_power) then
            max_power_bot = i
            max_power = nanobots(i) % r
        end if
    end do
    
    write (syslog%unit,'(a,2i10)') 'Max power bot: ',max_power_bot, max_power

    !-- Identify bots in range
    do i = 1, num_lines

        associate (bot => nanobots(i))

            if (manhattan_distance(bot%x, bot%y, bot%z, &
                nanobots(max_power_bot)%x, nanobots(max_power_bot)%y, nanobots(max_power_bot)%z) &
                    <= nanobots(max_power_bot) % r) then

                part1 = part1 + 1

            end if

        end associate
    end do

    !-- Part 1 answer
    write (syslog%unit,'(a,i0)') 'Part 1: ',part1 ! 595
    write (          *,'(a,i0)') 'Part 1: ',part1

    !-- Get dimensions
    do i = 1, num_lines
        if (nanobots(i) % x - nanobots(i) % r < x_min) x_min = nanobots(i) % x - nanobots(i) % r
        if (nanobots(i) % x + nanobots(i) % r > x_max) x_max = nanobots(i) % x + nanobots(i) % r
        if (nanobots(i) % y - nanobots(i) % r < y_min) y_min = nanobots(i) % y - nanobots(i) % r
        if (nanobots(i) % y + nanobots(i) % r > y_max) y_max = nanobots(i) % y + nanobots(i) % r
        if (nanobots(i) % z - nanobots(i) % r < z_min) z_min = nanobots(i) % z - nanobots(i) % r
        if (nanobots(i) % z + nanobots(i) % r > z_max) z_max = nanobots(i) % z + nanobots(i) % r
    end do

    write (syslog%unit,'(a,i0)') 'x_min: ',x_min
    write (syslog%unit,'(a,i0)') 'x_max: ',x_max
    write (syslog%unit,'(a,i0)') 'y_min: ',y_min
    write (syslog%unit,'(a,i0)') 'y_max: ',y_max
    write (syslog%unit,'(a,i0)') 'z_min: ',z_min
    write (syslog%unit,'(a,i0)') 'z_max: ',z_max

    !-- Configure inital searchbox
    box => Searchboxes(1)
    searchboxes_count = searchboxes_count + 1
    box = SearchBox(x=x_min,y=y_min,z=z_min)
    box % origin_dist = manhattan_distance(box%x,box%y,box%z,x1=0,y1=0,z1=0)
    length = 1
    do while (box % x + length < x_max .or. &
              box % y + length < y_max .or. &
              box % z + length < z_max)
        length = length * 2
    end do
    box % side_length = length
    call count_boxes(box)

    write (syslog%unit,'(a,i0)') 'Bots in initial box: ',box % covered_bots

    heap(1) = 1
    heap_count = 1
    do while (heap_count > 0)

        associate (qbox => searchboxes(heap(1)))

            heap(1) = heap(heap_count)
            heap_count = heap_count - 1
            call bubble_down
            
            !-- Part 2 solution
            if (qbox % side_length == 1) then
                part2 = qbox % origin_dist
                
                write (syslog%unit,'(a,i0)') 'Covered nanobots in answer: ',qbox % covered_bots
                write (syslog%unit,'(a,i0)') 'Part 2: ',part2 ! 88122632 (covered bots: 982)
                write (          *,'(a,i0)') 'Part 2: ',part2
                exit
            end if

            new_side = qbox % side_length / 2
            x = qbox % x
            do a = 0, 1
                x = x + new_side * a
                y = qbox % y
                do b = 0, 1
                    y = y + new_side * b
                    z = qbox % z
                    do c = 0, 1
                        z = z + new_side * c
                        heap_count = heap_count + 1
                        heap(heap_count) = searchboxes_count
                        box => searchboxes(searchboxes_count)
                        searchboxes_count = searchboxes_count + 1
                        box % x = x
                        box % y = y
                        box % z = z
                        box % side_length = new_side
                        box % origin_dist = manhattan_distance(box%x,box%y,box%z,x1=0,y1=0,z1=0)
                        call count_boxes(box)
                        call bubble_up
                    end do
                end do
            end do

        end associate

    end do
    
    !-- End timer
    call syslog % end_timer
    call syslog%log(__FILE__,'Done.')

contains

    subroutine swap(a, b)
        integer, intent(inout) :: a
        integer, intent(inout) :: b
        integer :: tmp
        tmp = a
        a = b
        b = tmp
    end subroutine swap
 
    logical function compare_heap_entry(pa, pb) result(cmp)
        integer,intent(in) :: pa
        integer,intent(in) :: pb
        type(Searchbox),pointer :: a
        type(Searchbox),pointer :: b

        a => searchboxes(heap(pa))
        b => searchboxes(heap(pb))

        if (a % covered_bots /= b % covered_bots) then
            cmp = a % covered_bots > b % covered_bots
            return
        end if
        if (a % origin_dist /= b % origin_dist) then
            cmp = a % origin_dist < b % origin_dist
            return
        end if
        cmp = a % side_length < b % side_length
    end function compare_heap_entry

    subroutine bubble_up
        integer :: pos
        pos = heap_count

        do while (pos > 1 .and. compare_heap_entry(pos, pos/2))
            call swap( heap(pos/2), heap(pos) )
            pos = pos / 2
        end do
    end subroutine bubble_up

    subroutine bubble_down
        integer :: pos = 1
        integer :: swap_pos = 0

        pos = 1
        do while (pos * 2 <= heap_count)
            swap_pos = pos * 2
            if (swap_pos + 1 <= heap_count .and. compare_heap_entry(swap_pos+1, swap_pos)) then
                swap_pos = swap_pos + 1
            end if
            if (compare_heap_entry(swap_pos, pos)) then
                call swap( heap(swap_pos),heap(pos) )
                pos = swap_pos
            else
                exit
            end if
        end do
    end subroutine bubble_down

    integer function range_dist(x,lo,hi)
        integer,intent(in) :: x
        integer,intent(in) :: lo
        integer,intent(in) :: hi

        if (x < lo) then
            range_dist = lo - x
            return
        end if
        if (x > hi) then
            range_dist = x - hi
            return
        end if
        range_dist = 0
    end function range_dist

    subroutine count_boxes(box)
        type(SearchBox),intent(inout) :: box
        integer :: x1
        integer :: y1
        integer :: z1
        integer :: x2
        integer :: y2
        integer :: z2
        integer :: count
        integer :: d

        x1 = box % x
        y1 = box % y
        z1 = box % z
        x2 = box % x + box % side_length - 1
        y2 = box % y + box % side_length - 1
        z2 = box % z + box % side_length - 1

        count = 0
        do i = 1, num_lines
            d = 0
            d = d + range_dist(nanobots(i)%x, x1, x2)
            d = d + range_dist(nanobots(i)%y, y1, y2)
            d = d + range_dist(nanobots(i)%z, z1, z2)
            if (d <= nanobots(i) % r) count = count + 1
        end do
        box % covered_bots = count
    end subroutine count_boxes

    integer function manhattan_distance(x0,y0,z0,x1,y1,z1) result(d)
        integer,intent(in) :: x0,y0,z0
        integer,intent(in) :: x1,y1,z1

        d = abs(x1-x0) + abs(y1-y0) + abs(z1-z0)
        return
    end function manhattan_distance


end program