module nanobot_mod
    use syslog_mod
    use string_tools_mod
    implicit none

    type :: Nanobot
        integer :: x = 0
        integer :: y = 0
        integer :: z = 0
        integer :: r = 0
    end type
    interface Nanobot
        module procedure init_Nanobot_from_string
    end interface

    type(Nanobot), allocatable :: Nanobots(:)

contains

    type(Nanobot) function init_nanobot_from_string(str) result(nb)
        character(len=*) :: str
        integer :: pos

        ! Strip pos=<
        pos = index(str,'<')
        str(:pos) = ' '

        ! Strip >
        pos = index(str,'>')
        str(pos:pos+1) = ' '

        ! Strip r
        pos = index(str,'r')
        str(pos:pos) = ' '

        ! Strip =
        pos = index(str,'=')
        str(pos:pos) = ' '

        ! Read
        read (str,*) nb % x, nb % y, nb % z, nb % r

    end function

end module
