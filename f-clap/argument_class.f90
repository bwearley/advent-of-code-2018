!-----------------------------------------------------------------------
! Fortran Command Line Argument Parser (f-clap)
!-----------------------------------------------------------------------
!
! MODULE:
! argument_class
!
! AUTHOR:
! Brian Earley
!
! DESCRIPTION:
!> Defines the Argument type which encapsulates each type of argument
!! passed on the command line
!-----------------------------------------------------------------------

module argument_class
    use argument_group_class
    use fclap_settings_mod
    implicit none
    private

    ! Nargs Strings
    character(len=*),parameter,public :: NARGS_ONE_OPTIONAL_QMARK = '?'
    character(len=*),parameter,public :: NARGS_MULTIPLE_PLUS = '+'
    character(len=*),parameter,public :: NARGS_MULTIPLE_OPTIONAL_STAR = '*'

    ! Action_Type Strings
    character(len=*),parameter,public :: ACTION_TYPE_STORE_VALUE = 'store_value'
    character(len=*),parameter,public :: ACTION_TYPE_STORE_TRUE  = 'store_true'
    character(len=*),parameter,public :: ACTION_TYPE_STORE_FALSE = 'store_false'

    !-- Argument
    type,public :: Argument
        character(len=:),     allocatable :: name                 !< name of the argument (e.g., input_file, or the name of the variable this affects)
        character(len=:),     allocatable :: switch_short         !< short switch (e.g., -o, -v)
        character(len=:),     allocatable :: switch_long          !< long switch (e.g., --output, --version)
        character(len=:),     allocatable :: metavar              !< name to use in place of actual name in usage information
        character(len=:),     allocatable :: value                !< stored value for arguments with one value
        character(len=mxclen),allocatable :: values_list(:)       !< stores array of inputs for arguments with multiple values
        character(len=:),     allocatable :: nargs                !< stores number of arguments or +, *, ? as string
        character(len=:),     allocatable :: default_value        !< default value if nargs='?'
        character(len=:),     allocatable :: action_type          !< type of action for argument (store_true, store_false, store_value)
        character(len=:),     allocatable :: help                 !< help information
        type(Argument_Group),pointer      :: group => null()      !< pointer to an Argument_Group
        integer                           :: nargs_int = 0        !< stores the number of arguments as an integer
        logical                           :: important  = .false. !< flag indicating whether this argument should be printed in summary
        logical                           :: positional = .false. !< flag indicating if this is a positional argument
    contains
        procedure :: print_usage
        procedure :: print_help
        procedure :: is_configured
        final     :: destroy_argument
    end type

    interface argument
        module procedure init_argument
    end interface

contains

    !> Create and return an Argument
    function init_argument(                  &
        name,group,switch_short,switch_long, &
        metavar,nargs,default_value,         &
        help,action,important) result(arg)

        implicit none
        type(Argument)                        :: arg
        character(len=*),intent(in)           :: name
        class(Argument_Group),target,optional :: group
        character(len=*),intent(in) ,optional :: switch_short
        character(len=*),intent(in) ,optional :: switch_long
        character(len=*),intent(in) ,optional :: metavar
        character(len=*),intent(in) ,optional :: nargs
        character(len=*),intent(in) ,optional :: default_value
        character(len=*),intent(in) ,optional :: help
        character(len=*),intent(in) ,optional :: action
        logical         ,intent(in) ,optional :: important

        !-- Assign properties to object
                                    arg % name          =  trim(name)
        if (present(group))         arg % group         => group
        if (present(metavar))       arg % metavar       =  trim(metavar)
        if (present(nargs))         arg % nargs         =  trim(nargs)
        if (present(default_value)) arg % default_value =  trim(default_value)
        if (present(help))          arg % help          =  trim(help)
        if (present(important))     arg % important     =  important

        !-- Short switch
        if (present(switch_short)) then
            if (switch_short(1:1) == '-') then
                arg % switch_short =      trim(switch_short)
            else
                arg % switch_short = '-'//trim(switch_short)
            end if
        end if

        !-- Long switch
        if (present(switch_long)) then
            if (switch_long(1:2) == '--') then
                arg % switch_long =       trim(switch_long)
            else
                arg % switch_long = '--'//trim(switch_long)
            end if
        end if

        if (present(action)) then
            if (action == ACTION_TYPE_STORE_TRUE .or.  &
                action == ACTION_TYPE_STORE_FALSE .or. &
                action == ACTION_TYPE_STORE_VALUE) then
                arg % action_type = action
            else
                error stop 'ERROR: Invalid action type specified.'
            end if
        else
            arg % action_type = ACTION_TYPE_STORE_VALUE
        end if

        ! Arg must be positional if it does not have any switches
        if ((.not. present(switch_short)) .and. (.not. present(switch_long))) then
            arg % positional = .true.
        end if

!-- Error checking
#if defined(debug) || defined(DEBUG)
        ! Positional arguments without switches must have nargs set
        if ((.not. present(switch_short)) .and. &
            (.not. present(switch_long))  .and. &
            (.not. present(nargs))) then
            error stop 'ERROR: Switchless/positional arguments must have nargs set.'
        end if

        ! Check short switch length
        if (present(switch_short)) then
            if (len(arg % switch_short) > 2) error stop 'ERROR: Short switch must be 1 character (e.g., -a)'
        end if

        ! Verify short switches are not numeric
        if (present(switch_short)) then
            if (verify(arg % switch_short(2:2),set='-0123456789') == 0) then
                error stop 'ERROR: Short switches must not be numeric because they conflict with negative number arguments (e.g., -1)'
            end if
        end if

        ! Logical flags should have no other argments
        if (arg % action_type == ACTION_TYPE_STORE_TRUE .or. &
            arg % action_type == ACTION_TYPE_STORE_FALSE) then
            if (allocated(arg % nargs)) then
                if (arg % nargs /= '0') then
                    error stop 'ERROR: Arguments with action StoreTrue or StoreFalse must have no other arguments (nargs=0 or unspecified).'
                end if
            end if
        end if

        ! Default value must be specified for optional arguments
        if (arg % nargs == NARGS_ONE_OPTIONAL_QMARK) then
            if (.not. present(default_value)) then
                error stop 'ERROR: Optional arguments must have a default value.'
            end if
        end if

#endif

        !-- Read character nargs into nargs_int if it appears to be a number
        if (allocated(arg % nargs)) then
            if (arg % nargs /= NARGS_MULTIPLE_OPTIONAL_STAR .and. &
                arg % nargs /= NARGS_MULTIPLE_PLUS          .and. &
                arg % nargs /= NARGS_ONE_OPTIONAL_QMARK) then
                ! nargs should be a number
                read (arg % nargs,*) arg % nargs_int
            end if
        end if

        ! Set nargs to 1 if not otherwise set for stored value arguments
        if (arg % action_type == ACTION_TYPE_STORE_VALUE .and. .not. present(nargs)) then
            arg % nargs_int = 1
        end if

        ! Set default_values
        if (arg % action_type == ACTION_TYPE_STORE_FALSE) arg % default_value = 'T'
        if (arg % action_type == ACTION_TYPE_STORE_TRUE ) arg % default_value = 'F'

        if (.not. allocated(arg % default_value)) then
            arg % default_value = unspecified_default_value
        end if

    end function

    !> Print help information for this argument (name/switch and description)
    subroutine print_help(self)
        class(Argument),intent(inout) :: self !< argument to print
        integer,parameter :: width_arg_text = 25 !< width of argument column when printed
        character(len=width_arg_text) :: arg_text
        character(len=:),allocatable  :: print_name

        ! Exit if this is not an allocated arg
        if (.not. allocated(self % name)) return

        ! Print group name first (if applicable)
        if (associated(self % group)) call self % group % print_info

        ! Print argument name or switch
        if (self % positional) then
            write (arg_text,'(a)') self % name

        else if (allocated(self % switch_short) .and. allocated(self % switch_long)) then
            if (self % action_type == ACTION_TYPE_STORE_VALUE) then
                if (allocated(self % metavar)) then
                    print_name = '<' // self % metavar // '>'
                else
                    print_name = '<' // self % name // '>'
                end if
                write (arg_text,'(a)') self % switch_short // ', ' // self % switch_long // '=' // print_name
            else
                write (arg_text,'(a)') self % switch_short // ', ' // self % switch_long
            end if

        else if (allocated(self % switch_short)) then
              write (arg_text,'(a)') self % switch_short

        else if (allocated(self % switch_long)) then
              write (arg_text,'(a)') self % switch_long

        end if

        ! Print name/switch generated above and help information
        write (*,*) arg_text // self % help

    end subroutine

    !> Print usage information for this argument ([-o <name>])
    subroutine print_usage(self)
        class(Argument),intent(inout) :: self !< argument to print
        character(len=:),allocatable  :: print_name

        ! Exit if this is not an allocated arg
        if (.not. allocated(self % name)) return

        ! Print metavar or actual name
        if (allocated(self % metavar)) then
            print_name = '<' // self % metavar // '>'
        else
            print_name = '<' // self % name // '>'
        end if

        ! Print positional arguments
        if (self % positional) then
            write (*,'(a)',advance='no') ' ' // print_name
        end if

        ! Print switched arguments if important
        if (self % important .and. (.not. self % positional)) then

            write (*,'(a)',advance='no') ' ['

            ! Write switch
            if (allocated(self % switch_short)) then
                write (*,'(a)',advance='no') self % switch_short
            else if (allocated(self % switch_long)) then
                write (*,'(a)',advance='no') self % switch_long
            end if

            ! Write zero or more arguments
            if (self % nargs == NARGS_MULTIPLE_OPTIONAL_STAR) then
                write (*,'(a)',advance='no') ' [' // print_name // ']...'

            ! Write one or more arguments
            else if (self % nargs == NARGS_MULTIPLE_PLUS) then
                write (*,'(a)',advance='no') ' ' // print_name // ' [' // print_name // ']...'

            ! Write fixed number of arguments
            else if (self % nargs_int /= 0) then
                block
                    integer :: i
                    do i = 1, self % nargs_int
                        write (*,'(a)',advance='no') ' ' // print_name
                    end do
                end block
            end if

            write (*,'(a)',advance='no') ']'

        end if

    end subroutine

    !> Determine wether or not this argument has already received all of its values
    logical function is_configured(self)
        class(Argument) :: self

        !-- Check for single values storage
        if (self % nargs_int == 1) then
            if (allocated(self % value)) then
                is_configured = .true.
                return
             else
                 is_configured = .false.
                 return
             end if
        end if

        !-- Check for multiple values storage
        if (self % nargs_int >= 0) then
            if (allocated(self % values_list)) then
                if (size(self % values_list) >= self % nargs_int) then
                    is_configured = .true.
                    return
                else
                    is_configured = .false.
                    return
                end if
            else
                is_configured = .false.
                return
            end if
        end if

        !-- Not configured
        print*,'DEBUG: is_configured fall through for argument: ', self % name
        is_configured = .false.
        return
    end function

    !> Nullifies Argument_Group pointer prior to destroying self
    pure subroutine destroy_argument(self)
        type(Argument),intent(inout) :: self !< argument to destroy

        if (associated(self % group)) then
            nullify(self % group)
        end if
    end subroutine

end module
