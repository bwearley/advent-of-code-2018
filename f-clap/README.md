# f-clap: Fortran Command Line Argument Parser

A Fortran implementation of Python's argparse

f-clap can parse Unix-style command line arguments passed to a Fortran program. It supports positional arguments, short switches, and long switches. Switches can also accept a defined number of arguments, which may be passed with or without an equals sign.

~~~~
./executable arg1 arg2 -c -t -r --long-switch --long-argument=arg --another-long-argument arg3
~~~~

Short switches may also be combined.
~~~~
./executable -ctr
~~~~

f-clap will automatically generate formatted help content when executed with a help switch, e.g., `./exe -h` or `./exe --help`

Reference-style: 
![F-CLAP Help](https://gitlab.com/bwearley/f-clap/raw/master/f-clap.png)

## Configuring f-clap
f-clap can be configured in `fclap_config_smod.f90`. Arguments can be added and grouped in code, for example:

~~~~
call add_argument_group(Argument_Group('Options',desc='Optional switches for added functionality'))
    
call add_argument( &
    Argument( &
        name='tag',group=arg_groups(group_idx), &
        switch_short='-t',switch_long='--tag',  &
        help='Tag to append to output files',   &
        important=.true.), &
    get_index=tag_idx)
~~~~

## Reading inputs
Inputs from the command line are stored as `character` strings and can be read and used a variety of ways, as illustrated below:

~~~~
subroutine test_argparse
    use argparse_mod
    implicit none
    
    !-- Test Variables
    type(Argument) :: a
    character(len=:),allocatable :: t1
    character(len=mxclen),allocatable :: t2(:)
    real :: test_real1

    ! Make temporary copy of argument data into `a`
    a = get_argument_named('hull-gamma')

    ! Print whether `a' is configured
    print*, "Argument `a' is configured:", a % is_configured()

    ! Get value stored in full-gamma at command line (as string)
    t1 = trim(get_value_for_arg('full-gamma'))

    ! Test the output of t1
    print*,'Output stored in full-gamma: (',t1, ')'

    ! Read string t1 into a real variable
    read (t1,*) test_real1

    ! Get multiple stored values from argument extras
    t2 = get_values_for_arg('extras')

    ! Print 1st stored argument in extras
    print*,'Extras, first argument: ',trim(t2(1))

    ! Print 3rd stored argument in extras
    print*,'Extras, third argument: ',trim(t2(3))

end subroutine
~~~~

## Compilation Options

f-clap code contains pre-processor directives based on the following definitions passed to the compiler:

`-Ddebug`
: Compile with debug options: useful for initial configuration of f-clap but not needed once options have been fully configured

`-DWIN32`
: Compile for Windows: support Windows-like program paths with backslashes

`-DBASIC_TERMINAL`
: Compile without using special terminal escape characters for pretty formatting