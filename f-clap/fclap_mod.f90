!-----------------------------------------------------------------------
! Fortran Command Line Argument Parser (f-clap)
!-----------------------------------------------------------------------
!
! MODULE:
! fclap_mod
!
! AUTHOR:
! Brian Earley
!
! DESCRIPTION:
!> Provides command line argument parsing capabilities similar to
!! Python's argparse package
!-----------------------------------------------------------------------

module fclap_mod
    use argument_group_class
    use argument_class
    use fclap_settings_mod
    implicit none

    integer :: arg_idx   = 0 !< counter pointing to argument index 
    integer :: group_idx = 0 !< counter pointing to group index

    type(Argument_Group),public,allocatable,target :: arg_groups(:)
    type(Argument)      ,public,allocatable,target :: arguments(:)

    !-- Define built-in switches
    character(len=*),parameter,private :: HELP_ARGUMENT        = 'help'
    character(len=*),parameter,private :: HELP_SWITCH_SHORT    = '-h'
    character(len=*),parameter,private :: HELP_SWITCH_LONG     = '--help'
    character(len=*),parameter,private :: VERSION_ARGUMENT     = 'version'
    character(len=*),parameter,private :: VERSION_SWITCH_SHORT = '-v'
    character(len=*),parameter,private :: VERSION_SWITCH_LONG  = '--version'
    
    !> Configures the Arguments and Argument_Groups
    !! Implementation deferred to fclap_config_smod
    interface
        module subroutine configure_fclap
        end subroutine
    end interface

contains

    !> Add a passed Argument type to arguments array
    subroutine add_argument(arg, get_index)
        implicit none
        class(Argument),intent(in)     :: arg
        integer,optional,intent(inout) :: get_index

        arg_idx = arg_idx + 1
        arguments(arg_idx) = arg

        ! Pass back self-index if requested
        if (present(get_index)) get_index = arg_idx
    end subroutine

    !> Add a passed Argument_Group type to arg_groups array
    subroutine add_argument_group(arg_grp)
        implicit none
        class(Argument_Group),intent(in) :: arg_grp 

        group_idx = group_idx + 1
        arg_groups(group_idx) = arg_grp
    end subroutine

    !> Print version information
    subroutine print_version
        implicit none

        write (*,*) get_executable() // ' ' // version_string
        
        stop
    end subroutine

    !> Return current executable name excluding path
    function get_executable() result(exe)
        integer,parameter            :: max_len_exe = 100
        character(len=max_len_exe)   :: executable
        integer                      :: last_slash_location !< location in executable string of final slash (/)
        character(len=:),allocatable :: exe

        ! Get command used to invoke program
        call GET_COMMAND_ARGUMENT(0,executable)

        ! Determine substring location of path
#if defined(_WIN32) || defined(WIN32) || defined(WINDOWS)
        ! C:\Path\to\executable.exe -> executable.exe
        last_slash_location = index(string=executable,substring='\',back=.true.)
#else
        ! ./path/to/executable -> executable
        last_slash_location = index(string=executable,substring='/',back=.true.)    
#endif

        ! Trim path
        if (last_slash_location /= 0) then
            exe = trim(executable(last_slash_location+1:))
        else
            exe = trim(executable)
        end if
    
    end function

    !> Print usage information for program
    subroutine print_usage
        use terminal_tools_mod
        implicit none
        integer :: a_idx !< argument index

        write (*,*) TERM_BOLD//'USAGE'//TERM_END

        write (*,'(a)',advance='no') ' $ '// get_executable()

        do a_idx = 1, size(arguments)
            call arguments(a_idx) % print_usage
        end do
    end subroutine

    !> Print help information for program
    subroutine print_help
        use terminal_tools_mod
        implicit none
        integer :: a_idx !< argument index

        ! Print usage information (including executable name and important switches)
        call print_usage
        write (*,*)
        write (*,*)

        write (*,*) TERM_BOLD//'ARGUMENTS'//TERM_END
        
        ! Print detailed help information
        do a_idx = 1, size(arguments)
            call arguments(a_idx) % print_help
        end do

        write (*,*)

    end subroutine

    !> Parse arguments passed to command line and store argument values in Argument types
    subroutine parse_command_line_arguments
        implicit none
        integer                :: cli_idx              !< index of argument input from cmd line
        integer                :: arg_idx              !< index of Argument in arguments() array
        integer                :: compound_idx         !< index for looping through compound basic flags
        character(len=mxclen)  :: current_arg          !< store current argument passed
        type(Argument),pointer :: active_arg => null() !< points to argument currently collecting values
        integer                :: eq_idx               !< location of equals in argument (e.g., index of '=' in '--some-switch=1.0')

        !-- Handle built-in arguments
        call get_command_argument(1, current_arg)
        select case (current_arg)
            case (HELP_ARGUMENT,HELP_SWITCH_SHORT,HELP_SWITCH_LONG)
                call print_help
                stop! hammer time
            case (VERSION_ARGUMENT,VERSION_SWITCH_SHORT,VERSION_SWITCH_LONG)
                call print_version
                stop
        end select

        !-- Insufficient number of arguments -- print usage and quit
        if (command_argument_count() < min_args_passed) then
            call print_usage
            write (*,*)
            write (*,*)
            write (*,*) 'Run with '//HELP_SWITCH_SHORT//' or '//HELP_SWITCH_LONG//' for more info.'
            stop
        end if

        !-- Loop through each argument from command line
        CLI_LOOP: do cli_idx = 1, command_argument_count()
            call get_command_argument(cli_idx, current_arg)

            ! Reset compound index when cycling CLI_LOOP
            compound_idx = 2

            !-- Loop through the configured arguments
            CONFIGURED_ARGS_LOOP: do arg_idx = 1, size(arguments)

                !-- New argument
                if (current_arg == arguments(arg_idx) % switch_short .or. &
                    current_arg == arguments(arg_idx) % switch_long) then

                    if (arguments(arg_idx) % action_type == ACTION_TYPE_STORE_VALUE) then
                        ! Begin pointing at new argument
                        active_arg => arguments(arg_idx)
                    else
                        ! Clear pointer, arg is no longer active (one and done)
                        active_arg => null()
                        if (arguments(arg_idx) % action_type == ACTION_TYPE_STORE_TRUE) then
                            arguments(arg_idx) % value = 'T'
                        else if (arguments(arg_idx) % action_type == ACTION_TYPE_STORE_FALSE) then
                            arguments(arg_idx) % value = 'F'
                        else
                           !print*,'DEBUG: Fall through'
                            continue
                        end if
                    end if

                    cycle CLI_LOOP

                !-- Flag to Positional Terminator (--)
                else if (trim(current_arg) == '--') then
                    active_arg => null()
                
                !-- Compound flags
                else if (current_arg(1:1) == '-' .and. current_arg(2:2) /= '-' .and. trim(current_arg(3:)) /= '') then

                        if ('-'//current_arg(compound_idx:compound_idx) == arguments(arg_idx) % switch_short) then

                            !-- Process true/false flags
                            if      (arguments(arg_idx) % action_type == ACTION_TYPE_STORE_TRUE) then
                                arguments(arg_idx) % value = 'T'
                            else if (arguments(arg_idx) % action_type == ACTION_TYPE_STORE_FALSE) then
                                arguments(arg_idx) % value = 'F'
                            else
                                error stop 'ERROR: Compound switches can only be used for store_true and store_false arguments.'
                            end if
                            compound_idx = compound_idx + 1
                        end if

                        if (compound_idx > len(trim(current_arg))) then
                            ! Reset counter and move on to next argument passed to CLI
                            compound_idx = 2
                            cycle CLI_LOOP
                        else
                            ! Continue reading compound arguments
                            cycle CONFIGURED_ARGS_LOOP
                        end if
                
                !-- Argument and value (e.g., --some-switch=1.0)
                else if (index(current_arg,'=') > 0) then
                    eq_idx = index(current_arg,'=')
                   
                    !-- Split argument string at location of '=', read value
                    if (current_arg(1:eq_idx-1) == arguments(arg_idx) % switch_short .or. &
                        current_arg(1:eq_idx-1) == arguments(arg_idx) % switch_long) then
                        arguments(arg_idx) % value = current_arg(eq_idx+1:)
                        cycle CLI_LOOP
                    end if

                    cycle CONFIGURED_ARGS_LOOP

                !-- Read positional arguments or option arguments
                else
                
                    !-- Optional arguments
                    if (associated(active_arg)) then
                        
                        !-- Save values
                        if (active_arg % nargs_int == 1 .or. &
                            active_arg % nargs == NARGS_ONE_OPTIONAL_QMARK) then
                            
                            !-- Save single value
                            active_arg % value = current_arg

                            !-- No more args accepted, nullify
                            nullify(active_arg)

                            cycle CLI_LOOP
                        
                        else
                            !-- Save multiple values (uses reallocation--slow, but not in a hot loop)                        
                            if (allocated(active_arg % values_list)) then
                                active_arg % values_list = [active_arg % values_list, current_arg]
                            else
                                active_arg % values_list = [current_arg]
                            end if

                            !-- If no more args accepted, nullify pointer
                            if (active_arg % nargs /= NARGS_MULTIPLE_PLUS .and. &
                                active_arg % nargs /= NARGS_MULTIPLE_OPTIONAL_STAR) then
                                if (active_arg % nargs_int > 0) then
                                    if (size(active_arg % values_list) >= active_arg % nargs_int) then
                                        nullify(active_arg)
                                    end if
                                end if
                            end if
                            
                            cycle CLI_LOOP

                        end if
                    
                    !-- Positional arguments
                    else

                        ARG_ASSOC: associate (this_arg => arguments(arg_idx))
                            if (this_arg % positional) then

                                !-- Check if this positional argument has already been configured
                                if (this_arg % is_configured()) cycle CONFIGURED_ARGS_LOOP
                                
                                !-- Save one value for single value arg
                                if (this_arg % nargs_int == 1 .and. .not. allocated(this_arg % value)) then
                                    this_arg % value = current_arg
                                
                                !-- Save one value for multiple value arg
                                else if (this_arg % nargs_int > 0 .and. size(this_arg % values_list) < this_arg % nargs_int) then
                                    
                                    !-- Save multiple values (uses reallocation--slow, but not in a hot loop)
                                    this_arg % values_list = [this_arg % values_list, current_arg]
    
                                !-- Save one value for multiple value arg
                                else if (this_arg % nargs == NARGS_MULTIPLE_PLUS .or. &
                                         this_arg % nargs == NARGS_MULTIPLE_OPTIONAL_STAR) then
    
                                    !-- Save multiple values (uses reallocation--slow, but not in a hot loop)
                                    this_arg % values_list = [this_arg % values_list, current_arg]
    
                                else
                                    ! Go to next arg, this one is already full/completed
                                    cycle CONFIGURED_ARGS_LOOP
                                end if
                                cycle CLI_LOOP
                            end if
                        end associate ARG_ASSOC
                    end if
                
                end if

                !-- Unknown Argument
                if (fail_on_unknown_argument .and. arg_idx == size(arguments)) then
                    write (*,*) 'Failure: Unknown argument: ',current_arg
                    error stop
                end if
            end do CONFIGURED_ARGS_LOOP
        end do CLI_LOOP

        !-- Nullify argument pointer
        if (associated(active_arg)) nullify(active_arg)
    end subroutine

    !> Print out argument information grabbed from command line
    subroutine print_debug_information
        implicit none
        integer :: arg_idx !< index of Argument in arguments() array
        integer :: i       !< index of argument in values_list

        write (*,*) 'F-CLAP DEBUG INFORMATION'
        write (*,*) '+----------------------------+'

        do arg_idx = 1, size(arguments)
            
            ARG_ASSOC: associate (this_arg => arguments(arg_idx))
                
                ! Name
                write (*,*) 'Name: ', trim(this_arg % name)

                ! Type
                select case (this_arg % action_type)
                case (ACTION_TYPE_STORE_FALSE)
                    write (*,*) 'Type: Store False'
                case (ACTION_TYPE_STORE_TRUE)
                    write (*,*) 'Type: Store True'
                case (ACTION_TYPE_STORE_VALUE)
                    write (*,*) 'Type: Store Value'
                case default
                end select
                
                ! Stored value(s)
                if (allocated(this_arg % value)) then
                    write (*,*) 'Value: ', trim(this_arg % value)
                
                else if (allocated(this_arg % values_list)) then
                    write (*,*) 'Values: '
                    do i = 1, size(this_arg % values_list)
                        write (*,'(x,i4,a,a)') i, ': ', trim(this_arg % values_list(i))
                    end do
                
                else
                    write (*,*) 'Uninitialized'
                end if
            
                write (*,*) '+----------------------------+'
            
            end associate ARG_ASSOC
        end do
    end subroutine

    !> Returns an Argument type searched by name
    type(Argument) function get_argument_named(name) result(arg)
        implicit none
        character(len=*),intent(in) :: name
        integer                     :: arg_idx
        
        do arg_idx = 1, size(arguments)
            if (arguments(arg_idx) % name == name) then
                arg = arguments(arg_idx)
                return
            end if
        end do

        write (*,*) 'ERROR: Unknown argument: ', name
        error stop
    end function

    !> Returns the stored value of an argument searched by name
    function get_value_for_arg(name) result(val)
        implicit none
        character(len=*),intent(in)  :: name
        character(len=:),allocatable :: val
        type(Argument)               :: arg_tmp

        arg_tmp = get_argument_named(name)
        if (arg_tmp % is_configured()) then
            val = arg_tmp % value
            return
        else
           !val = ''
            val = arg_tmp % default_value
            return
        end if
    end function

    !> Returns all stored values of an argument searched by name
    function get_values_for_arg(name) result(vals)
        implicit none
        character(len=*),intent(in)       :: name
        character(len=mxclen),allocatable :: vals(:)
        type(Argument)                    :: arg_tmp

        arg_tmp = get_argument_named(name)
        if (arg_tmp % is_configured()) then
            vals = arg_tmp % values_list
            return
        else
           !vals = ['']
            vals = [ arg_tmp % default_value ]
            return
        end if
    end function

    !> Returns whether an argument searched by name is configured
    function arg_is_configured(name)
        implicit none
        character(len=*),intent(in) :: name
        logical                     :: arg_is_configured
        type(Argument)              :: arg_tmp

        arg_tmp = get_argument_named(name)
        if (arg_tmp % is_configured()) then
            arg_is_configured = .true.
            return
        else
            arg_is_configured = .false.
            return
        end if
    end function

end module