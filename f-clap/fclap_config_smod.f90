!-----------------------------------------------------------------------
! Fortran Command Line Argument Parser (f-clap)
!-----------------------------------------------------------------------
!
! MODULE:
! fclap_config_smod
!
! AUTHOR:
! Brian Earley
!
! DESCRIPTION:
!> Defines the F-CLAP arguments
!-----------------------------------------------------------------------

submodule (fclap_mod) fclap_config_smod
    implicit none

contains

    module procedure configure_fclap
        use fclap_mod
        implicit none

        integer :: tag_idx
        
        !-- Allocate
        allocate(arg_groups(max_num_groups))
        allocate(arguments(max_num_arguments))
    
        call add_argument(                    &
            Argument(                         &
                name='input_file',            &
                nargs='1',                    &
                help='Input file for the day' &
            )                                 &
        )

    end procedure

end submodule