!-----------------------------------------------------------------------
! Fortran Command Line Argument Parser (f-clap)
!-----------------------------------------------------------------------
!
! MODULE:
! argument_group_class
!
! AUTHOR:
! Brian Earley
!
! DESCRIPTION:
!> Defines the Argument_Group type which contains the name and
!! description of groups of Arguments
!-----------------------------------------------------------------------

module argument_group_class
    implicit none
    private
  
    !-- Argument_Group
    type,public :: Argument_Group
        private
        character(len=:),allocatable :: name              !<
        character(len=:),allocatable :: desc              !<
        logical                      :: printed = .false. !<
    contains
        procedure :: print_info
    end type

    interface argument_group
        module procedure init_argument_group
    end interface

contains

    !> Create and return an Argument_Group
    function init_argument_group(name,desc) result(grp)
        implicit none
        type(Argument_Group)                 :: grp  !<
        character(len=*),intent(in)          :: name !<
        character(len=*),intent(in),optional :: desc !<

        grp % name = trim(name)

        if (present(desc)) grp % desc = trim(desc)

    end function

    !> Prints group information if not already printed
    subroutine print_info(self)
        use terminal_tools_mod
        implicit none
        class(Argument_Group),intent(inout) :: self

        if (.not. allocated(self % name)) return

        if (.not. self % printed) then
            write (*,*)
            write (*,*) term_bold // to_upper(self % name) // term_end
            if (allocated(self % desc)) then
                write (*,*) self % desc
            end if
            self % printed = .true.
        end if
    end subroutine

    function to_upper(str_in) result(str_out)
        implicit none
        character(len=*), intent(in) :: str_in
        character(len=len(str_in))   :: str_out
        integer                      :: i, j

        do i = 1, len(str_in)
            j = iachar(str_in(i:i))
            if (j >= iachar("a") .and. j<=iachar("z") ) then
               str_out(i:i) = achar(iachar(str_in(i:i))-32)
            else
               str_out(i:i) = str_in(i:i)
            end if
       end do

    end function

end module