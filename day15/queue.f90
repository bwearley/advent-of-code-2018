!
! Module library implementing elementary Queue routines.
! (c) 1992-2010, A. Migdalas @ MSCC
! version 0.7 @ 2009/11/08
!
! BE: I took IntQueue.f90 and modified it to hold queues of
! arbitrary QITEM types


MODULE Queue
  INTEGER :: IK = 0
!
! QUEUE must be implemented by client as Integer Array.
!       Client is responsible for checking the array bounds.
! FIRST is a cursor that keeps track of the Front of the Queue.
! LAST is a cursor that keeps track of the Rear of the Queue.
!      Both FIRST and LAST must be initialized to 0 by client to
!      denote empty Queue. Client is responsible for keeping 
!      them within bounds.

  TYPE :: QITEM
    INTEGER :: X = 0
    INTEGER :: Y = 0
    INTEGER :: DIST = HUGE(IK)
  END TYPE QITEM

CONTAINS

  !
  ! Enqueues node, i.e. adds it to the rear of the Queue.
  ! If Queue size has been exceeded, it does nothing and 
  ! returns without error.  Client is responsible to
  ! check for Queue overflow.
  !

  SUBROUTINE add_to_queue(node,queue,last)
    TYPE(QITEM), INTENT(IN) ::  node
    INTEGER, INTENT(INOUT) :: last
    TYPE(QITEM), INTENT(INOUT), DIMENSION(:) :: queue

    IF ( last < SIZE(queue) ) THEN
       last = last + 1
       queue(last) = node
    END IF

  END SUBROUTINE add_to_queue

  !
  ! Dequeues front, i.e. deletes the front node from queue.
  ! If Queue is empty, it does nothing. Client is responsible
  ! to check  for violations of Queue size.
  !

  SUBROUTINE delete_from_queue(first,last)
    INTEGER, INTENT(INOUT) :: first
    INTEGER, INTENT(IN) :: last

    IF ( first < last ) THEN
       first = first + 1
    END IF

  END SUBROUTINE delete_from_queue

  !
  ! Selects the front element from queue without deleting it.
  ! If Queue is empty, it does nothing. Client is responsible
  ! to check  for violations of Queue size.
  !

  SUBROUTINE select_from_queue(node,queue,first,last)
    INTEGER, INTENT(IN) :: last,first
    TYPE(QITEM), INTENT(OUT) :: node
    TYPE(QITEM), INTENT(IN), DIMENSION(:) :: queue

    IF ( first < last ) THEN
       node = queue(first+1)
    ENDIF

  END SUBROUTINE select_from_queue

END MODULE Queue