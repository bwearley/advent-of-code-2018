module map_mod
    use syslog_mod
    use string_tools_mod
    use get_digit_of_number_mod
    use unit_mod
    use game_mod
    use Queue
    implicit none

    !-- x,y coordinates in dijkstra map
    integer :: xdij
    integer :: ydij

    !-- Overall map dimensions
    integer :: x_dim = 0
    integer :: y_dim = 0

    type :: Position
        integer :: x = 0
        integer :: y = 0
        integer :: dist = HUGE(x_dim)
        integer :: hp = HUGE(x_dim)
    end type

    ! Point direction definitions
    integer,parameter :: POINT_ABOVE = 1
    integer,parameter :: POINT_LEFT  = 2
    integer,parameter :: POINT_RIGHT = 3
    integer,parameter :: POINT_BELOW = 4

    type(Position),allocatable :: target_positions(:)
    type(Position) :: these_targets(4)

    integer :: self_surrounding_distances(4)


    !-- MapSquareType type definitions
    enum,bind(C)
        enumerator :: MapSquareTypeWall
        enumerator :: MapSquareTypeOpen
    end enum

    !-- MapSquare struct
    type :: MapSquare
        integer :: x = -1
        integer :: y = -1
        integer :: MapSquareType = MapSquareTypeWall
        character(len=1) :: a
        type(Unit),pointer :: unit => null()
    contains
        procedure :: is_open
    end type

    type(MapSquare), allocatable :: map(:,:)

    type :: DijkstraMapSquare
        integer :: x = -1
        integer :: y = -1
        integer :: distance = Huge(x_dim)
        logical :: visited = .false.
    end type

    type(DijkstraMapSquare), allocatable :: dijmap(:,:)

contains

    integer function minimum_distance_point_to_point(x0,y0,x1,y1) result(dist)
        integer :: x0,x1
        integer :: y0,y1
        integer :: x,y
        TYPE(QITEM) :: queue(0:x_dim*y_dim)
        type(QITEM) :: node
        integer :: first, last

        first = 0
        last = 0
        
        call reset_dijkstra
        
        ! Mark walls / currently occupied squares visited
        do x = 1, x_dim
            do y = 1, y_dim
                if (.not. map(x,y) % is_open()) then
                    dijmap(x,y) % visited = .true.
                else
                    dijmap(x,y) % visited = .false.
                end if
            end do
        end do

        ! Mark starting point as visited
        dijmap(x0,y0) % visited = .true.
        call add_to_queue(Qitem(x=x0,y=y0,dist=0),queue,last)

        ! BFS
        do while (first /= last) !(last /= 0)
            node = queue(first)
            call delete_from_queue(first,last)

            ! Destination found
            if (node%x == x1 .and. node%y == y1) then
                dist = node%dist
                return
            end if

            ! Moving y up
            if (node%y+1 <= y_dim) then
                if (.not. dijmap(node%x,node%y+1) % visited) then
                    call add_to_queue(Qitem(x=node%x,y=node%y+1,dist=node%dist+1),queue,last)
                    dijmap(node%x,node%y+1) % visited = .true.
                end if
            end if
            
            ! Moving y down
            if (node%y-1 >= 1) then
                if (.not. dijmap(node%x,node%y-1) % visited) then
                    call add_to_queue(Qitem(x=node%x,y=node%y-1,dist=node%dist+1),queue,last)
                    dijmap(node%x,node%y-1) % visited = .true.
                end if
            end if

            ! Moving x left
            if (node%x-1 >= 1) then
                if (.not. dijmap(node%x-1,node%y) % visited) then
                    call add_to_queue(Qitem(x=node%x-1,y=node%y,dist=node%dist+1),queue,last)
                    dijmap(node%x-1,node%y) % visited = .true.
                end if
            end if

            ! Moving x right
            if (node%x+1 <= x_dim) then
                if (.not. dijmap(node%x+1,node%y) % visited) then
                    call add_to_queue(Qitem(x=node%x+1,y=node%y,dist=node%dist+1),queue,last)
                    dijmap(node%x+1,node%y) % visited = .true.
                end if
            end if

        end do
        dist = HUGE(x)
        return

    end function

    subroutine reset_dijkstra
        implicit none
        dijmap(:,:) % distance = Huge(x_dim)
        dijmap(:,:) % visited = .false.
    end subroutine

    logical function is_open(self)
       class(MapSquare),intent(in) :: self

       if (self % MapSquareType == MapSquareTypeOpen .and. .not. associated(self % unit)) then
           is_open = .true.
           return
       else
           is_open = .false.
           return
       end if
    end function

    ! recursive logical function floodfill(x0,y0,xdest,ydest) result(pathable)
    !     integer :: x0, y0, xdest, ydest
    !     if (x0 == xdest .and. y0 == ydest) then
    !         pathable = .true.
    !         return
    !     end if
    !     if (x0 >= x_dim .or. y0 >= y_dim) then
    !         pathable = .false.
    !         return
    !     end if
    !     if (dijmap(x0,y0) % visited) then
    !         pathable = .false.
    !         return
    !     end if
    !     if (.not. map(x0,y0) % is_open()) then
    !         pathable = .false.
    !         return
    !     end if
            
    !     if (floodfill(x0+1, y0,  xdest, ydest)) then
    !         pathable = .true.
    !         return
    !     end if
    !     if (floodfill(x0-1, y0, xdest, ydest)) then
    !         pathable = .true.
    !         return
    !     end if
    !     if (floodfill(x0, y0+1, xdest, ydest)) then
    !         pathable = .true.
    !         return
    !     end if
    !     if (floodfill(x0, y0-1, xdest, ydest)) then
    !         pathable = .true.
    !         return
    !     end if
    !     pathable = .false.
    !     return
    ! end function

    function points_in_range_of_point(x,y) result(pts)
        integer,intent(in) :: x
        integer,intent(in) :: y
        integer :: i
        integer :: pt(2) = -1
        type(Position) :: pts(4)

        ! Check point above
        i = POINT_ABOVE
        pt(:) = [ x, y-1]
        if (pt(2) /= 0) then
            if (map(pt(1),pt(2)) % is_open()) then
                pts(i) % x = pt(1)
                pts(i) % y = pt(2)
            end if
        end if
        
        ! Check point left
        i = POINT_LEFT
        pt(:) = [ x-1, y]
        if (pt(1) /= 0) then
            if (map(pt(1),pt(2)) % is_open()) then
                pts(i) % x = pt(1)
                pts(i) % y = pt(2)
            end if
        end if

        ! Check point right
        i = POINT_RIGHT
        pt(:) = [ x+1, y]
        if (pt(1) /= x_dim+1) then
            if (map(pt(1),pt(2)) % is_open()) then
                pts(i) % x = pt(1)
                pts(i) % y = pt(2)
            end if
        end if

        ! Check point below
        i = POINT_BELOW
        pt(:) = [ x, y+1]
        if (pt(2) /= y_dim+1) then
            if (map(pt(1),pt(2)) % is_open()) then
                pts(i) % x = pt(1)
                pts(i) % y = pt(2)
            end if
        end if

    end function

    !> Write out the current state of map
    subroutine write_state
        implicit none
        integer :: x, y

        write (syslog%unit,'(a,i0)') 'Round: ', round

        !-- Write header
        ! Tens Place
        write (syslog%unit,'(a)', advance='no') '  '
        do x = 1, x_dim
            write (syslog%unit,'(i1)',advance='no') get_digit_of_number(2,x)
        end do
        write (syslog%unit,*) ! advance
        ! Ones Place
        write (syslog%unit,'(a)', advance='no') '  '
        do x = 1, x_dim
            write (syslog%unit,'(i1)',advance='no') get_digit_of_number(1,x)
        end do
        write (syslog%unit,*) ! advance

        !-- Grid
        do y = 1, y_dim
            write (syslog%unit,'(i0.2)',advance='no') y
            do x = 1, x_dim
                write (syslog%unit,'(a)',advance='no') map(x,y) % a
            end do
            write (syslog%unit,*) ! advance
        end do
    end subroutine
end module
