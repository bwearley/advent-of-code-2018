module unit_mod
    use syslog_mod
    use string_tools_mod
    implicit none

    !-- Parameter Settings
    integer,parameter :: MAX_UNITS_PER_TEAM = 40

    integer,parameter :: DEFAULT_ATTACK_POWER = 3
    integer,parameter :: DEFAULT_HIT_POINTS = 200

    integer           :: ELF_ATTACK_POWER = 3   ! Need 23 to win
    integer,parameter :: GOBLIN_ATTACK_POWER = DEFAULT_ATTACK_POWER

    integer,parameter :: ELF_HIT_POINTS = DEFAULT_HIT_POINTS
    integer,parameter :: GOBLIN_HIT_POINTS = DEFAULT_HIT_POINTS

    !-- UnitType type definitions
    enum,bind(C)
        enumerator :: UnitTypeNone
        enumerator :: UnitTypeElf
        enumerator :: UnitTypeGoblin
    end enum
   
    ! Unit definition
    type :: Unit
        integer :: id = 0
        integer :: x = 0
        integer :: y = 0
        integer :: hp = 0
        integer :: attack_power = 0
        logical :: dead = .false.
        integer :: touched_last_round = -1
        integer :: unit_type = UnitTypeNone
    end type

    !-- Elf/Goblin Counters
    integer :: num_elves = 0
    integer :: num_goblins = 0
    integer :: num_elves_alive = 0
    integer :: num_goblins_alive = 0

    !-- Elf/Goblins
    type(Unit), target :: elves(MAX_UNITS_PER_TEAM)
    type(Unit), target :: goblins(MAX_UNITS_PER_TEAM)

contains

    subroutine write_unit_state
        implicit none
        integer :: i
        character(len=*),parameter :: fmt = '(i4,3i6,1l6)'

        !-- Elves
        write (syslog%unit,'(a,i0,a,i0,a)') 'Elves (',num_elves_alive,'/',num_elves,')'
        write (syslog%unit,'(a)') '  ID     X     Y    HP   Dead'
        do i = 1, num_elves
            write (syslog%unit,fmt=fmt) i,elves(i)%x,elves(i)%y,elves(i)%hp,elves(i)%dead
        end do
        write (syslog%unit,'(a,i0)') 'Total HP Remaining: ', sum(elves(:) % hp)

        !-- Goblins
        write (syslog%unit,'(a,i0,a,i0,a)') 'Goblins (',num_goblins_alive,'/',num_goblins,')'
        write (syslog%unit,'(a)') '  ID     X     Y    HP   Dead'
        do i = 1, num_goblins
            write (syslog%unit,fmt=fmt) i,goblins(i)%x,goblins(i)%y,goblins(i)%hp,goblins(i)%dead
        end do
        write (syslog%unit,'(a,i0)') 'Total HP Remaining: ', sum(goblins(:) % hp)

    end subroutine

end module