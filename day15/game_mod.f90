module game_mod

    !-- Time step
    integer :: round = 0

    !-- Number of targets for current unit
    integer :: num_targets = 0

    !-- Max number of target positions is 4 times the maximum number of target units (up/down/left/right)*targets
    integer :: max_targets = 0

    !-- Minimum distance
    integer :: min_dist = HUGE(round)

    !-- Number of tiles that have a tied distance
    integer :: num_tie_dist

    !-- Minimum HP
    integer :: min_hp

    !-- x,y coordinates of unit's new map location
    integer :: new_x
    integer :: new_y

    !-- Whether or not a unit is attacking this round
    logical :: attacking = .false.

    !-- Index of finally selected target
    integer :: selected_target_index

end module