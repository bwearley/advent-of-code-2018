!-----------------------------------------------------------------------
! Fortran Command Line Argument Parser (f-clap)
!-----------------------------------------------------------------------
!
! MODULE:
! fclap_settings_mod
!
! AUTHOR:
! Brian Earley
!
! DESCRIPTION:
!> Defines F-CLAP settings
!-----------------------------------------------------------------------

module fclap_settings_mod
    implicit none

    !> Version string
    character(len=*),parameter :: version_string = 'version 1.0'

    !> Default value when default_value is not specified
    !  Consider values such as '0', '1', etc. as appropriate.
    character(len=*),parameter :: unspecified_default_value = ''
    
    !> Minimum number of arguments that must be passed.
    integer,parameter :: min_args_passed = 1
    
    !> Maximum number of Argument_Group types to allocate.
    integer,parameter :: max_num_groups = 2

    !> Maximum number of Argument types to allocate.
    integer,parameter :: max_num_arguments = 2

    !> Maximum length of a command argument.
    integer,parameter :: mxclen = 256

    !> Controls whether unknown arguments fail and stop or fall through silently.
    logical,parameter :: fail_on_unknown_argument = .true.

end module