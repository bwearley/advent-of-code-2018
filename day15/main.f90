program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use map_mod
    implicit none

    !-- Counters
    integer :: i, j, x, y

    integer :: xtmp,ytmp

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    ! Input file reading properties
    integer,parameter            :: max_line_len = 150
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    character(len=10) :: elf_attack_power_override
    type(Argument) :: elf_attack_power_override_arg

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)
    y_dim = num_lines

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    
    !-- Start timer
    call syslog % start_timer

    elf_attack_power_override_arg = get_argument_named('elf-attack')
    if (elf_attack_power_override_arg % is_configured()) then
        write (syslog%unit,*) 'Overriding elf attack power'
        write (syslog%unit,'(a,i0)') 'Old power: ', ELF_ATTACK_POWER
        elf_attack_power_override = get_value_for_arg('elf-attack')
        read(elf_attack_power_override, *) ELF_ATTACK_POWER
        write (syslog%unit,'(a,i0)') 'New power: ', ELF_ATTACK_POWER
    end if

    !-- Read map setup
    x = 1
    y = 1
    do i = 1, num_lines

        ! Read line
        read (input_unit,'(a)') line

        ! Grid x-dimension is obtained from the length
        ! of first row, then we can allocate the grid
        if (i == 1) then
            x_dim = len(trim(line))
            write (syslog%unit,'(a,i0,a,i0,a)') 'Building map of (',x_dim,'x',y_dim,')'
            allocate(   map(x_dim,y_dim))
            allocate(dijmap(x_dim,y_dim))
        end if

        ! Build grid
        x = 1
        do j = 1, x_dim

            map(x,y) % x = x
            map(x,y) % y = y
            map(x,y) % a = line(j:j)

            ! Define map
            select case (line(j:j))
                case('#');         map(x,y) % MapSquareType = MapSquareTypeWall
                case('.','E','G'); map(x,y) % MapSquareType = MapSquareTypeOpen
                case default
                    write (syslog%unit,'(a,i0,a,i0)') 'Error reading map at ',x,',',y
                    error stop
            end select

            ! Define units (if applicable)
            select case (line(j:j))
                case('E')
                    num_elves = num_elves + 1
                    num_elves_alive = num_elves_alive + 1
                    elves(num_elves) % id = num_elves
                    elves(num_elves) % unit_type = UnitTypeElf
                    elves(num_elves) % attack_power = ELF_ATTACK_POWER
                    elves(num_elves) % hp = ELF_HIT_POINTS
                    elves(num_elves) % x = x
                    elves(num_elves) % y = y
                    map(x,y) % unit => elves(num_elves)
                case ('G')
                    num_goblins = num_goblins + 1
                    num_goblins_alive = num_goblins_alive + 1
                    goblins(num_goblins) % id = num_goblins
                    goblins(num_goblins) % unit_type = UnitTypeGoblin
                    goblins(num_goblins) % attack_power = GOBLIN_ATTACK_POWER
                    goblins(num_goblins) % hp = GOBLIN_HIT_POINTS
                    goblins(num_goblins) % x = x
                    goblins(num_goblins) % y = y
                    map(x,y) % unit => goblins(num_goblins)
                case default
                    continue
            end select

            ! Error checking
            if (num_elves > MAX_UNITS_PER_TEAM .or. &
                 num_goblins > MAX_UNITS_PER_TEAM) then
                write (syslog%unit,*) 'ERROR: Exceeded maximum number of units.'
                call bomb
            end if
            x = x + 1
        end do
        y = y + 1
    end do
    close (input_unit)

    !-- Write to log
    call syslog%log(__FILE__, &
        'Found '//string(num_elves)// ' elves and '//string(num_goblins)//' goblins.')
    call write_state

    !-- Max number of target positions is 4 times the maximum number of target units (up/down/left/right)*targets
    max_targets = max(num_elves,num_goblins)*4
    allocate(target_positions(max_targets))

    !-- Simulation
    flush(syslog%unit)
    round = 0
    ROUND_LOOP: do
        write (syslog%unit,'(*(g0))') 'Last completed round ', round
        write (syslog%unit,'(*(g0))') 'Beginning round ', round+1
        Y_LOOP: do y = 1, y_dim
            X_LOOP: do x = 1, x_dim

                ! Wall
                if (map(x,y) % MapSquareType == MapSquareTypeWall) cycle X_LOOP

                ! No unit here
                if (.not. associated(map(x,y) % unit)) cycle X_LOOP

                ! Already processed this unit this round
                if (map(x,y) % unit % touched_last_round == round) cycle X_LOOP
                associate (this_unit => map(x,y) % unit)
                
                ! Update unit
                this_unit % touched_last_round = round

                write (syslog%unit,'(3a,i0,a,i0,a,i0,a)') &
                    'Processing ',map(x,y) % a, ' (',x,',',y,') (round ', round,')'

                ! Game over
                if (num_elves_alive == 0 .or. num_goblins_alive == 0) then
                    write (syslog%unit,'(*(g0))') 'Game ending at round ',round
                    exit ROUND_LOOP
                end if

                ! If already in range, attack target
                if (currently_in_range(x,y)) then
                    call process_attack_at_coordinate(x,y)

                ! Else: Identify open squares in range of (adjacent) targets, move
                else

                    ! Reset
                    target_positions(:) % x = 0
                    target_positions(:) % y = 0
                    target_positions(:) % dist = HUGE(i)

                    ! Elf: Find goblin targets
                    if (this_unit % unit_type == UnitTypeElf) then
                        num_targets = 0
                        GOBLIN_SRCH: do i = 1, num_goblins
                            if (goblins(i) % dead) cycle GOBLIN_SRCH
                            these_targets(:) = points_in_range_of_point(goblins(i)%x,goblins(i)%y)
                            do j = 1, 4
                                if (these_targets(j) % x == 0 .and. these_targets(j) % y == 0) cycle
                                num_targets = num_targets + 1
                                target_positions(num_targets) % x = these_targets(j) % x
                                target_positions(num_targets) % y = these_targets(j) % y
                            end do
                        end do GOBLIN_SRCH
                    ! Goblin: Find elf targets
                    else if (this_unit % unit_type == UnitTypeGoblin) then
                        num_targets = 0
                        ELF_SRCH: do i = 1, num_elves
                            if (elves(i) % dead) cycle ELF_SRCH
                            these_targets(:) = points_in_range_of_point(elves(i)%x,elves(i)%y)
                            do j = 1, 4
                                if (these_targets(j) % x == 0 .and. these_targets(j) % y == 0) cycle
                                num_targets = num_targets + 1
                                target_positions(num_targets) % x = these_targets(j) % x
                                target_positions(num_targets) % y = these_targets(j) % y
                            end do
                        end do ELF_SRCH
                    else
                        write (syslog%unit,'(a,i0,a,i0)') 'ERROR: Unknown unit type at ',x,',',y
                        stop
                    end if
                    
                    ! If [...] there are no open squares which are in range of a target, the unit ends its turn
                    if (num_targets == 0) then
                        if (this_unit % unit_type == UnitTypeElf) then
                            write (syslog%unit,'(a,i0,a)') 'Elf ',this_unit % id,' had no targets, ending turn'
                        else if (this_unit % unit_type == UnitTypeGoblin) then
                            write (syslog%unit,'(a,i0,a)') 'Goblin ',this_unit % id,' had no targets, ending turn'
                        end if
                        cycle X_LOOP
                    
                    ! Need to figure out where to move
                    else

                        ! Determine which squares can be reached in the fewest steps
                        min_dist = HUGE(j)
                        do j = 1, num_targets
                            target_positions(j) % dist =         &
                                minimum_distance_point_to_point( &
                                    x,y,                         &
                                    target_positions(j) % x,     &
                                    target_positions(j) % y      &
                                )
                        end do
                        min_dist = minval(target_positions(:) % dist)
                        write (syslog%unit,'(a,i0)') 'Minimum distance: ', min_dist

                        write (syslog%unit,'(3a,i0,a,i0,a)') &
                            'Initial target destinations for ',map(x,y) % a, ' (',x,',',y,')'
                        do j = 1, num_targets
                            write (syslog%unit,'(i3,3i12)') j,target_positions(j) % x, target_positions(j) % y, target_positions(j) % dist
                        end do 

                        ! If unit cannot find an open path to any target_positions, end turn
                        if (min_dist == HUGE(i)) cycle X_LOOP

                        ! Find out if there were ties between closest square
                        num_tie_dist = 0
                        do j = 1, num_targets
                            if (target_positions(j) % dist == min_dist) num_tie_dist = num_tie_dist + 1
                            if (target_positions(j) % dist == min_dist) selected_target_index = j
                        end do
                        write (syslog%unit,'(4(a,i0))') 'Number of targets tied for distance ',min_dist,': ',num_tie_dist

                        ! Tie for target distances: pick a target
                        if (num_tie_dist > 1) then
                            
                            ! Delete non-minimum targets
                            do j = 1, num_targets
                                if (target_positions(j) % dist > min_dist) then
                                    target_positions(j) % x = 0
                                    target_positions(j) % y = 0
                                    target_positions(j) % dist = HUGE(i)
                                end if
                            end do

                            write (syslog%unit,'(3a,i0,a,i0,a)') &
                                'Target destinations for minimum distance for ',map(x,y) % a, ' (',x,',',y,')'
                            do j = 1, num_targets
                               write (syslog%unit,'(i3,3i12)') j,target_positions(j) % x, target_positions(j) % y, target_positions(j) % dist
                            end do 

                            ! If multiple squares are tied for being reachable in the fewest steps,
                            ! the first in reading order is chosen
                            Y_TMP: do ytmp = 1, y_dim
                                do xtmp = 1, x_dim
                                    do j = 1, num_targets
                                        if (target_positions(j) % x == xtmp .and. target_positions(j) % y == ytmp) then
                                            selected_target_index = j
                                            write (syslog%unit,'(4(a,i0))') &
                                                'Selected j=',j,' dist=',target_positions(j) % dist,' (x,y)=',target_positions(j) % x,',',target_positions(j) % y
                                            exit Y_TMP
                                        end if
                                    end do
                                end do
                            end do Y_TMP
                        ! Only one target has shortest distance: target picked
                        else if (num_tie_dist == 1) then
                            continue
                        else
                            write (syslog%unit,'(a,i0)') 'ERROR: Unknown failure with targeting. num_tie_dist =',num_tie_dist
                            stop
                        end if

                        ! Unit takes a single step towards the chosen square along the shortest path to
                        ! that square. If multiple steps would put the unit equally closer to its
                        ! destination, the unit chooses the step which is first in reading order.
                        ! This requires knowing when there is more than one shortest path so that you
                        ! can consider the first step of each such path.
                        ! Order:
                        ! (x,y-1)
                        ! (x-1,y)
                        ! (x+1,y)
                        ! (x,y+1)
                        ! Check which of the surrounding points decrease distance to target
                        self_surrounding_distances(POINT_ABOVE) =            &
                            minimum_distance_point_to_point(                 &
                                x,y-1,                                       &
                                target_positions(selected_target_index) % x, &
                                target_positions(selected_target_index) % y  &
                            )
                        self_surrounding_distances(POINT_LEFT) =             &
                            minimum_distance_point_to_point(                 &
                                x-1,y,                                       &
                                target_positions(selected_target_index) % x, &
                                target_positions(selected_target_index) % y  &
                            )
                        self_surrounding_distances(POINT_RIGHT) =            &
                            minimum_distance_point_to_point(                 &
                                x+1,y,                                       &
                                target_positions(selected_target_index) % x, &
                                target_positions(selected_target_index) % y  &
                            )
                        self_surrounding_distances(POINT_BELOW) =            &
                            minimum_distance_point_to_point(                 &
                                x,y+1,                                       &
                                target_positions(selected_target_index) % x, &
                                target_positions(selected_target_index) % y  &
                            )

                        if      (self_surrounding_distances(POINT_ABOVE) == target_positions(selected_target_index) % dist-1 &
                                 .and. map(x,y-1) % is_open()) then
                            new_x = x
                            new_y = y-1
                        else if (self_surrounding_distances(POINT_LEFT)  == target_positions(selected_target_index) % dist-1 &
                                 .and. map(x-1,y) % is_open()) then
                            new_x = x-1
                            new_y = y
                        else if (self_surrounding_distances(POINT_RIGHT) == target_positions(selected_target_index) % dist-1 &
                                 .and. map(x+1,y) % is_open()) then
                            new_x = x+1
                            new_y = y
                        else if (self_surrounding_distances(POINT_BELOW) == target_positions(selected_target_index) % dist-1 &
                                 .and. map(x,y+1) % is_open()) then
                            new_x = x
                            new_y = y+1
                        else
                            write (syslog%unit,'(a)') 'ERROR: Unknown failure with targeting' 
                            stop
                        end if
                        
                        ! Verify new location
                        if (.not. map(new_x,new_y) % is_open()) then
                            write (syslog%unit,'(a)') 'ERROR: Picked invalid path'
                            write (syslog%unit,'(4(a,i0),a)') 'Tried to move from (',x,',',y,') to (',new_x,',',new_y,')'
                            stop
                        end if

                        write (syslog%unit,'(3a,i0,a,i0,a,i0,a,i0,a)') &
                            'Moved ',map(x,y)%a,' from (',x,',',y,') to (',new_x,',',new_y,')'
                        
                        ! Update unit position
                        this_unit % x = new_x
                        this_unit % y = new_y

                        ! Point new map square back at this unit
                        if (associated(map(new_x,new_y) % unit)) then
                            write (syslog%unit,'(a)') 'ERROR: Trying to path into a square already occupied'
                            stop
                        end if
                        map(new_x,new_y) % unit => this_unit
                        if (this_unit % unit_type == UnitTypeElf) then
                            map(new_x,new_y) % a = 'E'
                        else if (this_unit % unit_type == UnitTypeGoblin) then
                            map(new_x,new_y) % a = 'G'
                        end if
                        
                        ! Stop this square from pointing at this unit
                        map(x,y) % unit => null()

                        ! Clear the text from the square on the map
                        map(x,y) % a = '.'

                        ! If unit just moved into range, try attacking again
                        if (currently_in_range(new_x,new_y)) then
                            call process_attack_at_coordinate(new_x,new_y)
                        end if

                    end if
                end if
    
                end associate

            end do X_LOOP
        end do Y_LOOP
        round = round + 1
        write (syslog%unit,'(*(g0))') 'Completed round ', round
        call write_state
        call write_unit_state
    end do ROUND_LOOP

    write (syslog%unit,'(a,i0,a)') 'Number of remaining units after ', round, ' rounds'
    write (syslog%unit,*) 'Elves: ', num_elves_alive
    write (syslog%unit,*) 'Goblins: ', num_goblins_alive
    if (num_elves_alive > num_goblins_alive) write (syslog%unit,*) 'Elves win!' 
    if (num_goblins_alive > num_elves_alive) write (syslog%unit,*) 'Goblins win!'

    call write_unit_state

    !-- Part 1
    write (          *,'(a,i0)') 'Part 1: ',round * sum(goblins(:) % hp) ! 208960
    write (syslog%unit,'(a,i0)') 'Part 1: ',round * sum(goblins(:) % hp)

    !-- Part 2
    write (          *,'(a,i0)') 'Part 2: ',round * sum(elves(:) % hp) ! 49863
    write (syslog%unit,'(a,i0)') 'Part 2: ',round * sum(elves(:) % hp)

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

contains

    logical function enemy_is_adjacent_to_coordinate(x,y,xa,ya) result(enemy)
        integer,intent(in) :: x, xa
        integer,intent(in) :: y, ya
        type(Unit),pointer :: this_unit => null()

        this_unit => map(x,y) % unit

        enemy = .false.

        if (associated(map(xa,ya) % unit)) then
            if (map(xa,ya) % unit % unit_type /= this_unit % unit_type) then
                enemy = .true.
                return
            end if
        end if

    end function

    logical function currently_in_range(x,y) result(in_range)
        integer,intent(in) :: x
        integer,intent(in) :: y
        integer :: otherx, othery
        type(Unit),pointer :: this_unit => null()

        this_unit => map(x,y) % unit

        in_range = .false.

        otherx = x
        othery = y-1
        if (enemy_is_adjacent_to_coordinate(x,y,otherx,othery)) then
            in_range = .true.
            return
        end if

        otherx = x-1
        othery = y
        if (enemy_is_adjacent_to_coordinate(x,y,otherx,othery)) then
            in_range = .true.
            return
        end if

        otherx = x+1
        othery = y
        if (enemy_is_adjacent_to_coordinate(x,y,otherx,othery)) then
            in_range = .true.
            return
        end if

        otherx = x
        othery = y+1
        if (enemy_is_adjacent_to_coordinate(x,y,otherx,othery)) then
            in_range = .true.
            return
        end if

    end function

    subroutine process_attack_at_coordinate(x,y)
        integer,intent(in) :: x
        integer,intent(in) :: y
        logical :: attacking
        type(Unit),pointer :: this_unit => null()

        this_unit => map(x,y) % unit

        ! Reset
        attacking = .false.
        target_positions(:) % x = 0
        target_positions(:) % y = 0
        target_positions(:) % hp = HUGE(i)
        j = 0

        ! Check if there are adjacent units
        num_targets = 0
        if (this_unit % unit_type == UnitTypeElf) then
            
            ! Check if there is an adjacent Goblin to attack
            if (associated(map(x,y-1) % unit)) then
                if (map(x,y-1) % unit % unit_type == UnitTypeGoblin) then
                    j = j + 1
                    target_positions(j) % x = x
                    target_positions(j) % y = y-1
                    target_positions(j) % hp = map(x,y-1) % unit % hp
                    attacking = .true.
                end if
            end if
            if (associated(map(x-1,y) % unit)) then
                if (map(x-1,y) % unit % unit_type == UnitTypeGoblin) then
                    j = j + 1
                    target_positions(j) % x = x-1
                    target_positions(j) % y = y
                    target_positions(j) % hp = map(x-1,y) % unit % hp
                    attacking = .true.
                end if
            end if
            if (associated(map(x+1,y) % unit)) then
                if (map(x+1,y) % unit % unit_type == UnitTypeGoblin) then
                    j = j + 1
                    target_positions(j) % x = x+1
                    target_positions(j) % y = y
                    target_positions(j) % hp = map(x+1,y) % unit % hp
                    attacking = .true.
                end if
            end if
            if (associated(map(x,y+1) % unit)) then
                if (map(x,y+1) % unit % unit_type == UnitTypeGoblin) then
                    j = j + 1
                    target_positions(j) % x = x
                    target_positions(j) % y = y+1
                    target_positions(j) % hp = map(x,y+1) % unit % hp
                    attacking = .true.
                end if
            end if
        else if (this_unit % unit_type == UnitTypeGoblin) then
            
            ! Check if there is an adjacent Elf to attack
            if (associated(map(x,y-1) % unit)) then
                if (map(x,y-1) % unit % unit_type == UnitTypeElf) then
                    j = j + 1
                    target_positions(j) % x = x
                    target_positions(j) % y = y-1
                    target_positions(j) % hp = map(x,y-1) % unit % hp
                    attacking = .true.
                end if
            end if
            if (associated(map(x-1,y) % unit)) then
                if (map(x-1,y) % unit % unit_type == UnitTypeElf) then
                    j = j + 1
                    target_positions(j) % x = x-1
                    target_positions(j) % y = y
                    target_positions(j) % hp = map(x-1,y) % unit % hp
                    attacking = .true.
                end if
            end if
            if (associated(map(x+1,y) % unit)) then
                if (map(x+1,y) % unit % unit_type == UnitTypeElf) then
                    j = j + 1
                    target_positions(j) % x = x+1
                    target_positions(j) % y = y
                    target_positions(j) % hp = map(x+1,y) % unit % hp
                    attacking = .true.
                end if
            end if
            if (associated(map(x,y+1) % unit)) then
                if (map(x,y+1) % unit % unit_type == UnitTypeElf) then
                    j = j + 1
                    target_positions(j) % x = x
                    target_positions(j) % y = y+1
                    target_positions(j) % hp = map(x,y+1) % unit % hp
                    attacking = .true.
                end if
            end if
        else
            write (syslog%unit,'(a,i0,a,i0)') 'ERROR: Unknown unit type at ',x,',',y
            stop
        end if

        num_targets = j
        if (attacking) then
            continue

            ! Attack adjacent target with fewest HP
            min_hp = minval(target_positions(:) % hp)

            ! if tied, attack first in reading order
            ATTCK_SRCH: do j = 1, num_targets
                if (target_positions(j) % hp == min_hp) then
                    selected_target_index = j
                    exit ATTCK_SRCH
                end if
            end do ATTCK_SRCH

            ! Attack
            associate (                                        &
                victim => map(                                 &
                    target_positions(selected_target_index)%x, &
                    target_positions(selected_target_index)%y  &
                ) % unit                                       &
            )

                victim % hp = max(victim % hp - this_unit % attack_power,0)

                ! If target dies, clear out its square
                if (victim % hp == 0) then
                    
                    victim % dead = .true.

                    victim % touched_last_round = round

                    if (victim % unit_type == UnitTypeElf) then
                        num_elves_alive = num_elves_alive - 1
                    else
                        num_goblins_alive = num_goblins_alive - 1
                    end if

                    ! Clear map visually
                    map( &
                        target_positions(selected_target_index)%x, &
                        target_positions(selected_target_index)%y) % a = '.'
                    
                    ! Nullify map target
                    map( &
                        target_positions(selected_target_index)%x, &
                        target_positions(selected_target_index)%y) % unit => null()
                end if

            end associate
        end if

    end subroutine

end program