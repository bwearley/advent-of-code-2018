module point_mod
    use syslog_mod
    implicit none

    integer,private :: i

    type :: Point
        integer :: id   = 0         ! Points(:) - id of self, Canvas(:,:) - id of closest point, or satisfactory distance flag
        integer :: x    = 0         ! x location
        integer :: y    = 0         ! y location
        integer :: area = 0         ! Points(:) - surrounding area
        integer :: dist = HUGE(i)   ! distance to nearest point
    end type

    ! Points (field & defined points)
    type(Point),allocatable :: points(:)    ! Array of points from input
    type(Point),allocatable :: canvas(:,:)  ! Array of all points in canvas

    !-- Canvas extents
    integer :: xmin = 0
    integer :: xmax = 0
    integer :: ymin = 0
    integer :: ymax = 0

contains

    subroutine draw_canvas
        implicit none
        integer :: x, y

        ! Write blank
        write (syslog%unit,'(3X)',advance='no')
        
        ! Write x-headers
        do x = xmin, xmax
            write (syslog%unit,'(i3)',advance='no') x
        end do
        write (syslog%unit,*) ! advance
        
        ! Write field
        do y = ymin, ymax
            ! Write y-column
            write (syslog%unit,'(i3)',advance='no') y
            do x = xmin, xmax
                write (syslog%unit,'(i3)',advance='no') canvas(x,y) % id
            end do
            write (syslog%unit,*) ! advance
        end do
    end subroutine
end module