program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use point_mod
    implicit none

    !-- Counters
    integer :: i
    integer :: x, y

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Distances
    integer,parameter :: MAX_MANHATTAN_DISTANCE = 10000
    integer :: dist = 0
    integer :: num_occurrences = 0
    integer :: area_safe = 0
    integer,allocatable :: distances(:)

    !-- Control for whether to write the canvas to log
    logical,parameter :: write_canvas = .true.

    ! Input file reading properties
    !integer,parameter            :: max_line_len = 100
    !character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(points(num_lines))
    allocate(distances(num_lines))

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    write (syslog%unit,*) 'Raw Points:'
    write (syslog%unit,*) '  ID   X   Y'
    do i = 1, num_lines
        
        !-- Read
        read (input_unit,*) points(i) % x, points(i) % y
        points(i) % id = i

        !-- Write to log
        write (syslog%unit,'(3i5)') &
            points(i) % id,         &
            points(i) % x,          &
            points(i) % y

    end do
    close (input_unit)

    !-- Start timer
    call syslog % start_timer

    !-- Build canvas
    xmin = minval(points(:) % x)
    xmax = maxval(points(:) % x)
    ymin = minval(points(:) % y)
    ymax = maxval(points(:) % y)
    write (syslog%unit,'(4(a,i0),a)') 'Building grid of (',xmin,'-',xmax,',',ymin,'-',ymax,')'
    allocate(canvas(xmin:xmax,ymin:ymax))

    !-- Put existing points on canvas
    do i = 1, num_lines
        canvas(points(i) % x, points(i) % y) % id = points(i) % id
    end do

    !-- Draw initial canvas
    if (write_canvas) then
        write (syslog%unit,*) 'Point Field:'
        call draw_canvas
    end if

    !-- Calculate distances between all points on canvas and defined points
    X_LOOP: do x = xmin, xmax
        Y_LOOP: do y = ymin, ymax
            
            ! Calculate distances to all defined points
            distances(:) = abs(x - points(:) % x) + abs(y - points(:) % y)

            ! Minimum distance
            dist = minval(distances)
            num_occurrences = 0

            ! Ensure there was only one occurrence of minimum distance
            do i = 1, num_lines
                if (distances(i) == dist) num_occurrences = num_occurrences + 1
            end do

            ! Point is equally far from 2 or more points: invalidate
            if (num_occurrences > 1) then
                canvas(x,y) % dist = -1
                canvas(x,y) % id   = -1

            ! Closest point
            else
                canvas(x,y) % dist = dist
                canvas(x,y) % id   = minloc(distances(:),1)
            end if
        end do Y_LOOP
    end do X_LOOP

    !-- Illustrate closest points
    if (write_canvas) then
        write (syslog%unit,*) 'Closest points:'
        call draw_canvas
    end if

    !-- Calculate areas around points
    do x = xmin, xmax
        do y = ymin, ymax
            do i = 1, num_lines
                if (canvas(x,y) % id == i) points(i) % area = points(i) % area + 1
            end do
        end do
    end do

    !-- Write areas to log
    write (syslog%unit,*) 'Areas for each point:'
    write (syslog%unit,*) '  ID   Area'
    do i = 1, num_lines
        write (syslog%unit,'(i4,i8)') points(i) % id, points(i) % area
    end do
    write (syslog%unit,'(a,i0)') 'Part 1: largest area = ', maxval(points(:)%area) !4475

    !-- Calculate
    X_LOOP_2: do x = xmin, xmax
        Y_LOOP_2: do y = ymin, ymax
            
            ! Calculate distances to all defined points
            distances(:) = abs(x - points(:) % x) + abs(y - points(:) % y)

            ! Minimum distance
            dist = sum(distances)

            ! Point exceeds maximum distance: invalidate
            if (dist >= MAX_MANHATTAN_DISTANCE) then
                canvas(x,y) % dist = HUGE(i)
                canvas(x,y) % id   = 0

            ! Point is close enough
            else
                canvas(x,y) % dist = dist
                canvas(x,y) % id   = 1 ! now using ID field as a flag
            end if
        end do Y_LOOP_2
    end do X_LOOP_2

    !-- Illustrate safe area
    if (write_canvas) then
        write (syslog%unit,*) 'Safe & close area:'
        call draw_canvas
    end if
    
    !-- Calculate area of safe region
    area_safe = sum(canvas(:,:) % id)
    write (syslog%unit,'(a,i0)') 'Part 2: safe & close area = ',area_safe !35237
    
    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program