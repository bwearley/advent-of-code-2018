module group_mod
    use syslog_mod
    use string_tools_mod
    implicit none

    integer,parameter :: MAX_GROUPS_PER_TEAM = 10
    integer,parameter :: MAX_WEAKNESSES = 2
    integer,parameter :: MAX_IMMUNITIES = 2

    ! Debug
    logical :: dbg = .true.

    ! Damage Types
    enum,bind(C)
        enumerator :: DamageTypeNone = 0
        enumerator :: Radiation
        enumerator :: Bludgeoning
        enumerator :: Fire
        enumerator :: Slashing
        enumerator :: Cold
    end enum

    ! Teams
    enum,bind(C)
        enumerator :: TeamNone = 0
        enumerator :: TeamImmune
        enumerator :: TeamInfection
    end enum

    type :: Group
        integer :: ix = 0
        integer :: group_num = 0
        integer :: num_units = 0
        integer :: hp = 0
        integer :: atk = 0
        integer :: initiative = 0
        integer :: weakness(MAX_WEAKNESSES)
        integer :: num_weakness = 0
        integer :: immune(MAX_IMMUNITIES)
        integer :: num_immune = 0
        integer :: dmg_type = 0
        integer :: team = TeamNone
        type(Group),pointer :: target => null()
        type(group),pointer :: targeted_by => null()
    contains
        procedure :: eff_power
        procedure :: print_info_verbose
        procedure :: damage_to_group
        procedure :: deal_damage_to_target
    end type
    interface Group
        module procedure init_group_from_string
    end interface

    !-- Counters
    integer :: num_infection_groups = 0
    integer :: num_immune_groups = 0
    integer :: num_all_groups = 0
    integer :: num_immune_units = 0
    integer :: num_infection_units = 0

    type(Group),target :: groups(MAX_GROUPS_PER_TEAM+MAX_GROUPS_PER_TEAM)
    integer :: target_selection_order(MAX_GROUPS_PER_TEAM+MAX_GROUPS_PER_TEAM) = 0
    integer :: attack_order(MAX_GROUPS_PER_TEAM+MAX_GROUPS_PER_TEAM) = 0

contains

    type(Group) function init_group_from_string(str,team) result(g)
        character(len=*) :: str
        integer,parameter :: NUM_STRING_PARTS = 50
        integer,parameter :: LEN_STR_PART = 50
        character(len=LEN_STR_PART) :: str_parts(NUM_STRING_PARTS)
        integer,intent(in) :: team
        integer :: pos
        integer :: i, j
        integer :: part_num
        logical :: reading_weaknesses
        logical :: reading_immunities

        ! Initialize
        g % team = team
        reading_weaknesses = .false.
        reading_immunities = .false.
        str_parts(:) = ' '
        pos = 1
        part_num = 1

        ! Split string on spaces
        READ_PARTS: do i = 1, len(trim(str))+1
            if (str(i:i) == ' ') then
                read (str(pos:i),*) str_parts(part_num)
                part_num = part_num + 1
                pos = i
            end if
        end do READ_PARTS

        ! Remove punctuation
        STRIP_PUNCTUATION: do i = 1, NUM_STRING_PARTS
            do j = 1, LEN_STR_PART
                if (str_parts(i)(j:j) == ';') str_parts(i)(j:j) = ' '
                if (str_parts(i)(j:j) == ')') str_parts(i)(j:j) = ' '
                if (str_parts(i)(j:j) == '(') str_parts(i)(j:j) = ' '
                if (str_parts(i)(j:j) == ',') str_parts(i)(j:j) = ' '
            end do
            str_parts(i) = adjustl(str_parts(i))
        end do STRIP_PUNCTUATION

        ! Diagnostic Output
        if (dbg) then
            write (syslog%unit,'(a)') str
            do i = 1, NUM_STRING_PARTS
              if (str_parts(i) /= ' ')  write (syslog%unit,'(i5,3x,a)') i,str_parts(i)
            end do
        end if

        ! Read values into struct
        read(str_parts(1),*) g % num_units
        read(str_parts(5),*) g % hp

        ! Read immunities and weaknesses
        READ_IMMUNITIES_WEAKNESSES: do i = 7, NUM_STRING_PARTS

            pos = i

            if (str_parts(i) == 'immune') then
                reading_immunities = .true.
                reading_weaknesses = .false.

            else if (str_parts(i) == 'weak') then
                reading_immunities = .false.
                reading_weaknesses = .true.

            else if (str_parts(i) == 'with') then
                reading_immunities = .false.
                reading_weaknesses = .false.
                exit READ_IMMUNITIES_WEAKNESSES

            else
                if (reading_immunities) then
                    select case (str_parts(i))
                        case ('radiation')
                            g % num_immune = g % num_immune + 1
                            g % immune(g % num_immune) = Radiation
                            
                        case ('bludgeoning')
                            g % num_immune = g % num_immune + 1
                            g % immune(g % num_immune) = Bludgeoning
                            
                        case ('fire')
                            g % num_immune = g % num_immune + 1
                            g % immune(g % num_immune) = Fire
                            
                        case ('slashing')
                            g % num_immune = g % num_immune + 1
                            g % immune(g % num_immune) = Slashing
                            
                        case ('cold')
                            g % num_immune = g % num_immune + 1
                            g % immune(g % num_immune) = Cold
                            
                        case default
                            continue
                    end select

                else if (reading_weaknesses) then
                    select case (str_parts(i))
                        case ('radiation')
                            g % num_weakness = g % num_weakness + 1
                            g % weakness(g % num_weakness) = Radiation
                            
                        case ('bludgeoning')
                            g % num_weakness = g % num_weakness + 1
                            g % weakness(g % num_weakness) = Bludgeoning
                            
                        case ('fire')
                            g % num_weakness = g % num_weakness + 1
                            g % weakness(g % num_weakness) = Fire
                            
                        case ('slashing')
                            g % num_weakness = g % num_weakness + 1
                            g % weakness(g % num_weakness) = Slashing
                            
                        case ('cold')
                            g % num_weakness = g % num_weakness + 1
                            g % weakness(g % num_weakness) = Cold
                            
                        case default
                            continue
                    end select
                
                else
                    continue
                end if
            end if
        end do READ_IMMUNITIES_WEAKNESSES

        READ_REST: do i = pos, NUM_STRING_PARTS
            if (str_parts(i) == 'does') then
                
                ! Attack damage
                read (str_parts(i+1),*) g % atk

                ! Attack damage type
                select case (str_parts(i+2))
                    case ('radiation')
                        g % dmg_type = Radiation
                    case ('bludgeoning')
                        g % dmg_type = Bludgeoning
                    case ('fire')
                        g % dmg_type = Fire
                    case ('slashing')
                        g % dmg_type = Slashing
                    case ('cold')
                        g % dmg_type = Cold
                    case default
                        continue
                end select

                ! Initiative
                read (str_parts(i+6),*) g % initiative

                exit READ_REST
            end if
        end do READ_REST

    end function

    !> Prints information for each group (roughly approximates input file format)
    subroutine print_info_verbose(self)
        class(Group) :: self
        integer :: i

        write (syslog%unit,'(a)',advance='no') team_to_str(self % team)//': '

        write (syslog%unit,'(i0,a,i0,a)',advance='no') &
            self % num_units, ' units each with ', self % hp, ' hit points '
            
        do i = 1, self % num_weakness
            write (syslog%unit,'(a,a,a)',advance='no') '(weak to ', type_to_str(self % weakness(i)), ')'
        end do

        do i = 1, self % num_immune
            write (syslog%unit,'(a,a,a)',advance='no') '(immune to ', type_to_str(self % immune(i)), ')'
        end do

        write (syslog%unit,'(a,i0,a,a,a,i0)',advance='no') &
            ' with an attack that does ', self % atk, ' ', type_to_str(self % dmg_type), ' damage at an initiative of ', self % initiative

        write (syslog%unit,*) ! advance

    end subroutine

    !> Number of units in that group multiplied by their attack damage
    integer function eff_power(self)
        class(Group) :: self
        eff_power = self % num_units * self % atk
    end function

    !> Returns damage type enum as as string
    function type_to_str(t)
        integer,intent(in) :: t
        character(len=:),allocatable :: type_to_str
        select case (t)
            case(Radiation)
                type_to_str = 'radiation'
            case(Bludgeoning)
                type_to_str = 'bludgeoning'
            case(Fire)
                type_to_str = 'fire'
            case(Slashing)
                type_to_str = 'slashing'
            case(Cold)
                type_to_str = 'cold'
            case default
                write (syslog%unit,*) 'Error: type_to_str ', t
                stop
        end select
    end function

    !> Returns amount of damage self would do to a given target (denoted by index)
    integer function damage_to_group(self,target_idx) result(dmg)
        class(Group) :: self
        integer,intent(in) :: target_idx
        integer :: i

        dmg = self % eff_power()

        associate (target => groups(target_idx))
            do i = 1, target % num_immune
                if (target % immune(i) == self % dmg_type) dmg = 0
            end do

            do i = 1, target % num_weakness
                if (target % weakness(i) == self % dmg_type) dmg = dmg * 2
            end do

            if (self % team == target % team) dmg = 0 ! no friendly fire

        end associate
    end function

    !> Deals damage to self's target and kills units
    subroutine deal_damage_to_target(self)
        class(Group) :: self
        integer :: i
        integer :: dmg
        integer :: killed_units

        dmg = self % damage_to_group(self % target % ix)

        associate (target => self % target)

            killed_units = min(dmg / target % hp, target % num_units)

            if (dbg) write (syslog%unit,'(a,a,i0,a,i0,a,a,a,i0,a,i0,a,i0,a,i0,a)')          &
                team_to_str(self % team),' group ',self % group_num,' deals ', dmg,  &
                ' damage to group ',team_to_str(target % team), ' group ',           &
                target % group_num, ' (',target%num_units,' units with ',target%hp,  &
                ' hp) killing ',killed_units,' units'

            target % num_units = target % num_units - killed_units

            select case (target % team)
            case (TeamImmune)
                num_immune_units = num_immune_units - killed_units
            case (TeamInfection)
                num_infection_units = num_infection_units - killed_units
            case default
                write (syslog%unit,*) 'Error: deal_damage_to_target '
                stop
            end select

        end associate
    end subroutine

    !> Returns team enum as a string
    function team_to_str(t)
        integer,intent(in) :: t
        character(len=:),allocatable :: team_to_str
        select case (t)
            case(TeamImmune)
                team_to_str = 'Immune'
            case(TeamInfection)
                team_to_str = 'Infection'
            case default
                write (syslog%unit,*) 'Error: team_to_str ', t
                stop
        end select
    end function

    !> Returns number of groups tied for a given power
    integer function ties_for_power(pow) result(ties)
        integer,intent(in) :: pow
        integer :: i

        ties = 0
        do i = 1, num_all_groups
            if (groups(i) % eff_power() == pow) ties = ties + 1
        end do

    end function

    !> Returns number of groups tied for a given initiative
    integer function ties_for_initiative(init) result(ties)
        integer,intent(in) :: init
        integer :: i

        ties = 0
        do i = 1, num_all_groups
            if (groups(i) % initiative == init) ties = ties + 1
        end do

    end function

    !> Returns the next highest power lower than given power and number of ties
    function next_highest_power(pow) result(res)
        integer :: res(3) ! 1=ix, 2=ties, 3=power
        integer,intent(in) :: pow
        integer :: curr_highest
        integer :: i

        curr_highest = -HUGE(i)
        do i = 1, num_all_groups
            if (groups(i) % eff_power() > curr_highest .and. groups(i) % eff_power() < pow) then
                curr_highest = groups(i) % eff_power()
                res(1) = i
                res(3) = curr_highest
            end if
        end do
        res(2) = ties_for_power(res(3))

    end function

    !> Returns the next highest initiative lower than provided init for given power
    function next_highest_initiative_for_power(pow,init) result(res)
        integer :: res(3) ! 1=ix, 2=ties, 3=initiative
        integer,intent(in) :: pow
        integer,intent(in) :: init
        integer :: curr_highest
        integer :: i

        curr_highest = -HUGE(i)
        do i = 1, num_all_groups
            if (groups(i) % eff_power() == pow) then
                if (groups(i) % initiative > curr_highest .and. groups(i) % initiative < init) then
                    curr_highest = groups(i) % initiative
                    res(1) = i
                    res(3) = curr_highest
                end if
            end if
        end do
        res(2) = 0

    end function

    !> Returns the next high initiative lower than provided init and number of ties
    function next_highest_initiative(init) result(res)
        integer :: res(3) ! 1=ix, 2=ties, 3=initiative
        integer,intent(in) :: init
        integer :: curr_highest
        integer :: i

        curr_highest = -HUGE(i)
        do i = 1, num_all_groups
            if (groups(i) % initiative > curr_highest .and. groups(i) % initiative < init) then
                curr_highest = groups(i) % initiative
                res(1) = i
                res(3) = curr_highest
            end if
        end do
        res(2) = ties_for_initiative(res(3))

    end function

end module
