program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use group_mod
    implicit none

    !-- Counters
    integer :: i, j

    integer :: iterations

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Puzzle values
    integer :: part1 = 0
    integer :: part2 = 0
    integer :: boost = 0

    !-- Puzzle Processing Variables
    integer :: this_dmg = 0
    integer :: max_dmg = 0
    integer :: this_initiative = 0
    integer :: max_initiative = 0
    integer :: this_eff_power = 0
    integer :: max_eff_power = 0
    integer :: target_ties = 0
    logical :: reading_immune = .false.
    logical :: reading_infection = .false.

    !-- Input file reading properties
    integer,parameter            :: max_line_len = 300
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Start timer
    call syslog % start_timer

    ! Reset for boost
100 continue
    write (syslog%unit,'(*(g0))') ' BOOST: ',boost    
    iterations = 0
    num_infection_groups = 0
    num_immune_groups = 0
    num_all_groups = 0
    
    num_immune_units = 0
    num_infection_units = 0

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    !-- Configure Teams
    do i = 1, num_lines

        ! Read line
        read (input_unit,'(a)') line

        if (line == 'Immune System:') then
            reading_immune = .true.
            reading_infection = .false.
        else if (line == 'Infection:') then
            reading_immune = .false.
            reading_infection = .true.
        else if (line == ' ') then
            continue
        else
            
            if (reading_immune) then
                num_immune_groups = num_immune_groups + 1
                num_all_groups = num_all_groups + 1
                if (num_immune_groups > MAX_GROUPS_PER_TEAM .or. num_all_groups > 2*MAX_GROUPS_PER_TEAM) then
                    call syslog%log(__FILE__,'ERROR: Exceeded maximum allowed number of groups.')
                    stop
                end if
                groups(num_all_groups) = Group(line,team=TeamImmune)
                groups(num_all_groups) % group_num = num_immune_groups
                groups(num_all_groups) % ix = num_all_groups
            
            else if (reading_infection) then
                num_infection_groups = num_infection_groups + 1
                num_all_groups = num_all_groups + 1
                if (num_infection_groups > MAX_GROUPS_PER_TEAM .or. num_all_groups > 2*MAX_GROUPS_PER_TEAM) then
                    call syslog%log(__FILE__,'ERROR: Exceeded maximum allowed number of groups.')
                    stop
                end if
                groups(num_all_groups) = Group(line,team=TeamInfection)
                groups(num_all_groups) % group_num = num_infection_groups
                groups(num_all_groups) % ix = num_all_groups
            end if
        end if
    end do
    close (input_unit)

    BOOST_LOOP: do i = 1, num_all_groups
        if (groups(i) % team == TeamImmune) groups(i) % atk = groups(i) % atk + boost    
    end do BOOST_LOOP

    UNIT_COUNT_LP: do i = 1, num_all_groups
        select case (groups(i) % team)
            case (TeamImmune)
                num_immune_units = num_immune_units + groups(i) % num_units
            case (TeamInfection)
                num_infection_units = num_infection_units + groups(i) % num_units
            case default
                write (syslog%unit,*) 'Error counting units per team.'
                stop
        end select
    end do UNIT_COUNT_LP

    ! Diagnostics
    write (syslog%unit,'(a,i0)') 'Number of immune system groups: ',num_immune_groups
    write (syslog%unit,'(a,i0)') 'Number of infection groups: ',num_infection_groups
    
    if (dbg) then
        do i = 1, num_all_groups
            call groups(i) % print_info_verbose
        end do
    end if

    FIGHT_LOOP: do

        iterations = iterations + 1

        if (iterations > 10000) then
            boost = boost + 1
            goto 100 !goto was just easier :(
        end if

        if (dbg) write (syslog%unit,*) '****************** NEW ROUND ******************'

        !-- Target selection order: In decreasing order of effective power,
        !   groups choose their targets; in a tie, the group with the
        !   higher initiative chooses first.
        TARGET_SELECTION_ORDER_BLK: block
            integer :: next_high(3)
            integer :: next_high_init(3)
            target_selection_order(:) = 0

            next_high(:) = next_highest_power(HUGE(i))
            i = 1
            TARGET_SELECT_ORDER_LP: do
                if (i > num_all_groups) exit TARGET_SELECT_ORDER_LP

                if (next_high(2) == 1) then
                    target_selection_order(i) = next_high(1)
                    i = i + 1
                else if (next_high(2) > 1) then
                   !write (syslog%unit,'(*(g0))') 'Notice: Target selection order tie (ties: ',next_high(2),')'
                    next_high_init(:) = next_highest_initiative_for_power(next_high(3),HUGE(i))
                    do j = 1, next_high(2)
                       !write (syslog%unit,'(*(g0))') 'next_high_init(1): ',next_high_init(1),',i=',i
                        target_selection_order(i) = next_high_init(1)
                        i = i + 1
                        next_high_init(:) = next_highest_initiative_for_power(next_high(3),next_high_init(3))
                    end do
                else
                    write (syslog%unit,*) 'Error: failure in TARGET_SELECTION_ORDER_BLK'
                    stop
                end if
                next_high(:) = next_highest_power(next_high(3))
            end do TARGET_SELECT_ORDER_LP
            
            ! Diagnostic output
            if (dbg) then
                write (syslog%unit,*) ' Target Selection Order:'
                write (syslog%unit,'(a)') '   ID  Ord#               Team  EffPow  Init.   #Units '
                do i = 1, num_all_groups
                    write (syslog%unit,'(2i5,a20,3i8)') i, target_selection_order(i), &
                        team_to_str(groups(target_selection_order(i)) % team), &
                        groups(target_selection_order(i)) % eff_power(), &
                        groups(target_selection_order(i)) % initiative, &
                        groups(target_selection_order(i)) % num_units
                end do
            end if
        end block TARGET_SELECTION_ORDER_BLK

        !-- Target selection
        RESET_LP: do i = 1, num_all_groups
            groups(i) % target      => null()
            groups(i) % targeted_by => null()
        end do RESET_LP
        
        ! If an attacking group is considering two defending groups to
        ! which it would deal equal damage, it chooses to target the
        ! defending group with the largest effective power; if there is
        ! still a tie, it chooses the defending group with the highest
        ! initiative. If it cannot deal any defending groups damage, it
        ! does not choose a target.
        TARGET_SELECT_MAIN: do i = 1, num_all_groups

            associate(curr_attacker => groups(target_selection_order(i)))
                this_dmg = 0
                max_dmg = 0

                ! Seek highest damage target
                TARGET_SELECT_1: do j = 1, num_all_groups
                    if (groups(j) % team == curr_attacker % team) cycle ! friendly fire
                    if (associated(groups(j) % targeted_by)) cycle ! target already selected by another
                    if (groups(j) % num_units == 0) cycle ! target dead

                    this_dmg = curr_attacker % damage_to_group(j)
                    if (this_dmg > max_dmg .and. this_dmg > 0) then
                        max_dmg = this_dmg
                        curr_attacker % target => groups(j)
                    end if
                end do TARGET_SELECT_1

                if (max_dmg == 0) cycle TARGET_SELECT_MAIN

                ! Check for ties in damage
                target_ties = 0
                TARGET_TIE_CHECK: do j = 1, num_all_groups
                    if (groups(j) % team == curr_attacker % team) cycle ! friendly fire
                    if (associated(groups(j) % targeted_by)) cycle ! target already selected by another
                    if (groups(j) % num_units == 0) cycle ! target dead
                    
                    this_dmg = curr_attacker % damage_to_group(j)
                    if (this_dmg == max_dmg) target_ties = target_ties + 1
                end do TARGET_TIE_CHECK

                ! No ties: target found
                if (target_ties == 1) then
                    curr_attacker % target % targeted_by => curr_attacker
                
                else if (target_ties > 1) then
                    ! Tied: seek highest effective power target
                    !write (syslog%unit,'(*(g0))') 'Note: Targeting tie on damage for ', &
                    !    team_to_str(groups(i)%team),' group ',groups(i)%group_num, &
                    !    ' (dmg: ',max_dmg,', ties: ',target_ties,')'
                    this_eff_power = 0
                    max_eff_power = 0
                    TARGET_SELECT_2: do j = 1, num_all_groups
                        if (groups(j) % team == curr_attacker % team) cycle ! friendly fire
                        if (associated(groups(j) % targeted_by)) cycle ! target already selected by another
                        if (groups(j) % num_units == 0) cycle ! target dead

                        this_dmg = curr_attacker % damage_to_group(j)
                        if (this_dmg == max_dmg) then
                            this_eff_power = groups(j) % eff_power()
                            if (this_eff_power > max_eff_power) then
                                max_eff_power = this_eff_power
                                curr_attacker % target => groups(j)
                            end if
                        end if
                    end do TARGET_SELECT_2

                    ! Check for ties in effective power
                    target_ties = 0
                    TARGET_TIE_CHECK_2: do j = 1, num_all_groups
                        if (groups(j) % team == curr_attacker % team) cycle ! friendly fire
                        if (associated(groups(j) % targeted_by)) cycle ! target already selected by another
                        if (groups(j) % num_units == 0) cycle ! target dead
                        
                        this_dmg = curr_attacker % damage_to_group(j)
                        if (this_dmg == max_dmg .and. groups(j) % eff_power() == max_eff_power) then
                            target_ties = target_ties + 1
                        end if
                    end do TARGET_TIE_CHECK_2

                    if (target_ties == 1) then
                        curr_attacker % target % targeted_by => curr_attacker
                    else if (target_ties > 1) then
                        ! Tied again: seek highest initiative target
                        if (dbg) write (syslog%unit,*) 'Alert: Damage and eff. power targeting tie for ',team_to_str(groups(i)%team)
                        this_initiative = 0
                        max_initiative = 0
                        TARGET_SELECT_3: do j = 1, num_all_groups
                            if (groups(j) % team == curr_attacker % team) cycle ! friendly fire
                            if (associated(groups(j) % targeted_by)) cycle ! target already selected by another
                            if (groups(j) % num_units == 0) cycle ! target dead

                            this_dmg = curr_attacker % damage_to_group(j)
                            if (this_dmg == max_dmg) then
                                this_initiative = groups(j) % initiative
                                if (this_initiative > max_initiative) then
                                    max_initiative = this_initiative
                                    curr_attacker % target => groups(j)
                                end if
                            end if
                        end do TARGET_SELECT_3

                        ! Check for ties in initiative
                        target_ties = 0
                        TARGET_TIE_CHECK_3: do j = 1, num_all_groups
                            if (groups(j) % team == curr_attacker % team) cycle ! friendly fire
                            if (associated(groups(j) % targeted_by)) cycle ! target already selected by another
                            if (groups(j) % num_units == 0) cycle ! target dead

                            this_dmg = curr_attacker % damage_to_group(j)
                            if (this_dmg == max_dmg .and. groups(j) % initiative == max_initiative) then
                                target_ties = target_ties + 1
                            end if
                        end do TARGET_TIE_CHECK_3

                        ! Tied again: impossible
                        if (target_ties == 1) then
                            curr_attacker % target % targeted_by => curr_attacker
                        else if (target_ties > 1) then
                            write (syslog%unit,*) 'ERROR: Failed to break all ties.'
                            error stop
                        else
                            curr_attacker % target => null()
                        end if
                    else
                        curr_attacker % target => null()
                    end if
                else
                    curr_attacker % target => null()
                end if

            end associate
        end do TARGET_SELECT_MAIN

        ! Diagnostic Output
        if (dbg) then
            write (syslog%unit,*) ' Attack Plan:'
            write (syslog%unit,'(a)') &
                '   ID          Team  EffPow Attacks           Victim    VicEffPow    VicInitInit'
            do i = 1, num_all_groups
                write (syslog%unit,'(i5,a15,i7,a)',advance='no') &
                    i, team_to_str(groups(target_selection_order(i)) % team), &
                    groups(target_selection_order(i)) % eff_power(), ' attacks> '
                if (associated(groups(target_selection_order(i)) % target)) then
                    write (syslog%unit,'(a15,2i10)',advance='no') &
                        team_to_str(groups(target_selection_order(i)) % target % team),   &
                        groups(target_selection_order(i)) % target % eff_power(),         &
                        groups(target_selection_order(i)) % target % initiative
                else
                    write (syslog%unit,'(a20)',advance='no') 'None'
                end if
                write (syslog%unit,*) ! advance
            end do
        end if

        !-- Attacking 
        ATTACK_ORDER_BLK: block
            integer :: next_high_init(3)
            attack_order(:) = 0

            next_high_init(:) = next_highest_initiative(HUGE(i))
            i = 1
            ATTACK_ORDER_LP: do
                if (i > num_all_groups) exit ATTACK_ORDER_LP

                if (next_high_init(2) == 1) then
                    attack_order(i) = next_high_init(1)
                    i = i + 1
                else
                    write (syslog%unit,*) 'Error: failure in ATTACK_ORDER_BLK'
                    stop
                end if
                next_high_init(:) = next_highest_initiative(next_high_init(3))
            end do ATTACK_ORDER_LP
            
            ! Diagnostic output
            if (.false.) then
                write (syslog%unit,*) ' Attack Order:'
                do i = 1, num_all_groups
                    write (syslog%unit,'(2i5,a20,2i8)') i, attack_order(i), &
                        team_to_str(groups(attack_order(i)) % team), &
                        groups(attack_order(i)) % eff_power(), groups(attack_order(i)) % initiative
                end do
            end if
        end block ATTACK_ORDER_BLK

        if (dbg) write (syslog%unit,'(*(g0))') '*** ATTACKS ***',' BOOST: ',boost
        ATTACK_LOOP: do i = 1, num_all_groups

            associate (active_grp => groups(attack_order(i)))

                if (associated(active_grp % target)) then
                    call active_grp % deal_damage_to_target
                else
                    if (dbg) write (syslog%unit,'(a)') team_to_str(active_grp % team)//' passes'
                end if

                if (num_immune_units == 0 .or. num_infection_units == 0) exit FIGHT_LOOP

            end associate

        end do ATTACK_LOOP

        if (dbg) then
            write (syslog%unit,*) '*** Round Summary ***'
            write (syslog%unit,*) 'Units Remaining'
            write (syslog%unit,*) 'Immune: ', num_immune_units
            write (syslog%unit,*) 'Infection: ', num_infection_units
        end if

    end do FIGHT_LOOP

    do i = 1, num_all_groups
        write (syslog%unit,'(i4,a20,2i6)') &
            i, team_to_str(groups(i) % team), groups(i) % group_num, groups(i) % num_units
    end do
    write (syslog%unit,*) 'Units Remaining'
    write (syslog%unit,*) 'Immune: ', num_immune_units
    write (syslog%unit,*) 'Infection: ', num_infection_units

    ! Calculate Part 1
    part1 = 0
    do i = 1, num_all_groups
        part1 = part1 + groups(i) % num_units
    end do

    if (boost == 0) then
        write (          *,*) 'Part 1: ',part1 ! 22083
        write (syslog%unit,*) 'Part 1: ',part1
    end if

    !-- Part 2
    if (num_immune_units > 0) then
        part2 = part1
        write (          *,*) 'Part 2: ',part2,'Boost: ',boost ! 8975
        write (syslog%unit,*) 'Part 2: ',part2,'Boost: ',boost
    else
        dbg = .false.
        boost = boost + 1
        goto 100 !goto was just easier :(
    end if

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program