program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, j, k, m

    !-- ASCII character index
    integer :: ix

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Current length of polymer
    integer :: len_polymer = 0

    !-- Parameters
    integer,parameter :: MAX_POLYMER_LEN = 60000
    integer,parameter :: ASCII_CHAR_SHIFT_UPPERCASE = 64
    integer,parameter :: ASCII_CHAR_SHIFT_LOWERCASE = 96
    ! A = 65, Z = 90, a = 97, z = 122
    ! a = 97 -> i = 1
    ! A = 65 -> i = 1

    !-- Polymers
    character(len=MAX_POLYMER_LEN)     :: polymer_raw
    character(len=MAX_POLYMER_LEN)     :: polymer
    character(len=MAX_POLYMER_LEN)     :: polymer_new
    integer,dimension(MAX_POLYMER_LEN) :: ipolymer
    
    !-- Main variables
    integer,parameter :: NUM_UNITS = 26 ! (number of letters)
    integer :: remove_upper_ix = 0      ! index of uppercase letter to try removing
    integer :: remove_lower_ix = 0      ! index of lowercase letter to try removing

    !-- Input file reading properties
    integer,parameter            :: max_line_len = 600000
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Start timer
    call syslog % start_timer

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    read (input_unit,'(a)') line
    close (input_unit)

    if (len(trim(line)) <= max_line_len) then
        polymer_raw = trim(adjustl(line))
        !write (syslog%unit,*) polymer
    else
        write(syslog%unit,*) 'Error: line exceeded maximum length'
        call bomb
    end if

    LESION_LOOP: do m = 0, NUM_UNITS

        polymer = polymer_raw
        
        ! For non-first loops, try removing letter pairs (Aa,Bb,etc.) and replace with space
        if (m /= 0) then
            remove_lower_ix = m + ASCII_CHAR_SHIFT_LOWERCASE
            remove_upper_ix = m + ASCII_CHAR_SHIFT_UPPERCASE
            do i = 1, len(polymer)
                if (iachar(polymer(i:i)) == remove_lower_ix .or. &
                    iachar(polymer(i:i)) == remove_upper_ix) then
                    polymer(i:i) = ' '
                end if
            end do
        end if
        
        k = 0
        MAIN_LOOP: do
        
            ! Increment loop counter
            k = k + 1
    
            ! Reset length reduction counter
            j = 0
    
            len_polymer = len(adjustl(trim(polymer)))
            ipolymer(:) = 0
            polymer_new = ' '
    
            POLYMER_DIGITIZER: do i = 1, len_polymer
    
                ix = iachar(polymer(i:i))
    
                if (ix >= 65 .and. ix <= 90) then ! uppercase (+ve)
                    ix = +(ix - ASCII_CHAR_SHIFT_UPPERCASE)
                else if (ix >= 97 .and. ix <= 122) then ! lowercase (-ve)
                    ix = -(ix - ASCII_CHAR_SHIFT_LOWERCASE)
                else if (ix == 32) then !space
                    ix = 0
                else
                    print*,'Unknown character',ix,'(',polymer(i:i),') on iteration ',k
                    error stop
                end if
    
                ipolymer(i) = ix
            end do POLYMER_DIGITIZER
    
            PAIR_ANNIHILATOR: do i = 1, len_polymer - 1
    
                if (ipolymer(i) == -ipolymer(i+1)) then
    
                    ! Annihilate
                    ipolymer(i:i)     = 0
                    ipolymer(i+1:i+1) = 0
    
                end if
    
            end do PAIR_ANNIHILATOR
    
            REBUILD_POLYMER_STRING: do i = 1, len_polymer
    
                if (ipolymer(i) == 0) then
                    j = j + 1
                    cycle REBUILD_POLYMER_STRING
                end if
    
                if (ipolymer(i) > 0) then
                    ix = ipolymer(i) + ASCII_CHAR_SHIFT_UPPERCASE
                    polymer_new(i-j:i-j) = achar(ix)
                else
                    ix = -ipolymer(i) + ASCII_CHAR_SHIFT_LOWERCASE
                    polymer_new(i-j:i-j) = achar(ix)
                end if
                
            end do REBUILD_POLYMER_STRING
            
            if (j == 0) exit MAIN_LOOP ! done: didn't remove any this round
    
           !write (syslog%unit,*) ' iter = ', k, ' len = ', size(ipolymer)
           !write (syslog%unit,*) polymer_new
    
            polymer = adjustl(polymer_new)
    
        end do MAIN_LOOP
    
        ! Part 1
        if (m == 0) then
            write (syslog%unit,*) 'Part 1: ', len(adjustl(trim(polymer))) ! 11754
            
            call syslog % end_timer
            call syslog % start_timer

            write (syslog%unit,*) 'Part 2: '
            write (syslog%unit,*) ' # Letter  Length'
        ! Part 2
        else
            write (syslog%unit,'(i3,a5,i10)') &
                m,achar(m+ASCII_CHAR_SHIFT_LOWERCASE),len(adjustl(trim(polymer))) ! t=4098
        end if

    end do LESION_LOOP

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program