module fuelcell_mod
    use syslog_mod
    implicit none

    !-- Parameters defining grid size
    integer,parameter :: NUM_CELLS_X = 300
    integer,parameter :: NUM_CELLS_Y = 300
    integer,parameter :: GRID_LENGTH = NUM_CELLS_X

    !-- Puzzle input
    integer,parameter :: GRID_SERIAL_NUM = 3214

    !-- FuelCell struct
    type :: FuelCell
        integer :: x            = 0  !
        integer :: y            = 0  !
        integer :: rackid       = 0  !
        integer :: power        = 0  !
        integer :: summed_area  = 0  !
    end type
    interface FuelCell
        module procedure init_fc
    end interface

    !-- FuelCellGrid struct
    type :: FuelCellGrid
        integer :: x_topleft   = 0  !
        integer :: y_topleft   = 0  !
        integer :: total_power = 0  !
        integer :: grid_length = 0  !
    end type
    interface FuelCellGrid
        module procedure init_fc_grid
    end interface

    !-- Array of nodes
    type(fuelcell) :: fc(NUM_CELLS_X,NUM_CELLS_Y)
    type(fuelcellgrid) :: fcgrid

contains

    type(FuelCell) pure function init_fc(x,y) result(fc)
        implicit none
        integer,intent(in) :: x
        integer,intent(in) :: y

        ! Set coordinates
        fc % x = x
        fc % y = y

        ! Rack ID is x-coordinate + 10
        fc % rackid = fc % x + 10

        ! Power level is rack ID times y-coordinate
        fc % power = fc % rackid * fc % y

        ! Increase power level by value of grid serial number
        fc % power = fc % power + GRID_SERIAL_NUM

        ! Set power level to itself multiplied by rack ID
        fc % power = fc % power * fc % rackid

        ! Keep only the hundreds digit of the power level
        fc % power = get_digit_of_number(3,fc % power)

        ! Subtract 5 from power level
        fc % power = fc % power - 5
        
    end function

    pure integer function get_digit_of_number(digit_place,num) result(digit)
        integer,intent(in) :: digit_place
        integer,intent(in) :: num
        digit = mod(int(num / 10**(digit_place-1)),10)
    end function

    type(FuelCellGrid) pure function init_fc_grid(x,y,power,gridlen) result(fcgrid)
        integer,intent(in) :: x
        integer,intent(in) :: y
        integer,intent(in) :: power
        integer,intent(in),optional :: gridlen

        ! Configure
        fcgrid % x_topleft = x
        fcgrid % y_topleft = y
        fcgrid % total_power = power
        if (present(gridlen)) fcgrid % grid_length = gridlen

    end function

    subroutine draw_grid(valuestr)
        implicit none
        character(len=*),intent(in) :: valuestr
        integer :: x
        integer :: y

        select case (valuestr)
        case ('power')
            write (syslog%unit,'(a)') 'Table of Power Levels'
        case ('summed_area')
            write (syslog%unit,'(a)') 'Summed Area Table'
        case default
            error stop "INVALID CALL TO DRAW_GRID"
        end select

        write (syslog%unit,'(i4)',advance='no') 0
        do x = 1, NUM_CELLS_X
            write (syslog%unit,'(i4)',advance='no') x
        end do
        write (syslog%unit,*) ! advance
        do y = 1, NUM_CELLS_Y
            write (syslog%unit,'(i4)',advance='no') y
            do x = 1, NUM_CELLS_X
                select case (valuestr)
                case ('power')
                    write (syslog%unit,'(i4)',advance='no') fc(x,y) % power
                case ('summed_area')
                    write (syslog%unit,'(i4)',advance='no') fc(x,y) % summed_area
                case default
                    error stop "INVALID CALL TO DRAW_GRID"
                end select
            end do
            write (syslog%unit,*) ! advance
        end do

    end subroutine
end module