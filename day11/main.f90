program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use fuelcell_mod
    implicit none

    !-- Counters
    integer :: x,y

    !-- Coordinates of top left point of current bounding box
    integer :: x_tl
    integer :: y_tl

    !-- Coordinates just OUTSIDE of top left point of bounding box
    integer :: x_tl_0
    integer :: y_tl_0

    !-- Grid Dimension (part 1)
    integer,parameter :: FC_GRID_DIM = 3

    !-- Grid dimension (part 2)
    integer :: gd

    !-- Current grid's total power level
    integer :: gridpower_tmp = 0

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Start timer
    call syslog % start_timer

    !-- Build grid
    do y = 1, NUM_CELLS_Y
        do x = 1, NUM_CELLS_X
            fc(x,y) = FuelCell(x,y)
        end do
    end do

    !-- Draw grid of cell power levels
    call draw_grid('power')

    !-- Calculate Summed Area Table
    do y = 1, NUM_CELLS_Y
        do x = 1, NUM_CELLS_X

            fc(x,y) % summed_area = fc(x,y) % power

            if (x > 1) fc(x,y) % summed_area = fc(x,y) % summed_area + fc(x-1,y  ) % summed_area
            if (y > 1) fc(x,y) % summed_area = fc(x,y) % summed_area + fc(x  ,y-1) % summed_area
            if (x > 1 .and. y > 1) &
                       fc(x,y) % summed_area = fc(x,y) % summed_area - fc(x-1,y-1) % summed_area
        end do
    end do

    !-- Draw grid of cell power levels
    call draw_grid('summed_area')

    !-- Part 1: assess 3x3 grid
    ! do y = 1, NUM_CELLS_Y - FC_GRID_DIM
    !     do x = 1, NUM_CELLS_X - FC_GRID_DIM
    !         gridpower_tmp = sum(fc(x:x+FC_GRID_DIM-1,y:y+FC_GRID_DIM-1) % power)
    !         if (gridpower_tmp > fcgrid % totalpower) fcgrid = FuelCellGrid(x,y,gridpower_tmp)
    !     end do
    ! end do
    do y = FC_GRID_DIM, NUM_CELLS_Y
        do x = FC_GRID_DIM, NUM_CELLS_X

            ! Coordinates of top left of bounding box
            x_tl = x - FC_GRID_DIM + 1
            y_tl = y - FC_GRID_DIM + 1

            ! Coordinates for area subtraction (just outside top left bounding box)
            x_tl_0 = max(x-FC_GRID_DIM,1)
            y_tl_0 = max(y-FC_GRID_DIM,1)

            ! Grid power
            gridpower_tmp =                       &
                + fc(x     ,y     ) % summed_area &
                + fc(x_tl_0,y_tl_0) % summed_area &
                - fc(x_tl_0,y     ) % summed_area &
                - fc(x     ,y_tl_0) % summed_area
            
            if (gridpower_tmp > fcgrid % total_power) &
                fcgrid = FuelCellGrid(x_tl,y_tl,gridpower_tmp)
        end do
    end do

    ! Log answer (part 1)
    call syslog%log(__FILE__,'Peak power for 3x3 grid:')
    write (syslog%unit,'(3(a,i0))')      &
        " x = ",     fcgrid % x_topleft, &
        " y = ",     fcgrid % y_topleft, &
        " power = ", fcgrid % total_power
    write (syslog%unit,'(a,i0,a,i0)') 'Part 1: ', fcgrid % x_topleft, ',', fcgrid % y_topleft ! 21,42
    write (          *,'(a,i0,a,i0)') 'Part 1: ', fcgrid % x_topleft, ',', fcgrid % y_topleft ! 21,42

    !-- Part 2: assess XxX grid
    ! do gd = 2, GRID_LENGTH
    !   do y = 1, NUM_CELLS_Y - gd
    !       do x = 1, NUM_CELLS_X - gd
    !           gridpower_tmp = sum(fc(x:x+gd-1,y:y+gd-1) % power)
    !           if (gridpower_tmp > fcgrid % totalpower) fcgrid = FuelCellGrid(x,y,gridpower_tmp,gd)
    !       end do
    !   end do
    ! end do
    fcgrid = FuelCellGrid(0,0,0,0) ! reset
    do gd = 2, GRID_LENGTH
        do y = gd, NUM_CELLS_Y
            do x = gd, NUM_CELLS_X

                ! Coordinates of top left of bounding box
                x_tl = x - gd + 1
                y_tl = y - gd + 1

                ! Coordinates for area subtraction (just outside top left bounding box)
                x_tl_0 = max(x-gd,1)
                y_tl_0 = max(y-gd,1)

                ! Grid power
                gridpower_tmp =                       &
                    + fc(x     ,y     ) % summed_area &
                    + fc(x_tl_0,y_tl_0) % summed_area &
                    - fc(x_tl_0,y     ) % summed_area &
                    - fc(x     ,y_tl_0) % summed_area
                
                if (gridpower_tmp > fcgrid % total_power) &
                    fcgrid = FuelCellGrid(x_tl,y_tl,gridpower_tmp,gd)
            end do
        end do
    end do

    ! Log answer (part 2)
    gd = fcgrid % grid_length
    call syslog%log(__FILE__,'Peak power for '//string(gd)//'x'//string(gd)//' grid:')
    write (syslog%unit,'(3(a,i0))')      &
        " x = ",     fcgrid % x_topleft, &
        " y = ",     fcgrid % y_topleft, &
        " power = ", fcgrid % total_power
    write (syslog%unit,'(3(a,i0))') 'Part 2: ', fcgrid % x_topleft, ',', fcgrid % y_topleft, ',', gd ! 230,212,13
    write (          *,'(3(a,i0))') 'Part 2: ', fcgrid % x_topleft, ',', fcgrid % y_topleft, ',', gd! 230,212,13







    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program