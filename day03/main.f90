program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, j, k

    !-- Position index within string
    integer :: pos

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Parameters
    integer,parameter :: WIDTH_MAX  = 1000
    integer,parameter :: HEIGHT_MAX = 1000

    !-- Main variables

    !-- Patch data
    integer,allocatable :: id        (:)
    integer,allocatable :: left_inset(:) 
    integer,allocatable :: top_inset (:) 
    integer,allocatable :: width     (:) 
    integer,allocatable :: height    (:) 

    !-- Fabric
    integer :: fabric        (WIDTH_MAX,HEIGHT_MAX) = 0
    integer :: fabric_overlap(WIDTH_MAX,HEIGHT_MAX) = 0       

    ! Measured total overlap area
    integer :: overlap_area = 0

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(id        (num_lines))
    allocate(left_inset(num_lines))
    allocate(top_inset (num_lines))
    allocate(width     (num_lines))
    allocate(height    (num_lines))

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    write (syslog%unit,*) 'Raw Records:'
    write (syslog%unit,*) 'Guard Act   Hr  Min  Mth  Day Year  DoY'
    do i = 1, num_lines
        
        ! Read line
        read (input_unit,'(a)') line
        
        ! Strip leading #
        line = line(2:)

        ! Find space
        pos = index(line,' ')
        read (line(1:pos),*) id(i)

        ! Strip everything before @
        pos = index(line,'@')
        line = line(pos+1:)

        ! Find colon
        pos = index(line,':')
        read (line(1:pos-1),*) left_inset(i), top_inset(i)

        ! Strip everything before colon
        line = line(pos+1:)
        pos = index(line,'x')

        ! Replace x with ,
        line(pos:pos) = ','

        ! Read remaining line into width and height
        read (line,*) width(i),height(i)

    end do
    close (input_unit)

    !-- Start timer
    call syslog % start_timer

    !-- Process patches
    do i = 1, num_lines
        do j = left_inset(i) + 1, left_inset(i) + width(i)
            do k = top_inset(i) + 1, top_inset(i) + height(i)
                fabric(j,k) = fabric(j,k) + 1
            end do
        end do
    end do

    !-- Illustrate patches
    write (syslog%unit,*) ' Patches layout:'
    PATCHES_LOOP: do k = 1, HEIGHT_MAX
        write (syslog%unit,'('//string(width_max)//'i0)') fabric(:,k)
    end do PATCHES_LOOP

    !-- Apply mask to calculate overlaps
    where (fabric > 1) fabric_overlap = 1
    overlap_area = sum(fabric_overlap) !104241

    write (syslog%unit,*) ' Amount of fabric in 2 more more claims: ',overlap_area
    write (          *,*) ' Amount of fabric in 2 more more claims: ',overlap_area

   !-- Find only fully independent patch
    PATCH_LOOP: do i = 1, num_lines
        do j = left_inset(i) + 1, left_inset(i) + width(i)
            do k = top_inset(i) + 1, top_inset(i) + height(i)
                if (fabric(j,k) > 1) cycle PATCH_LOOP
            end do
        end do
        write (syslog%unit,*) ' Found independent patch: ',i !806
        write (          *,*) ' Found independent patch: ',i
    end do PATCH_LOOP

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program