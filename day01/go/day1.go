package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	file, err := os.Open("../day1.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	part1 := 0
	var inputValues []int
	var baselineFreqs []int
	var currentFreqs []int

	for scanner.Scan() {
		line := scanner.Text()
		i, err := strconv.Atoi(line)
		if err != nil {
			log.Fatal(err)
		}
		inputValues = append(inputValues, i)
		part1 += i
		baselineFreqs = append(baselineFreqs, part1)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Part 1: ", part1)

	for _, bfreq := range baselineFreqs {
		currentFreqs = append(currentFreqs, bfreq)
	}

	shifts := 0
	for {
		shifts += 1

		for i := range baselineFreqs {
			currentFreqs[i] = baselineFreqs[i] + (shifts * part1)
		}

		for _, cfreq := range currentFreqs {
			for _, bfreq := range baselineFreqs {
				if cfreq == bfreq {
					part2 := cfreq
					fmt.Println("Part 2: ", part2)
					return
				}
			}
		}
	}
}
