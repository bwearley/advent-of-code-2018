#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <numeric>
//#include <set>
//#include <utility>
//#include <tuple>
//#include <map>
//#include <sstream>

void Day1(std::string file)
{
  std::ifstream in(file);
  std::vector<int> input_freqs;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      input_freqs.emplace_back(std::stoi(line,0));
    }
  }

  // Calculate frequency shift
  int freq_shift = std::accumulate(input_freqs.begin(), input_freqs.end(), 0);
  std::cout << "Part 1: " << freq_shift << std::endl;

  // Calculate baseline frequencies
  std::vector<int> baseline_freqs;
  baseline_freqs = input_freqs;
  for (int i = 0; i < input_freqs.size(); i++)
  {
    baseline_freqs[i] = std::accumulate(input_freqs.begin(), input_freqs.begin()+i+1, 0);
  }


  int j;
  std::vector<int> current_freqs;
  current_freqs = baseline_freqs;
  for (j = 1; ; j++)
  {
    for (int i = 0; i < current_freqs.size(); i++)
    {
      current_freqs[i] = baseline_freqs[i] + (j * freq_shift);
    }

    for (int k = 0; k < input_freqs.size(); k++)
    {
      for (int m = 0; m < input_freqs.size(); m++)
      {
        if (baseline_freqs[m] == current_freqs[k])
        {
          std::cout << "Part 2: " << current_freqs[k] << " (" <<j <<" iterations)" << std::endl;
          return;
        }
      }
    }
  }
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day1(argv[1]);

}