program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, j

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Raw frequencies from input file
    integer,allocatable :: input_values(:)

    !-- Frequencies
    integer,allocatable :: baseline_freqs(:)
    integer,allocatable :: current_freqs(:)

    !-- Shift frequency per pass through values
    integer :: freq_shift = 0

    !-- Maximum number of frequencies to store
    integer,parameter :: MAX_FREQ_LOOPS = 999999999

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(input_values  (num_lines))
    allocate(baseline_freqs(num_lines))
    allocate(current_freqs (num_lines))

    !-- Initializations
    input_values(:)   = 0
    baseline_freqs(:) = 0
    current_freqs(:)  = 0

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    read (input_unit,*) (input_values(i), i = 1, num_lines)
    close (input_unit)
    write (syslog%unit,*) 'Found input values: '
    write (syslog%unit,'(20i5)') input_values

    freq_shift = sum(input_values)
    write (syslog%unit,*) 'Part 1: final frequency'
    write (syslog%unit,*) freq_shift

    !-- Start timer
    call syslog % start_timer

    !-- Initialize
    do i = 1, num_lines
        baseline_freqs(i) = sum(input_values(1:i))
    end do
    write (syslog%unit,*) 'Baseline frequencies:'
    write (syslog%unit,'(20i7)') baseline_freqs(:)

    !-- Main
    j = 0
    MAIN_LP: do
        j = j + 1

        current_freqs(:) = baseline_freqs(:) + (j * freq_shift)

        do i = 1, num_lines
            if (any(baseline_freqs(:) == current_freqs(i))) exit MAIN_LP
        end do
     
        ! Write output periodically to check status
        if (mod(j,1000) == 0) then
            write (syslog%unit,*) 'iter = ', j , ' freqs = ', current_freqs(:)
        end if

        if (j >= MAX_FREQ_LOOPS) then
            write (syslog%unit,*) 'Failed to find a match after ', j, ' iterations'
            write (          *,*) 'Failed to find a match after ', j, ' iterations'
            call bomb
        end if
    end do MAIN_LP

    !-- Write to log
    write (syslog%unit,*) 'Performed ', j, ' loops'
    write (syslog%unit,*) 'First repeat: ', current_freqs(i) !75749
    write (          *,*) 'First repeat: ', current_freqs(i)

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program