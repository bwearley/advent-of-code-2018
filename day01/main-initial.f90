program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, j

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Raw frequencies from input file
    integer,allocatable :: input_values(:)

    !-- Current frequency
    integer :: current_freq = 0

    !-- Maximum number of frequencies to store
    integer,parameter :: MAX_SEEN_FREQS = 999999

    !-- Array of frequencies seen so far
    integer :: seen_freqs(MAX_SEEN_FREQS)   

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(input_values(num_lines))

    !-- Initializations
    input_values(:) = 0

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    read (input_unit,*) (input_values(i), i = 1, num_lines)
    close (input_unit)
    write (syslog%unit,*) 'Found input values: '
    write (syslog%unit,'(20i5)') input_values

    write (syslog%unit,*) 'Part 1: final frequency'
    write (syslog%unit,*) sum(input_values)

    !-- Start timer
    call syslog % start_timer

    !-- Main loop
    j = 0
    MAIN_LOOP: do

        INPUTS_LOOP: do i = 1, num_lines

            j = j + 1

            current_freq = current_freq + input_values(i)

            if (any(seen_freqs(1:j) == current_freq)) exit MAIN_LOOP

            seen_freqs(j) = current_freq

            ! Write output periodically to check status
            if (mod(j,1000) == 0) then
                write (syslog%unit,*) 'iter = ', j , ' freq = ', current_freq
            end if
        
        end do INPUTS_LOOP
        if (j >= MAX_SEEN_FREQS) then
            write (syslog%unit,*) 'Failed to find a match after ', j, ' iterations'
            write (          *,*) 'Failed to find a match after ', j, ' iterations'
            call bomb
        end if
    end do MAIN_LOOP

    !-- Write to log
    write (syslog%unit,*) 'Performed ', j, ' loops'
    write (syslog%unit,*) 'First repeat: ', current_freq !75749
    write (          *,*) 'First repeat: ', current_freq

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program