#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <string.h>

int sum(int arr[], int n) {
    int sum = 0;
    for (int i = 0; i <= n; i++) {
        sum += arr[i];
    }
    return sum;
}

int main(int argc, char *argv[]) {
    
    /*
     * Variable Declarations
     */

    // Parameters
    const int MAX_FREQUENCY_ADJUSTMENTS = 10000;

    // Frequencies
    int input_values  [MAX_FREQUENCY_ADJUSTMENTS] = {0};
    int baseline_freqs[MAX_FREQUENCY_ADJUSTMENTS] = {0};
    int current_freqs [MAX_FREQUENCY_ADJUSTMENTS] = {0};
    int freq_shift = 0;

    // File handling
    int num_lines = 0;
    FILE *file;
    char filename[200];
    char *newline;

    /*
     * Active Section
     */
    // Check arguments
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    // Get filename
    strncpy(filename, argv[1], sizeof(filename));
    newline = strchr(filename, '\n');
    if (newline) *newline = '\0';

    // Open & read file
    file = fopen(filename, "r");
    if (file) {
        char line[50];
        int i = 0;
        while (fgets(line, 50, file)) {
            sscanf(line, "%d\n", &input_values[i]);
            i += 1;
        }
        fclose(file);
        num_lines = i;
        printf("Number of lines: %d\n",num_lines);
    } else {
        printf("File not found.");
    }
    printf("\n");

    // Calculate frequency shift
    freq_shift = sum(input_values, num_lines);
    printf("Frequency shift: %d\n",freq_shift);

    // Initialize baseline frequencies
    for (int i = 0; i < num_lines; i++) {
        for (int j = 0; j <= i; j++) {
            baseline_freqs[i] += input_values[j];
        }
    }

    // Main loop
    int i = 0;
    int k = 0;
    int loop = 1;
    do {
        
        i++;
        
        for (int j = 0; j < num_lines; j++) {
            current_freqs[j] = baseline_freqs[j] + (i * freq_shift);
        }

        for (k = 0; k < num_lines; k++) {
            for (int m = 0; m < num_lines; m++) {
                if (baseline_freqs[m] == current_freqs[k]) {
                    loop = 0;
                    break;
                }
            }
            if (loop == 0) break;
        }
    } while (loop);

    printf("First repeat after %d loops: %d\n", i, current_freqs[k]); //75749

    return 0;
}