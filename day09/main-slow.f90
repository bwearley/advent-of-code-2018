program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    integer,parameter :: ik = 8

    !-- Counters
    integer(ik) :: r, i

    ! Tracks current marble
    integer(ik) :: currentmarble = 0

    ! Tracks current active player
    integer(ik) :: activeplayer = 0

    !-- Turn on/off pyramid printouts
    logical,parameter :: DEBUG_PRINTOUTS = .false.

    !-- MAIN VALUES
    ! Part 1
    ! integer(ik),parameter :: NUM_PLAYERS = 419
    ! integer(ik),parameter :: LAST_MARBLE = 72164
    ! integer(ik),parameter :: EXPECTED_ANSWER = 423717

    ! Part 2
    integer(ik),parameter :: NUM_PLAYERS = 419_ik
    integer(ik),parameter :: LAST_MARBLE = 7216400_ik
    integer(ik),parameter :: EXPECTED_ANSWER = 3553108197_ik
    !-- MAIN VALUES

    !-- Test values
    ! integer(ik),parameter :: NUM_PLAYERS = 9
    ! integer(ik),parameter :: LAST_MARBLE = 25
    ! integer(ik),parameter :: EXPECTED_ANSWER = 32
    
    ! integer(ik),parameter :: NUM_PLAYERS = 10
    ! integer(ik),parameter :: LAST_MARBLE = 1618
    ! integer(ik),parameter :: EXPECTED_ANSWER = 8317

    ! integer(ik),parameter :: NUM_PLAYERS = 13
    ! integer(ik),parameter :: LAST_MARBLE = 7999
    ! integer(ik),parameter :: EXPECTED_ANSWER = 146373

    ! integer(ik),parameter :: NUM_PLAYERS = 17
    ! integer(ik),parameter :: LAST_MARBLE = 1104
    ! integer(ik),parameter :: EXPECTED_ANSWER = 2764

    ! integer(ik),parameter :: NUM_PLAYERS = 21
    ! integer(ik),parameter :: LAST_MARBLE = 6111
    ! integer(ik),parameter :: EXPECTED_ANSWER = 54718

    ! integer(ik),parameter :: NUM_PLAYERS = 30
    ! integer(ik),parameter :: LAST_MARBLE = 5807
    ! integer(ik),parameter :: EXPECTED_ANSWER = 37305

    !-- Multiples of this value trigger special game behavior
    integer(ik),parameter :: SPECIAL_DIVISOR_VALUE = 23

    !-- When special behavior is triggered by multiples of above, this shift is sued
    integer(ik),parameter :: SPECIAL_SHIFT_VALUE = 7

    !-- Scores of all players
    integer(ik) :: scores(NUM_PLAYERS) = 0

    !-- Position of marble (index is marble number, value is its position)
    integer(ik) :: pos(0:LAST_MARBLE) = -1

    integer(ik) :: newpos, seekposition

    character(len=5000) :: outstr

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Start timer
    call syslog % start_timer

    !-- Initialize
    pos(0) = 1
    currentmarble = 0

    if (DEBUG_PRINTOUTS) then
        write(*,'(a)',advance='no') '[-]'
        write(*,'(i2)') 0
    end if
 
    MAIN_LOOP: do r = 1, LAST_MARBLE

        ! Switch player
        activeplayer = activeplayer + 1
        if (activeplayer > NUM_PLAYERS) activeplayer = 1

        ! Normal behavior: add marble to circle
        if (mod(r, SPECIAL_DIVISOR_VALUE) /= 0) then

            ! Calculate new marble's position
            if (pos(currentmarble) + 2 > maxval(pos(:currentmarble)) + 1) then
                newpos = 2
            else
                newpos = pos(currentmarble) + 2
            end if

            ! Slide marbles over
            SLIDE_LP: do i = 0, r   !newpos, r
                if (pos(i) < newpos) cycle SLIDE_LP
                pos(i) = pos(i) + 1
            end do SLIDE_LP

            ! Insert marble
            pos(r) = newpos

            ! Switch marble
            currentmarble = r

        ! Special behavior: remove marbles
        else
            scores(activeplayer) = scores(activeplayer) + r

            ! Calculate which marble is 7 to the left of currentmarble
            seekposition = pos(currentmarble) - SPECIAL_SHIFT_VALUE
            if (seekposition < 0) seekposition = maxval(pos(0:r)) - abs(seekposition)
            do i = 0, r
                if (pos(i) == seekposition) then
                    scores(activeplayer) = scores(activeplayer) + i
                    pos(i) = -1
                    exit
                end if
            end do

            ! Shift marbles accordingly
            do i = 0, r
                if (pos(i) == -1) cycle
                if (pos(i) > seekposition) then
                    pos(i) = pos(i) - 1
                end if
            end do

            ! Seek new current marble (clockwise of removed marble)
            if (seekposition > maxval(pos(0:r)) + 1) seekposition = maxval(pos(0:r)) - abs(seekposition)
            do i = 0, r
                if (pos(i) == seekposition) then
                    currentmarble = i
                    exit
                end if
            end do

        end if

        ! if (DEBUG_PRINTOUTS) then
        !     write(*,'(a,i0,a)',advance='no') '[',activeplayer,']'
        !     outstr = ' '
        !     do i = 0, r
        !         if (pos(i) == -1) cycle
        !         outstr(3*pos(i):3*pos(i)+1) = string(i)
        !     end do
        !     write(*,'(a)',advance='yes') trim(outstr)
        ! end if

        if (mod(r,10000) == 0) then
            write (syslog%unit,*) 'Processed marble ',r
        end if

    end do MAIN_LOOP

    !-- Write answer (part 1: 423717, part 2: )
    write (syslog%unit,'(2(a,i0))') 'Max score: ',maxval(scores(:)),' by player: ',maxloc(scores(:),1)
    write (          *,'(2(a,i0))') 'Max score: ',maxval(scores(:)),' by player: ',maxloc(scores(:),1)


    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program