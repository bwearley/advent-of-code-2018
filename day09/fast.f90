program main
    implicit none

    !-- Integer Kind
    integer,parameter :: ik = 8

    !-- Counters
    integer(ik) :: r, i

    ! Tracks current active player
    integer(ik) :: activeplayer = 0

    !-- Turn on/off pyramid printouts
    logical,parameter :: DEBUG_PRINTOUTS = .false.

    !-- MAIN VALUES
    ! Part 1
    ! integer(ik),parameter :: NUM_PLAYERS = 419
    ! integer(ik),parameter :: LAST_MARBLE = 72164
    ! integer(ik),parameter :: EXPECTED_ANSWER = 423717

    ! Part 2
    integer(ik),parameter :: NUM_PLAYERS = 419
    integer(ik),parameter :: LAST_MARBLE = 72164 * 100
    integer(ik),parameter :: EXPECTED_ANSWER = 3553108197_ik
    !-- MAIN VALUES

    !-- Test values
    ! integer(ik),parameter :: NUM_PLAYERS = 9
    ! integer(ik),parameter :: LAST_MARBLE = 25
    ! integer(ik),parameter :: EXPECTED_ANSWER = 32

    ! integer(ik),parameter :: NUM_PLAYERS = 10
    ! integer(ik),parameter :: LAST_MARBLE = 1618
    ! integer(ik),parameter :: EXPECTED_ANSWER = 8317

    ! integer(ik),parameter :: NUM_PLAYERS = 13
    ! integer(ik),parameter :: LAST_MARBLE = 7999
    ! integer(ik),parameter :: EXPECTED_ANSWER = 146373

    ! integer(ik),parameter :: NUM_PLAYERS = 17
    ! integer(ik),parameter :: LAST_MARBLE = 1104
    ! integer(ik),parameter :: EXPECTED_ANSWER = 2764

    ! integer(ik),parameter :: NUM_PLAYERS = 21
    ! integer(ik),parameter :: LAST_MARBLE = 6111
    ! integer(ik),parameter :: EXPECTED_ANSWER = 54718

    ! integer(ik),parameter :: NUM_PLAYERS = 30
    ! integer(ik),parameter :: LAST_MARBLE = 5807
    ! integer(ik),parameter :: EXPECTED_ANSWER = 37305

    !-- Multiples of this value trigger special game behavior
    integer(ik),parameter :: SPECIAL_DIVISOR_VALUE = 23

    !-- When special behavior is triggered by multiples of above, this shift is sued
    integer(ik),parameter :: SPECIAL_SHIFT_VALUE = 7

    !-- Scores of all players
    integer(ik) :: scores(NUM_PLAYERS) = 0

    type :: marble
        integer(ik) :: x = 0
        type(marble), pointer :: cw  => null()
        type(marble), pointer :: ccw => null()
    end type

    type(marble), target  :: marbles(0:LAST_MARBLE)
    type(marble), pointer :: current => marbles(0)
    type(marble), pointer :: posptr => null()

    !-- Initialize
    marbles(0) % cw  => marbles(0)
    marbles(0) % ccw => marbles(0)

    if (DEBUG_PRINTOUTS) then
        write(*,'(a)',advance='no') '[-]'
        write(*,'(a)') ' (0)'
    end if

    MAIN_LOOP: do r = 1, LAST_MARBLE

        ! Switch player
        activeplayer = activeplayer + 1
        if (activeplayer > NUM_PLAYERS) activeplayer = 1

        ! Normal behavior: add marble to circle
        if (mod(r, SPECIAL_DIVISOR_VALUE) /= 0) then

            ! Get marble to place next marble next to
            posptr => current % cw

            ! Assign value to marble
            marbles(r) % x = r

            ! Assign current marble's pointers based on the position pointer
            marbles(r) % ccw => posptr
            marbles(r) % cw  => posptr % cw

            ! write (*,'(a,i0,a,i0,a,i0,a)') &
            !     'Inserting ',marbles(r) % x, ' with ', &
            !     marbles(r) % ccw % x,' left of it and ',&
            !     marbles(r) % cw % x, ' to the right of it'

            ! Reassign pointer of marble previously adjacent to position pointer to new marble
            posptr % cw % ccw => marbles(r)

            ! Reassign pointer of position pointer to new marble
            posptr % cw => marbles(r)

            ! Switch current marble
            current => marbles(r)

        ! Special behavior: remove marbles
        else

            ! Shift position pointer ccw 7 times
            posptr => current
            do i = 1, SPECIAL_SHIFT_VALUE
                posptr => posptr % ccw
            end do

            ! Increase player score
            scores(activeplayer) = scores(activeplayer) + r
            scores(activeplayer) = scores(activeplayer) + posptr % x

            ! Remove the marble
            posptr % ccw % cw => posptr % cw
            posptr % cw % ccw => posptr % ccw

            ! Switch current marble
            current => posptr % cw

            ! Nullify
            posptr % cw => null()
            posptr % ccw => null()

        end if

        if (DEBUG_PRINTOUTS) then
            write(*,'(a,i0,a)',advance='no') '[',activeplayer,']'
            write (*,'(i3)',advance='no') marbles(0) % x
            posptr => marbles(0)
            do i = 0, r - 1
                if (current % x == posptr % cw % x) then
                    write (*,'(a,i2,a)',advance='no') '(',posptr % cw % x, ')'
                else
                    write (*,'(i4)',advance='no') posptr % cw % x
                end if
                posptr => posptr % cw
            end do
            write(*,*) ! advance
        end if

    end do MAIN_LOOP

    ! print*,marbles(0) % x
    ! do i = 0, LAST_MARBLE
    !     print*,marbles(i) % cw %x
    ! end do

    !-- Write answer (part 1: 423717, part 2: )
    write (          *,'(2(a,i0))') 'Max score: ',maxval(scores(:)),' by player: ',maxloc(scores(:),1)
    write (          *,'(a,i0)') 'Expected answer: ', EXPECTED_ANSWER

end program
