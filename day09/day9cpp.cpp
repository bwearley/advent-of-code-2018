#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <numeric>
//#include <set>
//#include <utility>
//#include <tuple>
//#include <map>
//#include <sstream>

struct Marble
{
    int x;
    struct Marble *cw;
    struct Marble *ccw;
};

void Day9(std::string file)
{
  std::ifstream in(file);

//   // Load file data
//   while (!in.eof())
//   {
//     std::string line;
//     std::getline(in, line);

//     if (!in.fail())
//     {
//       input_freqs.emplace_back(std::stoi(line,0));
//     }
//   }

  //const unsigned long NUM_PLAYERS = 9;
  //const unsigned long LAST_MARBLE = 25;
  //const unsigned long EXPECTED_ANSWER = 32;

  //const unsigned long NUM_PLAYERS = 30;
  //const unsigned long LAST_MARBLE = 5807;
  //const unsigned long EXPECTED_ANSWER = 37305;

  //const unsigned long NUM_PLAYERS = 419;
  //const unsigned long LAST_MARBLE = 72164;
  //const unsigned long EXPECTED_ANSWER = 423717;

  const unsigned long NUM_PLAYERS = 419;
  const unsigned long LAST_MARBLE = 72164 * 100;
  const unsigned long EXPECTED_ANSWER = 3553108197;

  // Multiples of this value trigger special game behavior
  const long SPECIAL_DIVISOR_VALUE = 23;

  // When special behavior is triggered by multiples of above, this shift is used
  const long SPECIAL_SHIFT_VALUE = 7;

  const bool DEBUG_PRINTOUTS = false;

  unsigned long activeplayer = 0;

  // Player scores
  std::vector<unsigned long> scores(NUM_PLAYERS);

  //
  struct Marble *current;
  struct Marble *posptr;
  // Marble marbles[LAST_MARBLE];
  Marble* marbles = new Marble[LAST_MARBLE]; // allocate on heap

  // Initialize
  std::fill(scores.begin(),scores.end(),0);

  marbles[0].cw  = &marbles[0];
  marbles[0].ccw = &marbles[0];
  current = &marbles[0];

  if (DEBUG_PRINTOUTS) {
    std::cout << "[-]" << " (0) " << std::endl;
  }

  for (long i = 1; i <= LAST_MARBLE; i++)
  {

    // Switch player
    activeplayer = activeplayer + 1;
    if (activeplayer > NUM_PLAYERS) activeplayer = 1;
    
    // Normal behavior: add marble to circle
    if (i % SPECIAL_DIVISOR_VALUE != 0)
    {
        // Get marble to place next marble next to
        posptr = current->cw;

        // Assign value to marble
        marbles[i].x = i;

        // Assign current marble's pointers based on the position pointer
        marbles[i].ccw = posptr;
        marbles[i].cw  = posptr->cw;

        /*std::cout << "Inserting "
                  << marbles[i].x << " with "
                  << marbles[i].ccw->x << " left of it and "
                  << marbles[i].cw->x << " to the right of it"
                  << std::endl;*/

        // Reassign pointer of marble previously adjacent to position pointer to new marble
        posptr->cw->ccw = &marbles[i];

        // Reassign pointer of position pointer to new marble
        posptr->cw = &marbles[i];

        // Switch current marble
        current = &marbles[i];

    }
    // Special behavior: remove marbles
    else
    {
        
        // Shift position pointer ccw 7 times
        posptr = current;
        for (long j = 0; j < SPECIAL_SHIFT_VALUE; j++)
        {
            posptr = posptr->ccw;
        }

        // Increase player score
        scores[activeplayer-1] = scores[activeplayer-1] + i;
        scores[activeplayer-1] = scores[activeplayer-1] + posptr->x;

        // Remove the marble
        posptr->ccw->cw = posptr->cw;
        posptr->cw->ccw = posptr->ccw;

        // Switch current marble
        current = posptr->cw;

        // Nullify
        posptr -> cw = NULL;
        posptr -> ccw = NULL;

    }

    if (DEBUG_PRINTOUTS) {
        std::cout << "[" << activeplayer << "]";
        std::cout << marbles[0].x;
        posptr = &marbles[0];
        for (long j = 0; j < i - 1; j++)
        {
            if (current->x == posptr->cw->x) {
                std::cout <<  "(" << posptr -> cw -> x << ")";
            }
            else
            {
                std::cout <<  posptr -> cw -> x;
            }
            posptr = posptr->cw;
        }
        std::cout << std::endl;
    }
  }

  // Segmentation fault for part 2
  std::cout << "Max score: " 
            << *std::max_element(scores.begin(),scores.end())
            << " by player "
            << std::distance(scores.begin(),std::max_element(scores.begin(),scores.end())) + 1
            << std::endl;
  std::cout << "Expected result: " << EXPECTED_ANSWER << std::endl;
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day9(argv[1]);

}