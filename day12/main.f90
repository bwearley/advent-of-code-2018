program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use recipe_mod
    implicit none

    !-- Integer Kind
    integer,parameter :: ik = 8

    !-- Counters
    integer(ik) :: i, j, x

    !-- Input file unit
    integer(ik) :: input_unit

    !-- Position indicator in string
    integer(ik) :: pos

    !-- Number of lines in input file
    integer(ik) :: num_lines

    !-- Number of recipes
    integer(ik) :: num_recipes = 0

    !-- Dynamically sized window on which to perform calculation
    integer(ik) :: leftmost_extent = -3_ik
    integer(ik) :: rightmost_extent = 150_ik

    !-- Answer convergence checks
    integer(ik) :: ans = 0                      ! current answer
    integer(ik) :: ans_last = 0                 ! last generation's answer
    integer(ik) :: ans_shift = 0                ! shift between this and last generation
    integer(ik) :: ans_shift_last = 0           ! shift between last generation and the gen before that
    integer(ik) :: num_unchanged_shift = 0      ! number of generations in which the shift has been unchanged

    !integer(ik),parameter :: NUM_GENERATIONS = 20
    integer(ik),parameter :: NUM_GENERATIONS = 50000000000_ik

    !-- Number of generations to wait to see if everything converges to gliders
    integer(ik),parameter :: NUM_UNCHANGED_GENS = 10_ik

    !-- Parameters
    integer(ik),parameter :: MAX_DIM_LEFT = -100
    integer(ik),parameter :: MAX_DIM_RIGHT = 1000

    !-- Generation Number
    integer(ik) :: gen = 0

    !-- Plant statuses per position
    integer(ik) :: plants(MAX_DIM_LEFT:MAX_DIM_RIGHT) = 0
    integer(ik) :: plants_new(MAX_DIM_LEFT:MAX_DIM_RIGHT) = 0
    character(len=:),allocatable :: initial_state    

    ! Input file reading properties
    integer(ik),parameter            :: max_line_len = 500
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)
    num_recipes = num_lines - 2

    !-- Allocate data appropriately
    allocate(recipes(num_recipes))

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    
    !-- Start timer
    call syslog % start_timer

    !-- Read initial state
    read (input_unit,'(a)') line
    pos = index(line,':')
    line = adjustl(trim(line(pos+1:)))
    initial_state = trim(line)
    call syslog%log(__FILE__,'Initial state: ')
    write (syslog%unit,'(a)') initial_state
    do i = 1, len(initial_state)
        if (initial_state(i:i) == '#') plants(i-1) = 1
    end do
    do i = -3, len(initial_state)
        write (syslog%unit,'(i3)',advance='no') i
    end do
    write (syslog%unit,*) ! advance
    do i = -3, len(initial_state)
        write (syslog%unit,'(i3)',advance='no') plants(i)
    end do
    write (syslog%unit,*) ! advance

    !- Read recipes
    read (input_unit,*) ! skip line
    do i = 1, num_recipes

        ! Read line
        read (input_unit,'(a)') line

        ! Parse line
        recipes(i) = Recipe(line)

    end do
    close (input_unit)

    !-- Write to log
    call syslog%log(__FILE__,'Found '//string(num_lines)//' recipes.')
    write (syslog%unit,*) 'Rec# Rslt   L2   L1    V   R1   R2'
    do i = 1, num_recipes
        write (syslog%unit,'(i5,L5,5i5)') &
            i,                          &
            recipes(i) % makes_plant,   &
            recipes(i) % pattern
    end do

    !-- Process generations
    call write_state
    gen = 1
    do !gen = 1, NUM_GENERATIONS
    
        do x = leftmost_extent, rightmost_extent

            ! Get current window
            curr_pattern = plants(x-REC_LEN:x+REC_LEN)

            ! Check which recipe to apply
            RECIPE_CHK: do j = 1, num_recipes

                if (all(recipes(j) % pattern == curr_pattern)) then
                    if (recipes(j) % makes_plant) plants_new(x) = 1
                    exit RECIPE_CHK
                end if

            end do RECIPE_CHK

            ! Dynamically expand extents (if applicable)
            if (x < leftmost_extent  + 5 .and. plants(x) == 1) leftmost_extent  = leftmost_extent  - 30
            if (x > rightmost_extent - 5 .and. plants(x) == 1) rightmost_extent = rightmost_extent + 30

        end do

        ! Update plants
        plants(:) = plants_new(:)

        ! Clear temporary array
        plants_new(:) = 0

        ! Write state
        if (gen < 200) call write_state

        ! Calculate answer critiera (sum of position indices of plants)
        ans = 0
        do j = leftmost_extent, rightmost_extent
            if (plants(j) == 1) ans = ans + j
        end do
        ans_shift = ans - ans_last
        if (ans_shift_last == ans_shift) then
            num_unchanged_shift = num_unchanged_shift + 1
        else
            num_unchanged_shift = 0
            ans_shift_last = ans_shift
        end if
        ans_last = ans

        ! Write part 2 answer (3600000002022)
        if (num_unchanged_shift > NUM_UNCHANGED_GENS) then
            write (syslog%unit,'(2(a,i0))')'Finished at generation: ', gen,' with answer: ',ans
            write (syslog%unit,'(a,i0,a)') 'Gliders shift ', ans_shift, ' per generation'
            write (syslog%unit,'(a,i0)')   'Converged at gen ', gen-NUM_UNCHANGED_GENS
            write (syslog%unit,'(a,i0)')   'Part 2: ', ans + ans_shift * (NUM_GENERATIONS-gen)
            write (          *,'(a,i0)')   'Part 2: ', ans + ans_shift * (NUM_GENERATIONS-gen)
            exit
        end if

        !-- Write part 1 answer (3258)
        if (gen == 20) then
            write (syslog%unit,'(a,i0)') 'Part 1: ', ans
            write (          *,'(a,i0)') 'Part 1: ', ans
        end if

        ! Next generation
        gen = gen + 1

    end do

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

contains

    !> Write out the current state of plants
    subroutine write_state
        implicit none
        integer,parameter :: WINDOW_MIN = -3
        integer,parameter :: WINDOW_MAX = 250

        write (syslog%unit,'(i3,a)',advance='no') gen, ': '
        do i = WINDOW_MIN, WINDOW_MAX
            if (plants(i) == 1) then
                write (syslog%unit,'(a)',advance='no') '#'
            else
                write (syslog%unit,'(a)',advance='no') '.'
            end if
        end do
        write (syslog%unit,*) ! advance
    end subroutine
end program