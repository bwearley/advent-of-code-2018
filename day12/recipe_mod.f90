module recipe_mod
    use syslog_mod
    use string_tools_mod
    implicit none

    ! Length of one side of a recipe window
    integer,parameter :: REC_LEN = 2

    ! The current window of actual plant statuses
    integer :: curr_pattern(-REC_LEN:REC_LEN)

    !-- Recipe struct
    type :: Recipe
        !integer :: id = 0
        logical :: makes_plant
        integer :: pattern(-REC_LEN:+REC_LEN)
    end type
    interface Recipe
        module procedure init_recipe_from_string
    end interface

    !-- Array of Recipes
    type(Recipe), allocatable :: recipes(:)

contains

    type(Recipe) function init_recipe_from_string(str) result(r)
        implicit none
        character(len=*),intent(in) :: str
        integer :: i, str_idx

        ! Read first part of recipe
        do i = -REC_LEN, REC_LEN

            str_idx = i + REC_LEN + 1

            select case (str(str_idx:str_idx))
            case ('.')
                r % pattern(i) = 0
            case ('#')
                r % pattern(i) = 1
            case default
                call syslog%log(__FILE__,'Failed on character: '//string(str_idx)//' '//string(i))
                call syslog%log(__FILE__,'ERROR: Failure reading recipe string: '//str)
                error stop 'BAD RECIPE STRING'
            end select

        end do

        ! Strip recipe to just result
        i = index(str,'>')
        select case (trim(str(i+2:)))
        case ('.')
            r % makes_plant = .false.
        case ('#')
            r % makes_plant = .true.
        case default
            call syslog%log(__FILE__,'ERROR: Failure reading recipe string: '//str)
            error stop 'BAD RECIPE STRING'
        end select
        
    end function
end module
