module guard_record_mod
    use syslog_mod
    implicit none

    ! Total number of days from previous months
    integer,parameter :: doy_per_month(12) = &
        [000, 031, 059, 090, 120, 151, 181, 212, 243, 273, 304, 334]
        !Jan  Feb  Mar  Apr  May  Jun  Jul  Aug  Sep  Oct  Nov  Dec
        ! 1    2    3    4    5    6    7    8    9    10   11   12

    enum, bind(c)
        enumerator :: ActionUnspecified = -1
        enumerator :: ActionBeginShift
        enumerator :: ActionFallAsleep
        enumerator :: ActionWakeUp
    end enum

    type :: guard_record
        ! Properties
        integer :: guard_id = -1
        integer :: action = ActionUnspecified
        ! Date Properties
        integer :: hour
        integer :: minute
        integer :: month
        integer :: day
        integer :: year
        integer :: doy ! (day of year)
    end type
    interface guard_record
        module procedure init_record
    end interface

contains

    type(guard_record) function init_record(string) result(r)
        implicit none
        character(len=*) :: string
        character(len=:),allocatable :: datestring
        character(len=:),allocatable :: actionstring
        integer :: i
        character(len=6) :: begins
        character(len=5) :: shift

        ! Remove special characters from string
        do i = 1, len(string)
            if (string(i:i) == '[') string(i:i) = ' '
            if (string(i:i) == ']') string(i:i) = ' '
            if (string(i:i) == '-') string(i:i) = ' '
            if (string(i:i) == ':') string(i:i) = ' '
            if (string(i:i) == '#') string(i:i) = ' '
        end do

        ! Split strings
        datestring   = trim(adjustl(string(1:20)))
        actionstring = trim(adjustl(string(20:)))

        ! Read variables into data
        read (datestring,*) r % year, r % month, r % day, r % hour, r % minute

        ! Get day of year
        r % doy = doy_per_month(r % month) + r % day

        ! Assign action
        i = index(actionstring, ' ')
        select case (actionstring(1:i))
        case ('Guard')
            r % action = ActionBeginShift
            read (actionstring(i:),*) r % guard_id, begins, shift
        case ('wakes')
            r % action = ActionWakeUp
        case ('falls')
            r % action = ActionFallAsleep
        case default
            write (syslog%unit,*) 'Error: unexpected input string: ',string
            call bomb
        end select
    end function
end module