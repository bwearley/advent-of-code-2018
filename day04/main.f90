program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use guard_record_mod
    implicit none

    !-- Counters
    integer :: i, j

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Parameters
    integer,parameter :: DAYS_IN_YEAR         =  365
    integer,parameter :: TRACKED_MINS_PER_DAY =   60
    integer,parameter :: MAX_NUM_GUARDS       = 9999

    !-- Main variables

    ! Tracks currently propogating guard
    integer :: current_guard = 1        

    ! Stores all records, even incomplete ones
    type(guard_record),allocatable :: records_all(:)       

    ! Stores complete records sorted by (day,minute)
    type(guard_record) :: records_sorted(DAYS_IN_YEAR,0:TRACKED_MINS_PER_DAY-1)

    ! Tracks total sleep time per (guard,minute)
    integer :: sleepytime(MAX_NUM_GUARDS,0:TRACKED_MINS_PER_DAY-1) = 0
    
    ! Tracks whether guard is "currently" asleep
    integer :: is_sleeping(MAX_NUM_GUARDS)

    ! Tracks the "sleepiest" guard
    integer :: sleepiest_guard = 1

    ! The minute during which the most sleep happens
    integer :: sleepiest_minute = 0

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(records_all(num_lines))

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    write (syslog%unit,*) 'Raw Records:'
    write (syslog%unit,*) 'Guard Act   Hr  Min  Mth  Day Year  DoY'
    do i = 1, num_lines
        read (input_unit,'(a)') line
        records_all(i) = Guard_Record(line)
        write (syslog%unit,'(8i5)')    &
            records_all(i) % guard_id, &
            records_all(i) % action,   &
            records_all(i) % hour,     &
            records_all(i) % minute,   &
            records_all(i) % month,    &
            records_all(i) % day,      &
            records_all(i) % year,     &
            records_all(i) % doy
    end do
    close (input_unit)

    !-- Start timer
    call syslog % start_timer

    !-- Sort records according to day,time
    do i = 1, num_lines
        records_sorted(records_all(i)%doy, records_all(i)%minute) = records_all(i)
    end do

    !-- Propogate values
    write (syslog%unit,*) ' Records:'
    write (syslog%unit,*) 'Guard Act  Min  Mth  Day  DoY'
    DAYS_LOOP: do i = 1, DAYS_IN_YEAR

        MINS_LOOP: do j = 0, TRACKED_MINS_PER_DAY - 1
        
            if (records_sorted(i,j) % action == ActionUnspecified) cycle MINS_LOOP

            select case (records_sorted(i,j) % action)
            case (ActionBeginShift)
                current_guard = records_sorted(i,j) % guard_id
            case (ActionWakeUp,ActionFallAsleep)
                records_sorted(i,j) % guard_id = current_guard
            case default
                write (syslog%unit,*) 'Error: unknown record action'
                call bomb
            end select

            write (syslog%unit,'(6i5)')         &
                records_sorted(i,j) % guard_id, &
                records_sorted(i,j) % action,   &
                records_sorted(i,j) % minute,   &
                records_sorted(i,j) % month,    &
                records_sorted(i,j) % day,      &
                records_sorted(i,j) % doy
 
        end do MINS_LOOP

    end do DAYS_LOOP

    !-- Calculate answers
    DAYS_LOOP_2: do i = 1, DAYS_IN_YEAR

        is_sleeping(:) = 0 ! re-initialize

        MINS_LOOP_2: do j = 0, TRACKED_MINS_PER_DAY - 1

            if (records_sorted(i,j) % action == ActionFallAsleep) then
                is_sleeping(records_sorted(i,j) % guard_id) = 1
            end if

            if (records_sorted(i,j) % action == ActionWakeUp) then
                is_sleeping(records_sorted(i,j) % guard_id) = 0
            end if

            if (records_sorted(i,j) % action == ActionBeginShift) then
                is_sleeping(records_sorted(i,j) % guard_id) = 0
            end if

            sleepytime(:,j) = sleepytime(:,j) + is_sleeping(:)

        end do MINS_LOOP_2

    end do DAYS_LOOP_2

    !-- Guard totals
    write (syslog%unit,*) 'Guard   ID MINS'
    GUARDS_LOOP: do i = 1, MAX_NUM_GUARDS
        
        ! Find sleepiest guard
        if (sum(sleepytime(i,:)) > 0) then
            if (sum(sleepytime(i,:)) > sum(sleepytime(sleepiest_guard,:))) then
               sleepiest_guard = i
            end if
            write (syslog%unit,'(ai5,a,i0)') ' Guard ',i, ' ', sum(sleepytime(i,:))
        end if
    end do GUARDS_LOOP

    !-- Answers
    write (syslog%unit,'(a,i0,a,i0,a)') &
        'Guard ',sleepiest_guard,' slept ', sum(sleepytime(sleepiest_guard,:)), ' mins'
    write (syslog%unit,'(a,i0,a,i0)') &
        'Guard ',sleepiest_guard,' slept the most during minute ',maxloc(sleepytime(sleepiest_guard,:))-1
    
    !-- Diagnostic
    write (syslog%unit,*) 'MIN  #OCCUR'
    do i = 0, TRACKED_MINS_PER_DAY - 1
        write (syslog%unit,'(2i5)') i, sleepytime(sleepiest_guard,i)
    end do
    
    ! maxloc apparently gets the location of the max with respect to a 1-indexed array
    write (syslog%unit,'(a,i0)') &
        'Part 1 answer: ',       & !50558
        (maxloc(sleepytime(sleepiest_guard,:))-1) * sleepiest_guard
    write (syslog%unit,'(a,i0,a,i0,a)') &
        'Guard ',sleepiest_guard,' slept ',sum(sleepytime(sleepiest_guard,:)), ' mins.'

    sleepiest_guard = 1

    GUARDS_LOOP_2: do i = 1, MAX_NUM_GUARDS
        ! Find sleepiest minute
        MINS_LOOP_3: do j = 0, TRACKED_MINS_PER_DAY - 1
            if (sleepytime(i,j) > sleepytime(sleepiest_guard,sleepiest_minute)) then
                sleepiest_guard  = i
                sleepiest_minute = j
            end if
        end do MINS_LOOP_3
    end do GUARDS_LOOP_2
    write (syslog%unit,'(3(a,i0))')                   &
        'Guard ',sleepiest_guard,' slept ',           &
        sleepytime(sleepiest_guard,sleepiest_minute), &
        ' times during minute ', sleepiest_minute

    ! 28198
    write (syslog%unit,'(a,i0)') 'Part 2 answer: ',sleepiest_guard * sleepiest_minute

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program