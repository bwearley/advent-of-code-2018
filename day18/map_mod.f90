module map_mod
    use syslog_mod
    use string_tools_mod
    use get_digit_of_number_mod
    implicit none

    ! Map Dimensions
    integer :: x_dim = 0
    integer :: y_dim = 0

    integer :: x_dim_min = 1
    integer :: y_dim_min = 1

    !-- Map Square Type Definitions
    enum,bind(c)
        enumerator :: OpenGround
        enumerator :: Tree
        enumerator :: Lumberyard
    end enum

    !-- MapSquare struct
    type :: MapSquare
        integer :: x = -1
        integer :: y = -1
        integer :: type = OpenGround
    contains
        procedure :: a
    end type
    type(MapSquare), allocatable :: map0(:,:)
    type(MapSquare), allocatable :: map1(:,:)
    type(MapSquare), allocatable :: map_records(:,:,:)

contains

    !> Character value of a coordinate
    function a(self)
        class(MapSquare) :: self
        character(len=1) :: a
        select case (self % type)
        case (OpenGround)
            a = '.'
        case (Tree)
            a = '|'
        case (Lumberyard)
            a = '#'
        case default
        end select
    end function

    !> Write out the current state of map
    subroutine write_state(unit)
        implicit none
        integer,intent(in) :: unit
        integer :: x, y

        !-- Write header
        ! ! Hundreds Place
        ! write (syslog%unit,'(a)', advance='no') '    '
        ! do x = x_dim_min, x_dim !_max
        !     write (syslog%unit,'(i1)',advance='no') get_digit_of_number(3,x)
        ! end do
        write (unit,*) ! advance
        ! Tens Place
        write (unit,'(a)', advance='no') '    '
        do x = x_dim_min, x_dim !_max
            write (unit,'(i1)',advance='no') get_digit_of_number(2,x)
        end do
        write (unit,*) ! advance
        ! Ones Place
        write (unit,'(a)', advance='no') '    '
        do x = x_dim_min, x_dim !_max
            write (unit,'(i1)',advance='no') get_digit_of_number(1,x)
        end do
        write (unit,*) ! advance
        write (unit,*) ! advance

        !-- Grid
        do y = y_dim_min, y_dim !_max !min(y_dim_max,100)
            write (unit,'(i0.3,a)',advance='no') y, ' '
            do x = x_dim_min, x_dim !_max
                write (unit,'(a)',advance='no') map1(x,y) % a()
            end do
            write (unit,*) ! advance
        end do
    end subroutine

end module
