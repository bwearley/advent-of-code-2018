program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use map_mod
    implicit none

    integer,parameter :: ik = 8

    !-- Counters
    integer(ik) :: i, j
    integer(ik) :: x, y

    ! Time counter
    integer(ik) :: t = 0_ik

    ! Time of first repeat (t1), earlier time that was repeated (t1)
    integer(ik) :: t0
    integer(ik) :: t1

    ! Index of T_FINAL in saved map array
    integer(ik) :: t_idx

    ! Maximum past records that can be stored
    integer(ik),parameter :: MAX_MAP_RECORDS = 10000_ik

    ! Final time
    integer(ik),parameter :: T_FINAL = 1000000000_ik

    !-- Input file unit
    integer(ik) :: input_unit

    !-- Number of lines in input file
    integer(ik) :: num_lines

    integer(ik) :: trees = 0
    integer(ik) :: lumberyards = 0

    !-- Puzzle values
    integer(ik) :: part1 = 0
    integer(ik) :: part2 = 0

    integer(ik),allocatable :: tree_filter(:,:)
    integer(ik),allocatable :: lumberyard_filter(:,:) 

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)
    y_dim = num_lines

    !-- Start timer
    call syslog % start_timer

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    !-- Read map setup
    x = 1
    y = 1
    do i = 1_ik, num_lines

        ! Read line
        read (input_unit,'(a)') line

        ! Grid x-dimension is obtained from the length
        ! of first row, then we can allocate the grid
        if (i == 1) then
            x_dim = len(trim(line))
            write (syslog%unit,'(a,i0,a,i0,a)') 'Building map of (',x_dim,'x',y_dim,')'
            allocate(map0(x_dim,y_dim))
            allocate(map1(x_dim,y_dim))
            allocate(map_records(MAX_MAP_RECORDS,x_dim,y_dim))
            allocate(tree_filter(x_dim,y_dim))
            allocate(lumberyard_filter(x_dim,y_dim))
            tree_filter(:,:) = 0
            lumberyard_filter(:,:) = 0
        end if

        ! Build map
        do x = 1, x_dim
            map0(x,y) % x = x
            map0(x,y) % y = y
            select case (line(x:x))
                case ('.'); map0(x,y) % type = OpenGround
                case ('|'); map0(x,y) % type = Tree
                case ('#'); map0(x,y) % type = Lumberyard
                case default
                    write (syslog%unit,*) __FILE__,__LINE__,'ERROR: Failed generating map.'
                    stop
            end select
        end do
        y = y + 1
    end do
    close (input_unit)
    map1(:,:) = map0(:,:)

    !-- Main Loop
    t = 0
    MINUTE_LOOP: do
        map0(:,:) = map1(:,:)
        write (syslog%unit,'(a,i0)') 'T=',t
        call write_state(syslog%unit)
        Y_LOOP: do y = y_dim_min, y_dim
            X_LOOP: do x = x_dim_min, x_dim
                trees = 0
                lumberyards = 0
                LEFT_RIGHT: do i = -1, 1
                    UP_DOWN: do j = -1, 1

                        if (i == 0 .and. j == 0) cycle
                        if (x+i < x_dim_min .or. x+i > x_dim) cycle
                        if (y+j < y_dim_min .or. y+j > y_dim) cycle

                        if (map0(x+i,y+j) % type == Tree)       trees       = trees       + 1
                        if (map0(x+i,y+j) % type == Lumberyard) lumberyards = lumberyards + 1

                        select case (map0(x,y) % type)
                        case (OpenGround)
                            if (trees >= 3) then
                                map1(x,y) % type = Tree
                            else
                                map1(x,y) % type = OpenGround
                            end if
                        case (Tree)
                            if (lumberyards >= 3) then
                                map1(x,y) % type = Lumberyard
                            else
                                map1(x,y) % type = Tree
                            end if
                        case (Lumberyard)
                            if (lumberyards >= 1 .and. trees >= 1) then
                                map1(x,y) % type = Lumberyard
                            else
                                map1(x,y) % type = OpenGround
                            end if
                        case default
                            write (syslog%unit,*) __FILE__,__LINE__,'ERROR: Failed generating map.'
                            stop
                        end select
                    end do UP_DOWN
                end do LEFT_RIGHT
            end do X_LOOP
        end do Y_LOOP
        t = t + 1

        map_records(t,:,:) = map1(:,:)

        do i = 1, t-1
            if (all(map_records(i,:,:) % type == map1(:,:) % type)) then
                t0 = i
                t1 = t
                write (syslog%unit,'(2(a,i0))') 'Map match at time ',t0,' and time ',t1
                exit MINUTE_LOOP
            end if
        end do
        
        !-- Part 1
        if (t == 10) then
            where (map1(:,:) % type == Lumberyard) lumberyard_filter = 1
            where (map1(:,:) % type == Tree)       tree_filter = 1
            part1 = sum(lumberyard_filter) * sum(tree_filter)
            write (syslog%unit,'(a,i0)') 'Part 1: ',part1 ! 44743
            write (          *,'(a,i0)') 'Part 1: ',part1
        end if
    end do MINUTE_LOOP

    !-- Part 2
    ! Found map repeat pattern, advance to time
    t_idx = mod((T_FINAL - t1) , (t1 - t0))
    map1(:,:) = map_records(t0+t_idx,:,:)

    ! Calculate total value
    tree_filter(:,:) = 0
    lumberyard_filter(:,:) = 0
    where (map1(:,:) % type == Lumberyard) lumberyard_filter = 1
    where (map1(:,:) % type == Tree)       tree_filter = 1
    part2 = sum(lumberyard_filter) * sum(tree_filter)
    write (syslog%unit,'(a,i0)') 'Part 2: ',part2 ! 169024 
    write (          *,'(a,i0)') 'Part 2: ',part2

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program