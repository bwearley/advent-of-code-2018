module instruction_mod
    use syslog_mod
    implicit none

    ! Total number of different instructions
    integer,parameter :: MAX_INSTRUCTIONS = 26

    ! Shift in ASCII characters to get A-indexed
    ! A = 65, Z = 90, a = 97, z = 122
    ! a = 65 -> i = 1
    integer,parameter :: ASCII_CHAR_SHIFT = 64

    type :: instruction
        character(len=1) :: aid                  ! char id of self
        character(len=1) :: aprereq              ! char id of prereq
        integer :: id = 0                        ! numerical id of self
        integer :: prereqid = 0                  ! numerical id of prereq
        integer :: allprereqs(MAX_INSTRUCTIONS) = 0 ! all numerical prereq ids
        integer :: allprereq2(MAX_INSTRUCTIONS) = 0 ! copy of all numerical prereq ids
    end type
    interface instruction
        module procedure init_instruction
    end interface

contains

    type(instruction) function init_instruction(string) result(ins)
        implicit none
        character(len=*) :: string

        ! Index: 123456789012345678901234567890123456789012345678
        ! Strip "Step * must be finished before step * can begin."
        string(1:5) = ' '
        string(7:36) = ' '
        string(39:) = ' '

        ! Read variables into data
        read (string,*) ins % aprereq, ins % aid

        ! Assign object properties
        ins % id       = iachar(ins % aid)     - ASCII_CHAR_SHIFT
        ins % prereqid = iachar(ins % aprereq) - ASCII_CHAR_SHIFT

    end function
end module