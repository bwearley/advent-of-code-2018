program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    use instruction_mod
    implicit none

    !-- Counters
    integer :: i, j, k, m

    !-- Input file unit
    integer :: input_unit

    !-- Number of lines in input file
    integer :: num_lines

    !-- Parameters
    type(Instruction), &
      allocatable     :: raw_instructions(:)
    type(Instruction) :: instructions(MAX_INSTRUCTIONS)
    integer           :: nextprereqid(MAX_INSTRUCTIONS)  = 1
    integer           :: instr_ordered(MAX_INSTRUCTIONS) = 0
    integer           :: next_instr = 1
    integer           :: sum_instr = 0

    type :: WorkerTask
        integer :: workerid = 0
        integer :: taskid   = 0
        integer :: tstart   = 0
        integer :: tend     = 0
    end type
    
    !-- Workers
    integer,parameter :: NUM_HELPERS = 4
    integer,parameter :: NUM_WORKERS = NUM_HELPERS + 1
    type(WorkerTask) :: workertasks(NUM_WORKERS)

    !-- Time
    integer,parameter :: BASE_TIME_PER_INST = 60 ! sec
    integer :: time = 0
    integer :: task_time(MAX_INSTRUCTIONS)

    ! Input file reading properties
    integer,parameter            :: max_line_len = 100
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Count lines in input file
    num_lines = lines_in_file(input_file)

    !-- Allocate data appropriately
    allocate(raw_instructions(num_lines))

    !-- Open file and read into memory
    open (                    & 
        newunit = input_unit, & 
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    do i = 1, num_lines

        ! Read line
        read (input_unit,'(a)') line

        ! Parse line
        raw_instructions(i) = Instruction(line)

    end do
    close (input_unit)

    !-- Write to log
    write (syslog%unit,*) 'Inst Deps  IID PreReqID'
    do i = 1, num_lines
        write (syslog%unit,'(2a5,2i5)')    &
            raw_instructions(i) % aid,     &
            raw_instructions(i) % aprereq, &
            raw_instructions(i) % id,      &
            raw_instructions(i) % prereqid
    end do

    !-- Start timer
    call syslog % start_timer

    !-- Generate instruction network
    INST_NETWORK: do i = 1, MAX_INSTRUCTIONS
        
        instructions(i) % aid = achar(i + ASCII_CHAR_SHIFT)
        instructions(i) % id  = i

        !-- Generate dependencies
        GEN_DEP: do j = 1, num_lines

            if (raw_instructions(j) % id /= instructions(i) % id) cycle GEN_DEP

            instructions(i) % allprereqs(nextprereqid(i)) = raw_instructions(j) % prereqid

            nextprereqid(i) = nextprereqid(i) + 1

        end do GEN_DEP
    end do INST_NETWORK

    !-- Backup prereqs because we're about to destroy the array
    PREREQ_BACKUP: do i = 1, MAX_INSTRUCTIONS
        instructions(i) % allprereq2(:) = instructions(i) % allprereqs(:)
    end do PREREQ_BACKUP

    !-- Write dependencies to log
    write (syslog%unit,*) 'Dependencies:'
    write (syslog%unit,'(a)',advance='no') 'Inst InID '
    do i = 1, MAX_INSTRUCTIONS
        write (syslog%unit,'(a3)',advance='no') achar(i + ASCII_CHAR_SHIFT)
    end do
    write (syslog%unit,*) ! advance
    do i = 1, MAX_INSTRUCTIONS
        write (syslog%unit,'(a4,i4,2x)',advance='no') instructions(i) % aid, instructions(i) % id
        do j = 1, MAX_INSTRUCTIONS
            write (syslog%unit,'(26i3)',advance='no') &
                instructions(i) % allprereqs(j)
        end do
        write (syslog%unit,*) ! advance
    end do

    !-- Part 1: determine single-threaded instruction order
    i = 1
    MAIN_SORT_LOOP: do

        if (sum(instructions(i) % allprereqs) == 0) then

            write (syslog%unit,*) 'Execute order: ',i,achar(i+ASCII_CHAR_SHIFT)

            ! This instruction is next
            instr_ordered(next_instr) = i
            next_instr = next_instr + 1
            instructions(i) % allprereqs(:) =  0 ! destroy deps
            instructions(i) % allprereqs(1) = -1 ! make first dep -1 for summing

            ! Zero out dependencies on this instruction
            do j = 1, MAX_INSTRUCTIONS
                do k = 1, MAX_INSTRUCTIONS
                    if (instructions(j) % allprereqs(k) == i) &
                        instructions(j) % allprereqs(k) = 0
                end do
            end do
            i = 1
            cycle MAIN_SORT_LOOP
        end if

        i = i + 1
        if (i > MAX_INSTRUCTIONS) i = 1 ! reset

        ! Check if all deps have been satisfied
        sum_instr = 0
        do j = 1, MAX_INSTRUCTIONS
            sum_instr = sum_instr + sum(instructions(j) % allprereqs(:))
        end do
        if (sum_instr == -MAX_INSTRUCTIONS) exit MAIN_SORT_LOOP

    end do MAIN_SORT_LOOP

    !-- Write orders to log
    write (syslog%unit,*) 'Instruction order:'
    do i = 1, MAX_INSTRUCTIONS
        write (syslog%unit,'(i4,a4)') instr_ordered(i), achar(instr_ordered(i) + ASCII_CHAR_SHIFT)
    end do
    write (syslog%unit,*) 'Instruction string:'
    do i = 1, MAX_INSTRUCTIONS
        write (syslog%unit,'(a)',advance='no') achar(instr_ordered(i) + ASCII_CHAR_SHIFT)
    end do
    write (syslog%unit,*) ! advance

    !-- Reinstate prereqs from backup
    do i = 1, MAX_INSTRUCTIONS
        instructions(i) % allprereqs(:) = instructions(i) % allprereq2(:)
    end do

    !-- Calculate time per task
    do i = 1, MAX_INSTRUCTIONS
        task_time(i) = i + BASE_TIME_PER_INST
    end do

    !-- Clock loop
    write (syslog%unit,*) 'Starting multi-worker time loop'
    write (syslog%unit,'(a)',advance='no') ' Time '
    do j = 1, NUM_WORKERS
        write (syslog%unit,'(a,i0,1x)',advance='no') 'Worker',j
    end do
    write (syslog%unit,*) ! advance

    time = 0
    CLOCK_LOOP: do

        write (syslog%unit,'(i5)',advance='no') time

        WORKER_JOB_FINISH_LOOP: do j = 1, NUM_WORKERS

            ! Complete active tasks
            if (time >= workertasks(j) % tend) then

                ! Task done, zero out dependencies on it
                do m = 1, MAX_INSTRUCTIONS
                    do k = 1, MAX_INSTRUCTIONS
                        if (instructions(m) % allprereqs(k) == workertasks(j) % taskid) &
                            instructions(m) % allprereqs(k) = 0
                    end do
                end do

                ! Clear task from worker
                workertasks(j) % tstart = 0
                workertasks(j) % tend   = 0
                workertasks(j) % taskid = 0

            end if

        end do WORKER_JOB_FINISH_LOOP

        WORKER_JOB_START_LOOP: do j = 1, NUM_WORKERS

            ! Still busy
            if (time < workertasks(j) % tend) then
                write (syslog%unit,'(a8)',advance='no') &
                    achar(workertasks(j) % taskid + ASCII_CHAR_SHIFT)
                    cycle WORKER_JOB_START_LOOP
            end if

            if (time >= workertasks(j) % tend) then

                ! Assign new task if available
                INSTR_LOOP: do i = 1, MAX_INSTRUCTIONS

                    if (sum(instructions(i) % allprereqs) == 0) then
                        workertasks(j) % tstart = time
                        workertasks(j) % tend   = time + task_time(i)
                        workertasks(j) % taskid = i

                        instructions(i) % allprereqs(:) =  0
                        instructions(i) % allprereqs(1) = -1
                        write (syslog%unit,'(a8)',advance='no') &
                            achar(workertasks(j) % taskid + ASCII_CHAR_SHIFT)
                        cycle WORKER_JOB_START_LOOP
                    end if
                end do INSTR_LOOP

                ! Idle worker
                write (syslog%unit,'(a8)',advance='no') '.'
            end if
        end do WORKER_JOB_START_LOOP
        write (syslog%unit,*) ! advance

        time = time + 1

        ! Check if all deps have been satisfied
        sum_instr = 0
        do i = 1, MAX_INSTRUCTIONS
            sum_instr = sum_instr + sum(instructions(i) % allprereqs(:))
        end do
        ! All dependencies satisfied and all tasks at least started
        if (sum_instr == -MAX_INSTRUCTIONS) then
            ! Check all tasks finished
            do j = 1, NUM_WORKERS
                if (time <= workertasks(j) % tend) cycle CLOCK_LOOP
            end do
            exit CLOCK_LOOP
        end if

    end do CLOCK_LOOP

    ! Don't need the final value incremented
    time = time - 1

    !-- Write to log
    write (syslog%unit,*) 'Elapsed time: ', time ! 1133

    !-- End timer
    call syslog % end_timer

    call syslog%log(__FILE__,'Done.')

end program